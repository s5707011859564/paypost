package com.example.test;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Panit
 */
public class PasswordEncoder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String plain = "Welcome#1";
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
        String cipher1 = "$2a$10$QMhsFZ.jzWFtZ8K/M29nCea4/9bbLNVYrTnkmATeMHWN5n4HkOqaW";
        String cipher2 = encoder.encode(plain);
        
        System.out.println(encoder.matches(plain, cipher1));
    }
    
}
