package com.test.view;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
 
@ManagedBean
public class MenuView {
     
    public void save() {
    	
    	System.out.println("======xxxxxxxxxx Data saved ");
        addMessage("Success", "Data saved");
    }
     
    public void update() {
    	System.out.println("======xxxxxxxxxx Data updated ");
        addMessage("Success", "Data updated");
    }
     
    public void delete() {
    	System.out.println("======xxxxxxxxxx Data deleted ");
        addMessage("Success", "Data deleted");
    }
     
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}