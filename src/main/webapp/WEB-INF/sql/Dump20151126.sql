-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Nov 26, 2015 at 03:49 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `paypost`
--

-- --------------------------------------------------------

--
-- Table structure for table `post_mst_service`
--

DROP TABLE IF EXISTS `post_mst_service`;
CREATE TABLE `post_mst_service` (
  `post_sv_id` int(11) NOT NULL,
  `post_sv_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `post_sv_fixcost` double NOT NULL,
  `post_sv_weightcost` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_mst_service`
--

INSERT INTO `post_mst_service` (`post_sv_id`, `post_sv_name`, `post_sv_fixcost`, `post_sv_weightcost`) VALUES
(1, 'sv1', 100, 10),
(2, 'sv2', 200, 20);

-- --------------------------------------------------------

--
-- Table structure for table `post_stock_mst_product_item`
--

DROP TABLE IF EXISTS `post_stock_mst_product_item`;
CREATE TABLE `post_stock_mst_product_item` (
  `PRODUCT_ITEM_ID` int(5) NOT NULL,
  `PRODUCT_ITEM_CODE` varchar(10) NOT NULL,
  `PRODUCT_ITEM_NAME` varchar(50) NOT NULL,
  `PRODUCT_ITEM_UNIT` varchar(10) NOT NULL,
  `PRODUCT_ITEM_COST` double NOT NULL,
  `PRODUCT_ITEM_PRICE` double NOT NULL,
  `PRODUCT_ITEM_AMOUNT` int(11) NOT NULL,
  `LAST_TX_DATE` datetime NOT NULL,
  `PRODUCT_CATEGORY_ID` int(5) NOT NULL,
  `PRODUCT_ITEM_IS_ACTIVE` tinyint(1) NOT NULL,
  `PRODUCT_ITEM_REMARK` varchar(150) DEFAULT NULL,
  `CREATED_DATETIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_DATETIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_stock_mst_product_item`
--

INSERT INTO `post_stock_mst_product_item` (`PRODUCT_ITEM_ID`, `PRODUCT_ITEM_CODE`, `PRODUCT_ITEM_NAME`, `PRODUCT_ITEM_UNIT`, `PRODUCT_ITEM_COST`, `PRODUCT_ITEM_PRICE`, `PRODUCT_ITEM_AMOUNT`, `LAST_TX_DATE`, `PRODUCT_CATEGORY_ID`, `PRODUCT_ITEM_IS_ACTIVE`, `PRODUCT_ITEM_REMARK`, `CREATED_DATETIME`, `CREATE_BY`, `UPDATE_DATETIME`, `UPDATE_BY`) VALUES
(10, 'test1234', 'test', 'kg', 300, 200, 40, '2015-11-25 10:42:37', 22, 1, NULL, '2015-11-25 10:28:13', 'SYSTEM', '2015-11-25 10:42:37', 'SYSTEM'),
(12, 'test1235', 'www', 'bag', 300, 500, 200, '2015-11-25 10:44:40', 22, 1, NULL, '2015-11-25 10:44:40', 'SYSTEM', NULL, NULL),
(13, 'test1236', 'aaaa', 'bag', 100, 200, 200, '2015-11-25 10:45:24', 22, 1, NULL, '2015-11-25 10:45:24', 'SYSTEM', NULL, NULL),
(14, 'k', 'k', 'k', 1, 2, 1, '2015-11-26 00:00:00', 22, 1, NULL, '2015-11-26 00:00:00', 'SYSTEM', NULL, NULL),
(15, 'k', 'k', 'k', 20, 30, 11, '2015-11-26 19:37:11', 22, 1, NULL, '2015-11-26 19:37:11', 'SYSTEM', NULL, NULL),
(16, 'ake', 'ake', 'ake', 0, 0, 1, '2015-11-26 21:13:44', 24, 1, NULL, '2015-11-26 21:13:44', 'SYSTEM', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_tx`
--

DROP TABLE IF EXISTS `post_tx`;
CREATE TABLE `post_tx` (
  `post_id` int(11) NOT NULL,
  `post_otherprice` double NOT NULL,
  `post_totalprice` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_tx`
--

INSERT INTO `post_tx` (`post_id`, `post_otherprice`, `post_totalprice`) VALUES
(1, 200, 3200),
(2, 200, 3200),
(3, 200, 3200),
(4, 200, 9220),
(5, 200, 6210),
(6, 0, 20),
(7, 300, 310),
(8, 300, 310),
(9, 20, 35740),
(10, 20, 35740),
(11, 20, 35740),
(12, 300, 19810),
(13, 300, 110510),
(14, 500, 60900),
(15, 500, 60900),
(16, 300, 40310),
(17, 100, 11100),
(18, 0, 0),
(19, 20, 30),
(20, 20, 20);

-- --------------------------------------------------------

--
-- Table structure for table `post_tx_contact`
--

DROP TABLE IF EXISTS `post_tx_contact`;
CREATE TABLE `post_tx_contact` (
  `post_contact_id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `post_service_id` int(11) DEFAULT NULL,
  `post_contact_firstName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `post_contact_lastName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `post_contact_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amphure_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `post_contact_postCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `post_contact_phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_tx_contact`
--

INSERT INTO `post_tx_contact` (`post_contact_id`, `post_id`, `post_service_id`, `post_contact_firstName`, `post_contact_lastName`, `post_contact_address`, `amphure_id`, `district_id`, `province_id`, `post_contact_postCode`, `post_contact_phone`) VALUES
(1, 8, NULL, 'k', 'k', 'k', 1, 1, 1, '10200', '1'),
(2, 9, NULL, 'k', 'asdasd', 'k', 58, 301, 3, '11000', '1'),
(3, 10, NULL, 'k', 'asdasd', 'k', 58, 301, 3, '11000', '1'),
(4, 11, NULL, 'k', 'asdasd', 'k', 58, 301, 3, '11000', '1'),
(5, 12, NULL, 'k', 'k', 'k', 1, 1, 1, '10200', '1'),
(6, 13, NULL, 'q', 'q', 'q', 1, 1, 1, '10200', '1'),
(7, 14, NULL, 'a', 'a', 'a', 1, 1, 1, '10200', '1'),
(8, NULL, 1, 'o', 'o', 'o', 1, 1, 1, '10200', '1'),
(9, 15, NULL, 'a', 'a', 'a', 1, 1, 1, '10200', '1'),
(10, NULL, 2, 'o', 'o', 'o', 1, 1, 1, '10200', '1'),
(11, 16, NULL, 'asd', 'k', 'k', 1, 1, 1, '10200', '1'),
(12, NULL, 3, 'k', 'k', 'k', 52, 237, 2, '10270', '1'),
(13, 17, NULL, '????????', '?????????', 'testin', 928, 8290, 70, '90110', '0874797686'),
(14, NULL, 4, 'a', 'b', 'c', 1, 1, 1, '10200', '1'),
(15, NULL, 5, 'b', 'c', 'd', 52, 237, 2, '10270', '1'),
(16, 18, NULL, 'k', 'k', 'k', 1, 1, 1, '10200', '1'),
(17, 19, NULL, 'k', 'k', 'k', 1, 1, 1, '10200', '1'),
(18, NULL, 6, 'k', 'k', 'k', 1, 1, 1, '10200', '1'),
(19, 20, NULL, 'k', 'k', 'k', 1, 1, 1, '10200', '1');

-- --------------------------------------------------------

--
-- Table structure for table `post_tx_product`
--

DROP TABLE IF EXISTS `post_tx_product`;
CREATE TABLE `post_tx_product` (
  `post_product_id` int(11) NOT NULL,
  `post_service_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_tx_product`
--

INSERT INTO `post_tx_product` (`post_product_id`, `post_service_id`, `product_id`, `amount`) VALUES
(1, 3, 13, 400),
(2, 4, 10, 20),
(3, 4, 13, 30),
(4, 5, 12, 5),
(5, 6, 16, 30);

-- --------------------------------------------------------

--
-- Table structure for table `post_tx_service`
--

DROP TABLE IF EXISTS `post_tx_service`;
CREATE TABLE `post_tx_service` (
  `post_service_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `weight` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_tx_service`
--

INSERT INTO `post_tx_service` (`post_service_id`, `post_id`, `service_id`, `weight`) VALUES
(1, 14, 2, 10),
(2, 15, 2, 10),
(3, 16, 1, 1),
(4, 17, 2, 20),
(5, 17, 1, 0),
(6, 19, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post_mst_service`
--
ALTER TABLE `post_mst_service`
  ADD PRIMARY KEY (`post_sv_id`);

--
-- Indexes for table `post_stock_mst_product_item`
--
ALTER TABLE `post_stock_mst_product_item`
  ADD PRIMARY KEY (`PRODUCT_ITEM_ID`);

--
-- Indexes for table `post_tx`
--
ALTER TABLE `post_tx`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `post_tx_contact`
--
ALTER TABLE `post_tx_contact`
  ADD PRIMARY KEY (`post_contact_id`);

--
-- Indexes for table `post_tx_product`
--
ALTER TABLE `post_tx_product`
  ADD PRIMARY KEY (`post_product_id`);

--
-- Indexes for table `post_tx_service`
--
ALTER TABLE `post_tx_service`
  ADD PRIMARY KEY (`post_service_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post_mst_service`
--
ALTER TABLE `post_mst_service`
  MODIFY `post_sv_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `post_stock_mst_product_item`
--
ALTER TABLE `post_stock_mst_product_item`
  MODIFY `PRODUCT_ITEM_ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `post_tx`
--
ALTER TABLE `post_tx`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `post_tx_contact`
--
ALTER TABLE `post_tx_contact`
  MODIFY `post_contact_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `post_tx_product`
--
ALTER TABLE `post_tx_product`
  MODIFY `post_product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `post_tx_service`
--
ALTER TABLE `post_tx_service`
  MODIFY `post_service_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;