-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Nov 17, 2015 at 06:23 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `paypost`
--

-- --------------------------------------------------------

--
-- Table structure for table `post_stock_mst_product`
--

CREATE TABLE `post_stock_mst_product` (
  `PRODUCT_STOCK_ID` int(5) NOT NULL,
  `PRODUCT_STOCK_CODE` varchar(10) NOT NULL,
  `PRODUCT_STOCK_NAME` varchar(50) NOT NULL,
  `PRODUCT_ITEM_ID` int(5) NOT NULL,
  `COST` double NOT NULL,
  `PRICE` double NOT NULL,
  `PRODUCT_STOCK_IS_ACTIVE` tinyint(1) NOT NULL,
  `PRODUCT_STOCK_REMARK` varchar(150) DEFAULT NULL,
  `CREATED_DATETIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_DATETIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_stock_mst_product`
--

INSERT INTO `post_stock_mst_product` (`PRODUCT_STOCK_ID`, `PRODUCT_STOCK_CODE`, `PRODUCT_STOCK_NAME`, `PRODUCT_ITEM_ID`, `COST`, `PRICE`, `PRODUCT_STOCK_IS_ACTIVE`, `PRODUCT_STOCK_REMARK`, `CREATED_DATETIME`, `CREATE_BY`, `UPDATE_DATETIME`, `UPDATE_BY`) VALUES
(2, 'asdasdas', 'dadsd', 8, 3000, 100, 1, NULL, '2015-11-17 23:22:22', 'SYSTEM', '2015-11-17 23:36:34', 'SYSTEM'),
(5, 'asdasd', 'asdasdad', 7, 300, 500, 1, NULL, '2015-11-17 23:31:49', 'SYSTEM', '2015-11-17 23:36:07', 'SYSTEM'),
(4, 'asdad', 'asdasd', 7, 100, 200, 1, NULL, '2015-11-17 23:24:29', 'SYSTEM', NULL, NULL),
(6, 'dfds', 'dsfsdf', 7, 0, 0, 1, NULL, '2015-11-18 00:00:39', 'SYSTEM', NULL, NULL),
(9, 'asd', 'asdasdad', 7, 0, 0, 1, NULL, '2015-11-18 00:15:52', 'SYSTEM', NULL, NULL),
(8, 'sad', 'sadasd', 7, 0, 0, 1, NULL, '2015-11-18 00:01:15', 'SYSTEM', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_stock_mst_product_category`
--

CREATE TABLE `post_stock_mst_product_category` (
  `PRODUCT_CATEGORY_ID` int(5) NOT NULL,
  `PRODUCT_CATEGORY_CODE` varchar(10) DEFAULT NULL,
  `PRODUCT_CATEGORY_NAME` varchar(50) DEFAULT NULL,
  `PRODUCT_CATEGORY_IS_ACTIVE` tinyint(1) DEFAULT NULL,
  `PRODUCT_CATEGORY_REMARK` varchar(150) DEFAULT NULL,
  `CREATED_DATETIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_DATETIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_stock_mst_product_category`
--

INSERT INTO `post_stock_mst_product_category` (`PRODUCT_CATEGORY_ID`, `PRODUCT_CATEGORY_CODE`, `PRODUCT_CATEGORY_NAME`, `PRODUCT_CATEGORY_IS_ACTIVE`, `PRODUCT_CATEGORY_REMARK`, `CREATED_DATETIME`, `CREATE_BY`, `UPDATE_DATETIME`, `UPDATE_BY`) VALUES
(22, 'sdfdsf', 'sdfsdf', 1, NULL, '2015-11-18 00:19:10', 'SYSTEM', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_stock_mst_product_item`
--

CREATE TABLE `post_stock_mst_product_item` (
  `PRODUCT_ITEM_ID` int(5) NOT NULL,
  `PRODUCT_ITEM_CODE` varchar(10) NOT NULL,
  `PRODUCT_ITEM_NAME` varchar(50) NOT NULL,
  `PRODUCT_ITEM_UNIT` varchar(10) NOT NULL,
  `PRODUCT_CATEGORY_ID` int(5) NOT NULL,
  `PRODUCT_ITEM_IS_ACTIVE` tinyint(1) NOT NULL,
  `PRODUCT_ITEM_REMARK` varchar(150) DEFAULT NULL,
  `CREATED_DATETIME` datetime DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_DATETIME` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_stock_mst_product_item`
--

INSERT INTO `post_stock_mst_product_item` (`PRODUCT_ITEM_ID`, `PRODUCT_ITEM_CODE`, `PRODUCT_ITEM_NAME`, `PRODUCT_ITEM_UNIT`, `PRODUCT_CATEGORY_ID`, `PRODUCT_ITEM_IS_ACTIVE`, `PRODUCT_ITEM_REMARK`, `CREATED_DATETIME`, `CREATE_BY`, `UPDATE_DATETIME`, `UPDATE_BY`) VALUES
(7, '1234', 'test', 'hello', 22, 1, NULL, '2015-11-17 22:40:31', 'SYSTEM', '2015-11-18 00:22:46', 'SYSTEM'),
(9, 'sdfsf', 'sdfsdf', 'fsdfsf', 22, 1, NULL, '2015-11-18 00:19:23', 'SYSTEM', '2015-11-18 00:22:48', 'SYSTEM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post_stock_mst_product`
--
ALTER TABLE `post_stock_mst_product`
  ADD PRIMARY KEY (`PRODUCT_STOCK_ID`);

--
-- Indexes for table `post_stock_mst_product_category`
--
ALTER TABLE `post_stock_mst_product_category`
  ADD PRIMARY KEY (`PRODUCT_CATEGORY_ID`);

--
-- Indexes for table `post_stock_mst_product_item`
--
ALTER TABLE `post_stock_mst_product_item`
  ADD PRIMARY KEY (`PRODUCT_ITEM_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post_stock_mst_product`
--
ALTER TABLE `post_stock_mst_product`
  MODIFY `PRODUCT_STOCK_ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `post_stock_mst_product_category`
--
ALTER TABLE `post_stock_mst_product_category`
  MODIFY `PRODUCT_CATEGORY_ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `post_stock_mst_product_item`
--
ALTER TABLE `post_stock_mst_product_item`
  MODIFY `PRODUCT_ITEM_ID` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
