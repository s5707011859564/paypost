-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Nov 26, 2015 at 08:32 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `paypost`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_tx`
--

DROP TABLE IF EXISTS `product_tx`;
CREATE TABLE `product_tx` (
  `product_tx_id` int(11) NOT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `totalPrice` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_tx`
--

INSERT INTO `product_tx` (`product_tx_id`, `firstName`, `lastName`, `phone`, `totalPrice`) VALUES
(1, 'akee', 'ake', '123', 3000),
(2, 'ake', 'ake', '1234', 300000),
(3, 'ake', 'ake', '1234', 30000);

-- --------------------------------------------------------

--
-- Table structure for table `product_tx_item`
--

DROP TABLE IF EXISTS `product_tx_item`;
CREATE TABLE `product_tx_item` (
  `itemId` int(11) NOT NULL,
  `product_tx_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_tx_item`
--

INSERT INTO `product_tx_item` (`itemId`, `product_tx_id`, `product_id`, `amount`) VALUES
(1, 3, 10, 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_tx`
--
ALTER TABLE `product_tx`
  ADD PRIMARY KEY (`product_tx_id`);

--
-- Indexes for table `product_tx_item`
--
ALTER TABLE `product_tx_item`
  ADD PRIMARY KEY (`itemId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_tx`
--
ALTER TABLE `product_tx`
  MODIFY `product_tx_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_tx_item`
--
ALTER TABLE `product_tx_item`
  MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;