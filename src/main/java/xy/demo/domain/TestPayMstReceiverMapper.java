/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xy.demo.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Administrator
 */
public class TestPayMstReceiverMapper implements RowMapper {

    @Override
    public TestPayMstReceiver mapRow(ResultSet rs, int i) throws SQLException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         TestPayMstReceiver obj = new TestPayMstReceiver();
         obj.setPAY_RECEIVER_ID(rs.getString("PAY_RECEIVER_ID"));
         obj.setPAY_TYPE_ID(rs.getString("PAY_TYPE_ID"));
         obj.setPAY_RECEIVER_NAME(rs.getString("PAY_RECEIVER_NAME"));
         obj.setREMARK(rs.getString("REMARK"));
         obj.setCREATE_DATE(rs.getString("CREATE_DATE"));
         obj.setUPDATE_BY("xxxx");
         obj.setUPDATE_BY("ddd");   
         return obj;
    }

}
