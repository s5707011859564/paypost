/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xy.demo.domain;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class TestPayMstReceiver implements Serializable{
    private static final long serialVersionUID = 7314863945135898704L;
    private String  PAY_RECEIVER_ID;
    private String  PAY_TYPE_ID;
    private String  PAY_RECEIVER_NAME;
    private String  REMARK;
    private String CREATE_DATE;
    private String UPDATE_DATE;  
    private String UPDATE_BY;

    public String getPAY_RECEIVER_ID() {
        return PAY_RECEIVER_ID;
    }

    public void setPAY_RECEIVER_ID(String PAY_RECEIVER_ID) {
        this.PAY_RECEIVER_ID = PAY_RECEIVER_ID;
    }

    public String getPAY_TYPE_ID() {
        return PAY_TYPE_ID;
    }

    public void setPAY_TYPE_ID(String PAY_TYPE_ID) {
        this.PAY_TYPE_ID = PAY_TYPE_ID;
    }

    public String getPAY_RECEIVER_NAME() {
        return PAY_RECEIVER_NAME;
    }

    public void setPAY_RECEIVER_NAME(String PAY_RECEIVER_NAME) {
        this.PAY_RECEIVER_NAME = PAY_RECEIVER_NAME;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public void setCREATE_DATE(String CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public String getUPDATE_DATE() {
        return UPDATE_DATE;
    }

    public void setUPDATE_DATE(String UPDATE_DATE) {
        this.UPDATE_DATE = UPDATE_DATE;
    }

    public String getUPDATE_BY() {
        return UPDATE_BY;
    }

    public void setUPDATE_BY(String UPDATE_BY) {
        this.UPDATE_BY = UPDATE_BY;
    }
    
                     

}
