/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xy.demo.test;

/**
 *
 * @author Administrator
 */

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
 
@ManagedBean(name="normal")
@SessionScoped
public class NormalBean implements  Serializable{
    private static final long serialVersionUID = -5329721810467849153L;

	public String buttonId; 

	public void printIt(ActionEvent event){
		
		//Get submit button id
		buttonId = event.getComponent().getClientId();
		
	}
	
	public String outcome(){
		return "result";
	}
}