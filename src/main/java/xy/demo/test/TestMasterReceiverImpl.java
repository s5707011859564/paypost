/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xy.demo.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import xy.demo.domain.TestPayMstReceiver;
import xy.demo.domain.TestPayMstReceiverMapper;

/**
 *
 * @author Administrator
 */
public class TestMasterReceiverImpl implements TestMasterReceiver {
    private NamedParameterJdbcTemplate jdbcTemplate;
     @Autowired
     public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
     }
     
   /* static {
        //initial  First Request  configure  datasouce only
        //Use  connection db server type  Connection pool
        Common.setConfigForConnectionPool("", "jdbc/paypost");
    }*/
   // private NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(Common.getDataSource());

    @Override
    public void TestQeury() {
        String sql = " Select  REMARK From pay_mst_receiver\n "
                + " Where pay_receiver_id=:pay_receiver_id ";
        //pay_receiver_id = :pay_receiver_id
        System.out.println("xxx sql" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("pay_receiver_id", "1");
        if (jdbcTemplate == null) {
            System.out.println("========TEST NUL=================");
        }
        List<String> roles = jdbcTemplate.query(sql, parameters, new RowMapper<String>() {
         public String mapRow(ResultSet rs, int i) throws SQLException {
         System.out.println("xxxxxx==="+rs.getString("REMARK"));
         return rs.getString("REMARK");
         }
         });
    }

    @Override
    public void getTestQuery(String username) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" PAY_RECEIVER_ID, ")
                .append(" PAY_TYPE_ID ,")
                .append(" PAY_RECEIVER_NAME,")
                .append(" REMARK,")
                .append(" CREATE_DATE ")
                .append(" From pay_mst_receiver ")
                .append(" ");
        jdbcTemplate.query(sql.toString(), new ResultSetExtractor<Map>() {
            public Map extractData(ResultSet rs) throws SQLException, DataAccessException {
                HashMap<String, String> mapRet = new HashMap<String, String>();
                while (rs.next()) {
                    mapRet.put(rs.getString("PAY_RECEIVER_ID"), rs.getString("PAY_RECEIVER_ID"));
                    System.out.println("====>" + rs.getString("PAY_RECEIVER_ID"));
                }
                return mapRet;
            }
        });

    }

    @Override
    public List<TestPayMstReceiver> getMstReceiver() {
        List<TestPayMstReceiver> userList = new ArrayList();

        String sql = "select * from pay_mst_receiver";
        // JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);  
        if (jdbcTemplate == null) {
            System.out.println("========TEST NUL=================");
        }

        userList = jdbcTemplate.query(sql, new TestPayMstReceiverMapper());
        return userList;

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
