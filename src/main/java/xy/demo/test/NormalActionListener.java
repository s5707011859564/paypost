/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xy.demo.test;

/**
 *
 * @author Administrator
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import org.springframework.beans.factory.annotation.Autowired;
import th.ac.kmutnb.it.ase.paypost.dao.UserDao;
import th.ac.kmutnb.it.ase.paypost.dao.UserDaoImpl;
import xy.demo.domain.TestPayMstReceiver;

public class NormalActionListener implements ActionListener {

    @Autowired
    TestMasterReceiver objService;

    public void setObjService(TestMasterReceiver objService) {
        this.objService = objService;
    }
        
    static {
        //initial  First Request  configure  datasouce only
        //Use  connection db server type  Connection pool
        Common.setConfigForConnectionPool("", "jdbc/paypost");
    }

    @Override
    public void processAction(ActionEvent event)
            throws AbortProcessingException {

        System.out.println("=========Any use case here?");
        //test();
        test2();

    }

    private void test2() {

        //objService.getUser("");
        //objService.listTestQeury();
        objService.TestQeury();
//        List<PayMstReceiver> test = objService.getMstReceiver();
//        for (PayMstReceiver obj2 : test) {
//            System.out.println(obj2.getPAY_RECEIVER_ID()+","+obj2.getPAY_RECEIVER_NAME()+","+obj2.getREMARK());
//        }
        // UserDao obj = new UserDaoImpl();
        // obj.TestQeury();
        System.out.println("=======END===============");

    }

    private void test() {
        Connection conn = null;
        String forward = "";
        List productTypeList = null;
        List productList = null;
        try {
            //printOutParam(request);
            //HttpSession session = request.getSession(false);
            System.out.println("-------------HOME,Form Load Controller ------------------");

            /**
             * ******** Connection DB ************
             */
            conn = Common.open();
            Common.defaultTransaction(conn);
            /**
             * ******** Connection DB ************
             */

            listProductType(conn);
            System.out.println("================================");

            Common.close(conn);
            conn = null;

        } catch (Exception e) {
            //request.setAttribute("args1", e.getMessage());
            forward = "error100"; //error100.jsp
            System.out.println(e.toString());
        } finally {
            try {
                if (conn != null) {
                    Common.close(conn);
                }
                conn = null;
                //Close connection
            } catch (Exception ex) {
            }
        }
    }

    public void listProductType(Connection conn) throws Exception {
        // TODO Auto-generated method stub
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        StringBuffer sql = new StringBuffer();

        try {

            sql.delete(0, sql.length());
            sql.append(" select * from paypost.pay_mst_receiver  ");
            System.out.println("SQL Query:" + sql.toString());
            pstmt = conn.prepareStatement(sql.toString());
            rs = pstmt.executeQuery();
            System.out.println("--->execute SQL Query.");
            //int c=0;
            while (rs.next()) {
                System.out.println(rs.getString("PAY_RECEIVER_ID") + "," + rs.getString("PAY_RECEIVER_NAME") + "," + rs.getString("REMARK"));
            }
            //Log.debug("get UserInfo FetchSize :"+c);
            System.out.println("--->Query completed.");

        } catch (Exception e) {
            System.out.println("listProductType , " + e.getMessage());
            System.out.println(" SQL Exception: " + sql.toString());
        } finally {
            //clean up.
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
        }

    }
}
