/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.ProductTxDao;
import th.ac.kmutnb.it.ase.paypost.dao.StockMstProductCategoryDao;
import th.ac.kmutnb.it.ase.paypost.dao.StockMstProductItemDao;
import th.ac.kmutnb.it.ase.paypost.dto.PostContact;
import th.ac.kmutnb.it.ase.paypost.dto.ProductTx;
import th.ac.kmutnb.it.ase.paypost.dto.ProductTxItem;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductCategory;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductItem;

/**
 *
 * @author imetanon
 */
@Component
@ManagedBean
public class ProductTxView implements Serializable {

    private static final long serialVersionUID = -7575609689604522788L;

    private ProductTx productTx;
    private ProductTxItem productTxItem;
    private List<StockMstProductItem> productList;
    private List<StockMstProductCategory> productCategoryList;
    private int categoryId;

    @Autowired
    private StockMstProductItemDao stockMstProductItemDao;

    public void setStockMstProductItemDao(StockMstProductItemDao stockMstProductItemDao) {
	this.stockMstProductItemDao = stockMstProductItemDao;
    }

    @Autowired
    private StockMstProductCategoryDao stockMstProductCategoryDao;

    public void setStockMstProductCategoryDao(StockMstProductCategoryDao stockMstProductCategoryDao) {
	this.stockMstProductCategoryDao = stockMstProductCategoryDao;
    }
    
    @Autowired
    private ProductTxDao productTxDao;

    public ProductTxDao getProductTxDao() {
	return productTxDao;
    }

    public void setProductTxDao(ProductTxDao productTxDao) {
	this.productTxDao = productTxDao;
    }
    
    @PostConstruct
    public void init() {
	productTx = new ProductTx();
	productTx.setItemList(new ArrayList<ProductTxItem>());
	productList = stockMstProductItemDao.getProductItems();
	productCategoryList = stockMstProductCategoryDao.getProductCategorys();
	productTxItem = new ProductTxItem();
    }

    public void handleDialogClose(){
	productTx = new ProductTx();
	productTx.setItemList(new ArrayList<ProductTxItem>());
	productList = stockMstProductItemDao.getProductItems();
	productCategoryList = stockMstProductCategoryDao.getProductCategorys();
	productTxItem = new ProductTxItem();
    }
    
    public void onProductCategoryChange() {
	productList = stockMstProductItemDao.getProductItemsByCategory(categoryId);
    }
    
    public void createProductTx(){
	productTxDao.createProductTx(productTx);
	productTx = new ProductTx();
	productTx.setItemList(new ArrayList<ProductTxItem>());
	productList = stockMstProductItemDao.getProductItems();
	productCategoryList = stockMstProductCategoryDao.getProductCategorys();
	productTxItem = new ProductTxItem();
    }

    public void addItem() {
	productTx.getItemList().add(productTxItem);
	calculateTotalPrice();
	productTxItem = new ProductTxItem();
    }

    public void deletItem(ProductTxItem pItem) {
	productTx.getItemList().remove(pItem);
	calculateTotalPrice();
    }

    public ProductTx getProductTx() {
	return productTx;
    }

    public void setProductTx(ProductTx productTx) {
	this.productTx = productTx;
    }

    public ProductTxItem getProductTxItem() {
	return productTxItem;
    }

    public void setProductTxItem(ProductTxItem productTxItem) {
	this.productTxItem = productTxItem;
    }

    public List<StockMstProductItem> getProductList() {
	return productList;
    }

    public void setProductList(List<StockMstProductItem> productList) {
	this.productList = productList;
    }

    public List<StockMstProductCategory> getProductCategoryList() {
	return productCategoryList;
    }

    public void setProductCategoryList(List<StockMstProductCategory> productCategoryList) {
	this.productCategoryList = productCategoryList;
    }

    public int getCategoryId() {
	return categoryId;
    }

    public void setCategoryId(int categoryId) {
	this.categoryId = categoryId;
    }
    
    public String getProductNameById(){
	return stockMstProductItemDao.getProductItem(productTxItem.getProductId()).getProductName();
    }
    
    public void calculateTotalPrice(){
	double totalPrice = 0;
	if(!productTx.getItemList().isEmpty()){
	    for(ProductTxItem pi : productTx.getItemList()){
		totalPrice += stockMstProductItemDao.getProductItem(pi.getProductId()).getPrice() * pi.getAmount();
	    }
	}
	productTx.setTotalPrice(totalPrice);
	
    }
    
    public double calculateProductPrice(ProductTxItem pi){
	return stockMstProductItemDao.getProductItem(pi.getProductId()).getPrice() * pi.getAmount();
    }

}
