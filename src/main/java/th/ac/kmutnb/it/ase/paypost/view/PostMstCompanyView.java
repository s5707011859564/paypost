/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstCompanyDao;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstCompany;

/**
 *
 * @author pradoem
 * PostMstCompany
 * 
 */

@Component
@ManagedBean
@ViewScoped
public class PostMstCompanyView implements Serializable{
    private static final long serialVersionUID = -5275243072717857869L;
    
    //--------------------Connecton
    @Autowired
    private PostMstCompanyDao postMstCompanyDao;

    public void setPostMstCompanyDao(PostMstCompanyDao postMstCompanyDao) {
        this.postMstCompanyDao = postMstCompanyDao;
    }
    //---------------------------
   private PostMstCompany postMstCompanyObj;
    private String userLogin;
    private boolean editChecker = false;

    public PostMstCompany getPostMstCompanyObj() {
        return postMstCompanyObj;
    }

    public void setPostMstCompanyObj(PostMstCompany postMstCompanyObj) {
        this.postMstCompanyObj = postMstCompanyObj;
    }
    
    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    //------------------
    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    
      @PostConstruct
    public void init() {
        doReloadData();
    }
    //Reload Data
     public void doReloadData() {
           postMstCompanyObj = postMstCompanyDao.getPostMstCompany("comp");
     }
    /**
     * *************
     * Clear
     */
    public void doClear() {
         postMstCompanyObj = null;
    }

    /**
     * *************
     * Save/update
     */
    public void doSaveAction() {
         System.out.println("==== doSaveAction ==========");
       // System.out.println("====" +deliveryTypeId);
        postMstCompanyDao.updatePostMstCompany(postMstCompanyObj);

         editChecker = false;
         doReloadData();
         //doClear();
        System.out.println("===== doSaveAction : Complete  =====");
    }



    public void doEditAction() {
        System.out.println("===== doEditAction : Start  =====");
        postMstCompanyObj = postMstCompanyDao.getPostMstCompany("comp");
         editChecker = true;
    }


    public void handleDialogClose() {
        System.out.println("========Clase Dialog============");
        // this.mstAreaType = new PostMstAreaType();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
}
