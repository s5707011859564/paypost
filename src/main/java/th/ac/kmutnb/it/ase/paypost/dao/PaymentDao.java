package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.Payment;

public interface PaymentDao {

    public void createPayments(List<Payment> payments);
}
