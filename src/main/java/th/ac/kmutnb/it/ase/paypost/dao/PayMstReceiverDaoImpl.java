package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstReceiver;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstType;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class PayMstReceiverDaoImpl implements PayMstReceiverDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    //---------------------------------------

    @Override
    public void createPayMstReceiver(PayMstReceiver obj) {
        /* INSERT INTO paypost.pay_mst_receiver
         (PAY_TYPE_ID, PAY_RECEIVER_NAME, REMARK, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY) 
         VALUES (PAY_TYPE_ID, 'PAY_RECEIVER_NAME', 'REMARK', 'CREATE_DATE', 'CREATE_BY', 'UPDATE_DATE', 'UPDATE_BY');*/
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.pay_mst_receiver  ")
                .append("  (PAY_TYPE_ID, PAY_RECEIVER_NAME, REMARK, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY)    ")
                .append("   VALUES (:PAY_TYPE_ID, :PAY_RECEIVER_NAME , :REMARK  , NOW(),  :CREATE_BY , null , null )  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PAY_TYPE_ID", obj.getPayTypeId());
        parameters.put("PAY_RECEIVER_NAME", obj.getPayReceiverName());
        parameters.put("REMARK", obj.getRemark());
        parameters.put("CREATE_BY", obj.getCreateBy());

      //  System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert pay_mst_type OK ");
    }

    @Override
    public void updatePayMstReceiver(PayMstReceiver obj) {

        /* UPDATE paypost.pay_mst_receiver 
         SET PAY_TYPE_ID = PAY_TYPE_ID , PAY_RECEIVER_NAME = 'PAY_RECEIVER_NAME', REMARK = 'REMARK', 
         CREATE_DATE = 'CREATE_DATE', CREATE_BY = 'CREATE_BY', UPDATE_DATE = 'UPDATE_DATE', UPDATE_BY = 'UPDATE_BY' 
         WHERE -- Please complete
         ;*/
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.pay_mst_receiver   ")
                .append(" SET PAY_TYPE_ID = :PAY_TYPE_ID , ")
                .append("  PAY_RECEIVER_NAME = :PAY_RECEIVER_NAME , ")
                .append("  REMARK = :REMARK  , ")
                .append("  UPDATE_BY = :UPDATE_BY,  ")
                .append("  UPDATE_DATE = NOW()  ")
                .append("  WHERE  PAY_RECEIVER_ID = :PAY_RECEIVER_ID  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PAY_TYPE_ID", obj.getPayTypeId());
        parameters.put("PAY_RECEIVER_NAME", obj.getPayReceiverName());
        parameters.put("REMARK", obj.getRemark());
        parameters.put("UPDATE_BY", obj.getUpdateBy());
        //Where
        parameters.put("PAY_RECEIVER_ID", obj.getPayReceiverId());
       // System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  pay_mst_receiver OK ");
    }

    @Override
    public void deletePayMstReceiver(int payReceiverId) {
        /*  DELETE FROM paypost.pay_mst_receiver 
         WHERE PAY_RECEIVER_ID = PAY_RECEIVER_ID AND PAY_TYPE_ID = PAY_TYPE_ID AND PAY_RECEIVER_NAME = 'PAY_RECEIVER_NAME' AND 
        REMARK = 'REMARK' AND CREATE_DATE = 'CREATE_DATE' AND CREATE_BY = 'CREATE_BY' AND UPDATE_DATE = 'UPDATE_DATE' AND UPDATE_BY = 'UPDATE_BY' ;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.pay_mst_receiver   ")
                .append(" WHERE  PAY_RECEIVER_ID = :PAY_RECEIVER_ID  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PAY_RECEIVER_ID", payReceiverId);

        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("======Delete :" + payReceiverId + "  : successfull");
    }

    @Override
    public PayMstReceiver getPayMstReceiver(int payReceiverId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  a.PAY_RECEIVER_ID,  a.PAY_TYPE_ID,b.PAY_TYPE_NAME,  a.PAY_RECEIVER_NAME,  a.REMARK,  a.CREATE_DATE,  a.CREATE_BY,  a.UPDATE_DATE,  a.UPDATE_BY   ")
                .append(" FROM paypost.pay_mst_receiver a , paypost.pay_mst_type b  ")
                .append("  WHERE  a.PAY_RECEIVER_ID = ").append(payReceiverId)
                .append(" AND a.PAY_TYPE_ID = b.PAY_TYPE_ID ");

      //  System.out.println("======Sql :" + sql);
       Map<String, Object> parameters = new HashMap<>();
       //parameters.put("PAY_RECEIVER_ID", payReceiverId);
        PayMstReceiver receiverObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PayMstReceiver>() {
            @Override
            public PayMstReceiver mapRow(ResultSet rs, int rowNumber) throws SQLException {
                PayMstReceiver obj = new PayMstReceiver();
                obj.setPayReceiverId(rs.getInt("PAY_RECEIVER_ID"));
                obj.setPayTypeId(rs.getInt("PAY_TYPE_ID"));
                obj.setPayTypeName(Utilizer.replaceNull(rs.getString("PAY_TYPE_NAME")));
                obj.setPayReceiverName(Utilizer.replaceNull(rs.getString("PAY_RECEIVER_NAME")));
                obj.setRemark(Utilizer.replaceNull(rs.getString("REMARK")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                return obj;
            }
        }); 
        return receiverObj;
    }

    @Override
    public List<PayMstReceiver> listPayMstReceiver() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  a.PAY_RECEIVER_ID,  a.PAY_TYPE_ID,b.PAY_TYPE_NAME,  a.PAY_RECEIVER_NAME,  a.REMARK,  a.CREATE_DATE,  a.CREATE_BY,  a.UPDATE_DATE,  a.UPDATE_BY   ")
                .append(" FROM paypost.pay_mst_receiver a , paypost.pay_mst_type b  ")
                .append("  WHERE  a.PAY_TYPE_ID = b.PAY_TYPE_ID ")
                .append(" ORDER BY  a.PAY_RECEIVER_ID ");
      //  System.out.println("======Sql :" + sql);
        List<PayMstReceiver> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PayMstReceiver>() {
            @Override
            public PayMstReceiver mapRow(ResultSet rs, int rowNum) throws SQLException {
                PayMstReceiver obj = new PayMstReceiver();
                obj.setPayReceiverId(rs.getInt("PAY_RECEIVER_ID"));
                obj.setPayTypeId(rs.getInt("PAY_TYPE_ID"));
                obj.setPayTypeName(Utilizer.replaceNull(rs.getString("PAY_TYPE_NAME")));
                obj.setPayReceiverName(Utilizer.replaceNull(rs.getString("PAY_RECEIVER_NAME")));
                obj.setRemark(Utilizer.replaceNull(rs.getString("REMARK")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));

                return obj;
            }
        });

        return listResult;
    }

    @Override
    public Map<String, String> listMapPayMstType() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" PAY_TYPE_ID, PAY_TYPE_NAME ")
                .append(" FROM paypost.pay_mst_type ")
                .append(" Order by  PAY_TYPE_ID ");
        //Map<String, Object> parameters = new HashMap<>();
        List<PayMstType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PayMstType>() {
            @Override
            public PayMstType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PayMstType obj = new PayMstType();
                obj.setPayTypeId(rs.getInt("PAY_TYPE_ID"));
                obj.setPayTypeName(Utilizer.replaceNull(rs.getString("PAY_TYPE_NAME")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (PayMstType obj : listResult) {
          //result.put("PAY_TYPE_ID",obj.getPayTypeId()+"".trim());
            //result.put("PAY_TYPE_NAME",obj.getPayTypeName());
            result.put(obj.getPayTypeName(), obj.getPayTypeId() + "".trim());
        }
        return result;
    }

}
