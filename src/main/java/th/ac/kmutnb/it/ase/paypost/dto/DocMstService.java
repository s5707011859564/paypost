/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class DocMstService  implements Serializable{
    private static final long serialVersionUID = -4048699496293366995L;
    
    private int docId;
    private String docCode;
    private String docCodeName;
    private String docPaper;
    private String docRemark;
    private double docPrice;
    private int docUnit;
    private String docType;
    private String createDate;
    private String createBy;
    private String updateDate;
    private String updateBy;
    private int isActive;

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public String getDocCode() {
        return docCode;
    }

    public void setDocCode(String docCode) {
        this.docCode = docCode;
    }

    public String getDocCodeName() {
        return docCodeName;
    }

    public void setDocCodeName(String docCodeName) {
        this.docCodeName = docCodeName;
    }

    public String getDocPaper() {
        return docPaper;
    }

    public void setDocPaper(String docPaper) {
        this.docPaper = docPaper;
    }

    public String getDocRemark() {
        return docRemark;
    }

    public void setDocRemark(String docRemark) {
        this.docRemark = docRemark;
    }

    public double getDocPrice() {
        return docPrice;
    }

    public void setDocPrice(double docPrice) {
        this.docPrice = docPrice;
    }



    public int getDocUnit() {
        return docUnit;
    }

    public void setDocUnit(int docUnit) {
        this.docUnit = docUnit;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "DocMstService{" + "docId=" + docId + ", docCode=" + docCode + ", docCodeName=" + docCodeName + ", docPaper=" + docPaper + ", docRemark=" + docRemark + ", docPrice=" + docPrice + ", docUnit=" + docUnit + ", docType=" + docType + ", createDate=" + createDate + ", createBy=" + createBy + ", updateDate=" + updateDate + ", updateBy=" + updateBy + ", isActive=" + isActive + '}';
    }

    
    
}
