/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstDeliveryType;

/**
 *
 * @author Administrator
 */
public interface PostMstDeliveryTypeDao {

    public void createPostMstDeliveryType(PostMstDeliveryType obj);

    public void updatePostMstDeliveryType(PostMstDeliveryType obj);

    public void deletePostMstDeliveryType(int deliveryTypeId);

    public PostMstDeliveryType getPostMstDeliveryType(int deliveryTypeId);

    public List<PostMstDeliveryType> listPostMstDeliveryType();
}
