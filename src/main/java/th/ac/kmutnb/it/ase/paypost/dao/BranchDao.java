package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.Branch;

public interface BranchDao {

    public List<Branch> getBranches();

    public Branch getBranch(int id);
}
