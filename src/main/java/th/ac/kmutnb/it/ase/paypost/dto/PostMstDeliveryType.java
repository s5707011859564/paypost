/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator ประเภทการส่ง
 */
public class PostMstDeliveryType implements Serializable {
    private static final long serialVersionUID = -8373058363882757682L;

    private int deliveryTypeId;
    private String deliveryTypeCode;
    private String deliveryTypeDescTH;
    private String deliveryTypeDescEN;
    private int isActive;
    private String createBy;
    private String createDate;
    private String updateBy;
    private String updateDate;

    public int getDeliveryTypeId() {
        return deliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }

    public String getDeliveryTypeCode() {
        return deliveryTypeCode;
    }

    public void setDeliveryTypeCode(String deliveryTypeCode) {
        this.deliveryTypeCode = deliveryTypeCode;
    }

    public String getDeliveryTypeDescTH() {
        return deliveryTypeDescTH;
    }

    public void setDeliveryTypeDescTH(String deliveryTypeDescTH) {
        this.deliveryTypeDescTH = deliveryTypeDescTH;
    }

    public String getDeliveryTypeDescEN() {
        return deliveryTypeDescEN;
    }

    public void setDeliveryTypeDescEN(String deliveryTypeDescEN) {
        this.deliveryTypeDescEN = deliveryTypeDescEN;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "PostMstDeliveryType{" + "deliveryTypeId=" + deliveryTypeId + ", deliveryTypeCode=" + deliveryTypeCode + ", deliveryTypeDescTH=" + deliveryTypeDescTH + ", deliveryTypeDescEN=" + deliveryTypeDescEN + ", isActive=" + isActive + ", createBy=" + createBy + ", createDate=" + createDate + ", updateBy=" + updateBy + ", updateDate=" + updateDate + '}';
    }

}
