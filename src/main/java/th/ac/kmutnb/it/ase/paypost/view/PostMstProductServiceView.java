/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstProductServiceDao;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstProductService;

/**
 *
 * @author pradoem
 * บริการสินค้า
 * 
 * PostMstProductServiceDao
 * 
 * PostMstProductService
 */

@Component
@ManagedBean
@ViewScoped
public class PostMstProductServiceView  implements Serializable {
    private static final long serialVersionUID = -4496037265565532787L;

    //--------------------Connecton
    @Autowired
    private PostMstProductServiceDao postMstProductServiceDao;

    public void setPostMstProductServiceDao(PostMstProductServiceDao postMstProductServiceDao) {
        this.postMstProductServiceDao = postMstProductServiceDao;
    }
    private List<PostMstProductService> listpostMstProduct;
    private PostMstProductService postMstProductObj;
    private Map<String,String> productTypeDDL;
    private Map<String,String> deliveryTypeDDL;
    private Map<String,String> areaTypeDDL;
    private boolean editChecker = false;
    private String userLogin;

    public List<PostMstProductService> getListpostMstProduct() {
        return listpostMstProduct;
    }

    public void setListpostMstProduct(List<PostMstProductService> listpostMstProduct) {
        this.listpostMstProduct = listpostMstProduct;
    }

    public PostMstProductService getPostMstProductObj() {
        return postMstProductObj;
    }

    public void setPostMstProductObj(PostMstProductService postMstProductObj) {
        this.postMstProductObj = postMstProductObj;
    }

    public Map<String, String> getProductTypeDDL() {
        return productTypeDDL;
    }

    public void setProductTypeDDL(Map<String, String> productTypeDDL) {
        this.productTypeDDL = productTypeDDL;
    }

    public Map<String, String> getDeliveryTypeDDL() {
        return deliveryTypeDDL;
    }

    public void setDeliveryTypeDDL(Map<String, String> deliveryTypeDDL) {
        this.deliveryTypeDDL = deliveryTypeDDL;
    }

    public Map<String, String> getAreaTypeDDL() {
        return areaTypeDDL;
    }

    public void setAreaTypeDDL(Map<String, String> areaTypeDDL) {
        this.areaTypeDDL = areaTypeDDL;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public String getUserLogin() {
         return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
    
   //--------------Method Action
    @PostConstruct
    public void init() {
        doReloadData();

    }

    //Reload Data
     public void doReloadData() {
       listpostMstProduct = postMstProductServiceDao.listPostMstProductService();
     }
    /****************
     * Clear
     */
    public void doClear() {
      postMstProductObj = null;
    }

    /** *************
     * Save/update
     */
    public void doSaveAction() {
       // System.out.println("==== doSaveAction ==========");
      //  System.out.println("id ==== " +postMstProductObj.getPsId());

        postMstProductObj.setCreateBy(this.getUserLogin());
        postMstProductObj.setUpdateBy(this.getUserLogin());
         if (!editChecker) {
                postMstProductServiceDao.createPostMstProductService(postMstProductObj);
         } else {
                postMstProductServiceDao.updatePostMstProductService(postMstProductObj);
         }
         editChecker = false;
         doReloadData();
         doClear();
                
       // System.out.println("===== doSaveAction : Complete  =====");
    }

    public void doDeleteAction(int argsId) {
       //System.out.println("===== doDeleteAction : Start  =====");
       //System.out.println("===== areaTypeId :" + argsId);
       
       postMstProductServiceDao.deletePostMstProductService(argsId);
        
        doReloadData();

         editChecker = false;
         addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
         //System.out.println("===== Delete : Complete  =====");
    }

    public void doEditAction(int argsId) {
     //   System.out.println("===== doEditAction : Start  =====");
      //  System.out.println("===== areaTypeId :" + argsId);
        
       postMstProductObj= postMstProductServiceDao.getPostPostMstProductService(argsId);
        // payMstReceiverObj.
       productTypeDDL = postMstProductServiceDao.listMapPostMstProductType();
       deliveryTypeDDL = postMstProductServiceDao.listMapPostMstDeliveryType();
       areaTypeDDL    = postMstProductServiceDao.listMapPostMstAreaType();
         
         editChecker = true;
         //System.out.println("===== doEditAction : Complete  =====");
    }

    public void doAddForm() {
        editChecker = false;
        doClear();
       postMstProductObj = new PostMstProductService();
       productTypeDDL = postMstProductServiceDao.listMapPostMstProductType();
       deliveryTypeDDL = postMstProductServiceDao.listMapPostMstDeliveryType();
       areaTypeDDL    = postMstProductServiceDao.listMapPostMstAreaType();
      // System.out.println("==== doAddForm ==========");
    }

    public void handleDialogClose() {
      //  System.out.println("========Clase Dialog============");
         postMstProductObj = new PostMstProductService();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }   
 
}
