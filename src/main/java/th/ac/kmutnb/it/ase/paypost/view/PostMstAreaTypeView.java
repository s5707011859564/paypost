/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstAreaTypeDao;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstAreaType;

/**
 *
 * @author pradoem Master Data ประเภทพื้นที่การส่ง
 */
@Component
@ManagedBean
@ViewScoped
public class PostMstAreaTypeView implements Serializable {

    private static final long serialVersionUID = 1184012088409577279L;

    @Autowired
    private PostMstAreaTypeDao postMstAreaTypeDao;

    public void setPostMstAreaTypeDao(PostMstAreaTypeDao postMstAreaTypeDao) {
        this.postMstAreaTypeDao = postMstAreaTypeDao;
    }

    private List<PostMstAreaType> listMstAreaType;
    private PostMstAreaType mstAreaType;
    private int areaTypeId;
    private String areaTypeCode;
    private String areaTypeDescTH;
    private String areaTypeDescEN;
    private int isActive;
    private String userName;
    private boolean editChecker = false;

    public List<PostMstAreaType> getListMstAreaType() {
        return listMstAreaType;
    }

    public void setListMstAreaType(List<PostMstAreaType> listMstAreaType) {
        this.listMstAreaType = listMstAreaType;
    }

    public PostMstAreaType getMstAreaType() {
        return mstAreaType;
    }

    public void setMstAreaType(PostMstAreaType mstAreaType) {
        this.mstAreaType = mstAreaType;
    }

    public String getAreaTypeCode() {
        return areaTypeCode;
    }

    public void setAreaTypeCode(String areaTypeCode) {
        this.areaTypeCode = areaTypeCode;
    }

    public String getAreaTypeDescTH() {
        return areaTypeDescTH;
    }

    public void setAreaTypeDescTH(String areaTypeDescTH) {
        this.areaTypeDescTH = areaTypeDescTH;
    }

    public String getAreaTypeDescEN() {
        return areaTypeDescEN;
    }

    public void setAreaTypeDescEN(String areaTypeDescEN) {
        this.areaTypeDescEN = areaTypeDescEN;
    }

    public int getAreaTypeId() {
        return areaTypeId;
    }

    public void setAreaTypeId(int areaTypeId) {
        this.areaTypeId = areaTypeId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    @PostConstruct
    public void init() {
        listMstAreaType = postMstAreaTypeDao.listPostMstAreaType();
        //For Add Form
        // mstAreaTypeForm = new PostMstAreaType();
    }

    /**
     * *************
     * Clear
     */
    public void doClear() {
        areaTypeId = 0;
        areaTypeCode = null;
        areaTypeDescTH = null;
        areaTypeDescEN = null;
        isActive = -1;
    }

    /**
     * *************
     * Save/update
     */
    public void doSaveAction() {
        System.out.println("==== doSaveAction ==========");

        System.out.println("====" + areaTypeCode);
        userName = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();

        PostMstAreaType obj = new PostMstAreaType();
        obj.setAreaTypeId(areaTypeId);
        obj.setAreaTypeCode(areaTypeCode);
        obj.setAreaTypeDescTH(areaTypeDescTH);
        obj.setAreaTypeDescEN(areaTypeDescEN);
        obj.setIsActive(isActive);
        obj.setCreateBy(userName); //obj
        obj.setUpdateBy(userName);
         System.out.println("====editChecker :" + editChecker);
        if(!editChecker){
            postMstAreaTypeDao.createPostMstAreaType(obj);
        }else{
            postMstAreaTypeDao.updatePostMstAreaType(obj);
        }
        
        editChecker = false;
        System.out.println("====editChecker :" + editChecker);

        listMstAreaType = postMstAreaTypeDao.listPostMstAreaType();
        System.out.println("==Clear Bean ==");
        doClear();
        System.out.println("===== doSaveAction : Complete  =====");
    }

    public void doDeleteAction(int areaTypeId) {
        System.out.println("===== doDeleteAction : Start  =====");
        System.out.println("===== areaTypeId :" + areaTypeId);

        postMstAreaTypeDao.deletePostMstAreaType(areaTypeId);
        listMstAreaType = postMstAreaTypeDao.listPostMstAreaType();
        System.out.println("==Clear Bean ==");
        doClear();
        editChecker = false;
        addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
        System.out.println("===== Delete : Complete  =====");
    }

    public void doEditAction(int areaTypeId) {
        System.out.println("===== doEditAction : Start  =====");
        System.out.println("===== areaTypeId :" + areaTypeId);
        PostMstAreaType obj = postMstAreaTypeDao.getPostMstAreaType(areaTypeId);
        this.areaTypeId = obj.getAreaTypeId();
        this.areaTypeCode = obj.getAreaTypeCode();
        this.areaTypeDescTH = obj.getAreaTypeDescTH();
        this.areaTypeDescEN = obj.getAreaTypeDescEN();
        this.isActive = obj.getIsActive();

        editChecker = true;
        System.out.println("===== doEditAction : Complete  =====");
    }

    public void doAddForm() {
        editChecker = false;
        //mstAreaType = new PostMstAreaType();
        doClear();
        System.out.println("==== doAddForm ==========");
    }

    public void handleDialogClose() {
        System.out.println("========Clase Dialog============");
        // this.mstAreaType = new PostMstAreaType();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
