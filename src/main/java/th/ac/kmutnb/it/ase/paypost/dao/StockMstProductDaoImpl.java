/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProduct;

/**
 *
 * @author imetanon
 */
public class StockMstProductDaoImpl implements StockMstProductDao {
    
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
	jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void createProductStock(StockMstProduct productStock) {
	String sql
		= "INSERT INTO post_stock_mst_product(PRODUCT_STOCK_CODE, PRODUCT_STOCK_NAME, PRODUCT_ITEM_ID, COST, PRICE, PRODUCT_STOCK_IS_ACTIVE, PRODUCT_STOCK_REMARK, CREATED_DATETIME, CREATE_BY) VALUES (:productStockCode, :productStockName, :productItemId, :cost, :price, :isActive, :remark, now(), :created_user)";

	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productStockCode", productStock.getProductStockCode());
	parameters.put("productStockName", productStock.getProductStockName());
	parameters.put("productItemId", productStock.getProductId());
	parameters.put("cost", productStock.getCost());
	parameters.put("price", productStock.getPrice());
	parameters.put("isActive", productStock.isIsActive());
	parameters.put("remark", productStock.getRemark());
	parameters.put("created_user", "SYSTEM");
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public void updateProductStock(StockMstProduct productStock) {
	String sql
		= "UPDATE post_stock_mst_product \n"
		+ "SET \n"
		+ "    PRODUCT_STOCK_CODE = :productCode,\n"
		+ "    PRODUCT_STOCK_NAME = :productName,\n"
		+ "    PRODUCT_ITEM_ID = :productItemId,\n"
		+ "    COST = :cost,\n"
		+ "    price = :price,\n"
		+ "    PRODUCT_STOCK_IS_ACTIVE = :isActive,\n"
		+ "    PRODUCT_STOCK_REMARK = :remark,\n"
		+ "    UPDATE_DATETIME = now(),\n"
		+ "    UPDATE_BY = :updated_user\n"
		+ "WHERE\n"
		+ "    PRODUCT_STOCK_ID = :productId";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productId", productStock.getProductStockId());
	parameters.put("productCode", productStock.getProductStockCode());
	parameters.put("productName", productStock.getProductStockName());
	parameters.put("productItemId", productStock.getProductId());
	parameters.put("cost", productStock.getCost());
	parameters.put("price", productStock.getPrice());
	parameters.put("isActive", productStock.isIsActive());
	parameters.put("remark", productStock.getRemark());
	parameters.put("updated_user", "SYSTEM");
	jdbcTemplate.update(sql, parameters);

    }

    @Override
    public void deleteProductStock(int productStockId) {
	String sql
		= "DELETE FROM post_stock_mst_product \n"
		+ "WHERE\n"
		+ "    product_stock_id = :productStockId";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productStockId", productStockId);
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public StockMstProduct getProductStock(int productStockId) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StockMstProduct> getProductStocks() {
	String sql
		= "SELECT \n"
		+ "    product_stock_id, product_stock_code, product_stock_name, product_item_id, cost, price, product_stock_is_active, product_stock_remark\n"
		+ "FROM\n"
		+ "    post_stock_mst_product\n"
		+ "ORDER BY product_stock_id";
	List<StockMstProduct> result = jdbcTemplate.query(sql, new RowMapper<StockMstProduct>() {
	    
	    @Override
	    public StockMstProduct mapRow(ResultSet rs, int i) throws SQLException {
		StockMstProduct o = new StockMstProduct();
		o.setProductStockId(rs.getInt("product_stock_id"));
		o.setProductStockCode(rs.getString("product_stock_code"));
		o.setProductStockName(rs.getString("product_stock_name"));
		o.setProductId(rs.getInt("product_item_id"));
		o.setCost(rs.getDouble("cost"));
		o.setPrice(rs.getDouble("price"));
		o.setIsActive(rs.getBoolean("product_stock_is_active"));
		o.setRemark(rs.getString("product_stock_remark"));
		return o;
	    }

	    
	});
	return result;
    }

    

}
