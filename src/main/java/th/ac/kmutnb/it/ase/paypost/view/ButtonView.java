package th.ac.kmutnb.it.ase.paypost.view;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PayMstReceiverDao;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstProductServiceDao;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstReceiver;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstProductService;

/**
 *
 * @author Panit
 */
@Component
@ManagedBean
public class ButtonView {

    @Autowired
    private PayMstReceiverDao payMstReceiverDao;

    public void setPayMstReceiverDao(PayMstReceiverDao payMstReceiverDao) {
        this.payMstReceiverDao = payMstReceiverDao;
    }

    @Autowired
    private PostMstProductServiceDao postMstProductServiceDao;

    public void setPostMstProductServiceDao(PostMstProductServiceDao postMstProductServiceDao) {
        this.postMstProductServiceDao = postMstProductServiceDao;
    }

    private String text;
    private String greeting;

    public void buttonAction(ActionEvent actionEvent) {
        if (payMstReceiverDao == null) {
            addMessage("!!! payMstReceiverDao is null");
        } else {

            test1();
             test2();
            addMessage("=:)  payMstReceiverDao is NOT null");
        }
        greeting = "Hello! " + text;
        text = null;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public void test1() {
        List<PayMstReceiver> listObj = payMstReceiverDao.listPayMstReceiver();
        for (PayMstReceiver items : listObj) {
            System.out.println("<<<<-----" + items.getPayReceiverId() + "," + items.getPayReceiverName());
        }
    }

    public void test2() {
        List<PostMstProductService> listObj = postMstProductServiceDao.listPostMstProductService();
        int i = 0;
        for (PostMstProductService items : listObj) {
           System.out.println(i+++"==="+items.toString());
        }
    }
}
