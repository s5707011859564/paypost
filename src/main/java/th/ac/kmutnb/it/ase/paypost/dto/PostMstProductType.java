/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author pradoem
 * 
 * post_mst_product_type
 */
public class PostMstProductType  implements Serializable{
    private static final long serialVersionUID = -1651876125029383602L;
    private int productTypeId;
    private String productTypeCode;
    private String productTypeTH;
    private String productTypeEN;
    private int isActive;
    private String createBy;
    private String createDate;
    private String updateBy;
    private String updateDate;

 // PRIMARY KEY (`PRODUCT_TYPE_ID`,`PRODUCT_TYPE_CODE`)

    public String getProductTypeCode() {
        return productTypeCode;
    }

    public void setProductTypeCode(String productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    
    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProductTypeTH() {
        return productTypeTH;
    }

    public void setProductTypeTH(String productTypeTH) {
        this.productTypeTH = productTypeTH;
    }

    public String getProductTypeEN() {
        return productTypeEN;
    }

    public void setProductTypeEN(String productTypeEN) {
        this.productTypeEN = productTypeEN;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "PostMstProductType{" + "productTypeId=" + productTypeId + ", productTypeCode=" + productTypeCode + ", productTypeTH=" + productTypeTH + ", productTypeEN=" + productTypeEN + ", isActive=" + isActive + ", createBy=" + createBy + ", createDate=" + createDate + ", updateBy=" + updateBy + ", updateDate=" + updateDate + '}';
    }


    
}
