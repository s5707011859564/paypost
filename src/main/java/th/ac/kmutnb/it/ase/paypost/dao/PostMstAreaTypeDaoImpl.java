/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstAreaType;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class PostMstAreaTypeDaoImpl implements PostMstAreaTypeDao {

    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    //---------------------------------------
    @Override
    public void createPostMstAreaType(PostMstAreaType obj) {
        /*
         INSERT INTO paypost.post_mst_area_type
         (AREA_TYPE_CODE, AREA_TYPE_DESC_TH, AREA_TYPE_DESC_EN, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, ISACTIVE) 
         VALUES ('AREA_TYPE_CODE', 'AREA_TYPE_DESC_TH', 'AREA_TYPE_DESC_EN', 'CREATE_BY', 'CREATE_DATE', 'UPDATE_BY', 'UPDATE_DATE', ISACTIVE);
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.post_mst_area_type  ")
                .append(" (AREA_TYPE_CODE, AREA_TYPE_DESC_TH, AREA_TYPE_DESC_EN, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, ISACTIVE)   ")
                .append("VALUES (:AREA_TYPE_CODE , :AREA_TYPE_DESC_TH ,  :AREA_TYPE_DESC_EN ,  :CREATE_BY , NOW() , null , null , :ISACTIVE ) ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("AREA_TYPE_CODE", obj.getAreaTypeCode());
        parameters.put("AREA_TYPE_DESC_TH", obj.getAreaTypeDescTH());
        parameters.put("AREA_TYPE_DESC_EN", obj.getAreaTypeDescEN());
        parameters.put("CREATE_BY", obj.getCreateBy());
        parameters.put("ISACTIVE", obj.getIsActive());
        //System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert post_mst_area_type OK ");
    }

    @Override
    public void updatePostMstAreaType(PostMstAreaType obj) {
        /*
         UPDATE paypost.post_mst_area_type 
         SET AREA_TYPE_CODE = 'AREA_TYPE_CODE' , AREA_TYPE_DESC_TH = 'AREA_TYPE_DESC_TH', AREA_TYPE_DESC_EN = 'AREA_TYPE_DESC_EN',
         CREATE_BY = 'CREATE_BY', CREATE_DATE = 'CREATE_DATE', UPDATE_BY = 'UPDATE_BY', UPDATE_DATE = 'UPDATE_DATE', ISACTIVE = ISACTIVE 
         WHERE -- Please complete
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.post_mst_area_type   ")
                .append(" SET AREA_TYPE_CODE = :AREA_TYPE_CODE , ")
                .append("  AREA_TYPE_DESC_TH = :AREA_TYPE_DESC_TH , ")
                .append("  AREA_TYPE_DESC_EN = :AREA_TYPE_DESC_EN , ")
                .append("  UPDATE_BY = :UPDATE_BY,  ")
                 .append("  UPDATE_DATE = NOW() ,    ")
                .append("  ISACTIVE = :ISACTIVE  ")
                .append(" WHERE  AREA_TYPE_ID = :AREA_TYPE_ID  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("AREA_TYPE_CODE", obj.getAreaTypeCode());
        parameters.put("AREA_TYPE_DESC_TH", obj.getAreaTypeDescTH());
        parameters.put("AREA_TYPE_DESC_EN", obj.getAreaTypeDescEN());
        parameters.put("UPDATE_BY", obj.getUpdateBy());
        parameters.put("ISACTIVE", obj.getIsActive());
        //Where
        parameters.put("AREA_TYPE_ID", obj.getAreaTypeId());
       // System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  post_mst_area_type OK ");
    }

    @Override
    public void deletePostMstAreaType(int areaTypeId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.post_mst_area_type   ")
                .append(" WHERE  AREA_TYPE_ID = :AREA_TYPE_ID  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("AREA_TYPE_ID", areaTypeId);

        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("======Delete :" + areaTypeId + "  : successfull");
    }

    @Override
    public PostMstAreaType getPostMstAreaType(int areaTypeId) {
        /*
         SELECT AREA_TYPE_ID, AREA_TYPE_CODE, AREA_TYPE_DESC_TH, AREA_TYPE_DESC_EN, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, ISACTIVE 
         FROM paypost.post_mst_area_type;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("    AREA_TYPE_ID, AREA_TYPE_CODE, AREA_TYPE_DESC_TH, AREA_TYPE_DESC_EN, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, ISACTIVE ")
                .append(" FROM paypost.post_mst_area_type ")
                .append(" WHERE AREA_TYPE_ID = :AREA_TYPE_ID ");

       // System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("AREA_TYPE_ID", areaTypeId);

        PostMstAreaType areaTypeObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PostMstAreaType>() {
            @Override
            public PostMstAreaType mapRow(ResultSet rs, int rowNumber) throws SQLException {
                PostMstAreaType obj = new PostMstAreaType();
                obj.setAreaTypeId(rs.getInt("AREA_TYPE_ID"));
                obj.setAreaTypeCode(Utilizer.replaceNull(rs.getString("AREA_TYPE_CODE")));
                obj.setAreaTypeDescTH(Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_TH")));
                obj.setAreaTypeDescEN(Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_EN")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));

                return obj;
            }
        });
        return areaTypeObj;

    }

    @Override
    public List<PostMstAreaType> listPostMstAreaType() {
        /*
         SELECT AREA_TYPE_ID, AREA_TYPE_CODE, AREA_TYPE_DESC_TH, AREA_TYPE_DESC_EN, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, ISACTIVE 
         FROM paypost.post_mst_area_type;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("   AREA_TYPE_ID, AREA_TYPE_CODE, AREA_TYPE_DESC_TH, AREA_TYPE_DESC_EN, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, ISACTIVE   ")
                .append(" FROM paypost.post_mst_area_type ")
                .append(" Order by  AREA_TYPE_ID ");
        //System.out.println("======Sql :" + sql);
        List<PostMstAreaType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstAreaType>() {
            @Override
            public PostMstAreaType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PostMstAreaType obj = new PostMstAreaType();
                obj.setAreaTypeId(rs.getInt("AREA_TYPE_ID"));
                obj.setAreaTypeCode(Utilizer.replaceNull(rs.getString("AREA_TYPE_CODE")));
                obj.setAreaTypeDescTH(Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_TH")));
                obj.setAreaTypeDescEN(Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_EN")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                return obj;
            }
        });

        return listResult;
    }

}
