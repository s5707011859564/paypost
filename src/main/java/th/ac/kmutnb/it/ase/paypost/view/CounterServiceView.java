package th.ac.kmutnb.it.ase.paypost.view;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PayeeDao;
import th.ac.kmutnb.it.ase.paypost.dao.PaymentDao;
import th.ac.kmutnb.it.ase.paypost.dto.Payee;
import th.ac.kmutnb.it.ase.paypost.dto.Payment;

@Component
@ManagedBean
public class CounterServiceView {

    private List<Payee> payees;
    private List<Payment> payments;
    private Payment payment;
    private Payment selectedPayment;
    private final SecureRandom random = new SecureRandom();

    @Autowired
    private PayeeDao payeeDao;
    public void setPayeeDao(PayeeDao payeeDao) {
        this.payeeDao = payeeDao;
    }
    
    @Autowired
    private PaymentDao paymentDao;
    public void setPaymentDao(PaymentDao paymentDao) {
        this.paymentDao = paymentDao;
    }
    
    
    @PostConstruct
    public void init() {
        payees = payeeDao.getPayees();
        payments = new ArrayList<>();
        payment = new Payment();
        selectedPayment = new Payment();
    }
    
    public void createPayment(ActionEvent event) {
        Payment o = new Payment();
        int id = random.nextInt();
        o.setId(id);
        o.setAmount(payment.getAmount());
        o.setDueDate(payment.getDueDate());
        o.setPayee(payment.getPayee());
        o.setReferenceNumber(payment.getReferenceNumber());
        payment = new Payment();
        payments.add(o);
        RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void updatePayment(ActionEvent event) {
        for (Payment p : payments) {
            if (p.getId() == selectedPayment.getId()) {
                p.setAmount(selectedPayment.getAmount());
                p.setDueDate(selectedPayment.getDueDate());
                p.setPayee(selectedPayment.getPayee());
                p.setReferenceNumber(selectedPayment.getReferenceNumber());
                break;
            }
        }
        RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void deletePayment(ActionEvent event) {
        for (Payment p : payments) {
            if (p.getId() == selectedPayment.getId()) {
                payments.remove(p);
                break;
            }
        }
        RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }
    
    public void save(ActionEvent event) {
        paymentDao.createPayments(payments);
        payments.clear();
        payment = new Payment();
        selectedPayment = new Payment();
    }
    
    public void cancel(ActionEvent event) {
        payments.clear();
        payment = new Payment();
        selectedPayment = new Payment();
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Payment getSelectedPayment() {
        return selectedPayment;
    }

    public void setSelectedPayment(Payment selectedPayment) {
        this.selectedPayment = selectedPayment;
    }

    public List<Payee> getPayees() {
        return payees;
    }

    public void setPayees(List<Payee> payees) {
        this.payees = payees;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
    
    public String getPayeeName(int id) {
        for (Payee p : payees) {
            if (id == p.getPayeeId()) {
                return p.getPayeeName();
            }
        }
        return "";
    }
    
    public BigDecimal getTotal() {
        BigDecimal result = new BigDecimal(BigInteger.ZERO);
        for (Payment p : payments) {
            result = result.add(p.getAmount());
        }
        return result;
    }
}
