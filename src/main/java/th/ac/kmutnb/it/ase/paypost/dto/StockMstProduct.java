/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author imetanon
 */
public class StockMstProduct implements Serializable{
    private static final long serialVersionUID = -6321730786978160777L;
    
    private int productStockId;
    private String productStockCode;
    private String productStockName;
    private int productId;
//    private int amount;
//    private int balance;
    private double cost;
    private double price;
    private boolean isActive;
    private String remark;

    public int getProductStockId() {
	return productStockId;
    }

    public void setProductStockId(int productStockId) {
	this.productStockId = productStockId;
    }

    public String getProductStockCode() {
	return productStockCode;
    }

    public void setProductStockCode(String productStockCode) {
	this.productStockCode = productStockCode;
    }

    public String getProductStockName() {
	return productStockName;
    }

    public void setProductStockName(String productStockName) {
	this.productStockName = productStockName;
    }

    public int getProductId() {
	return productId;
    }

    public void setProductId(int productId) {
	this.productId = productId;
    }

    public double getCost() {
	return cost;
    }

    public void setCost(double cost) {
	this.cost = cost;
    }

    public double getPrice() {
	return price;
    }

    public void setPrice(double price) {
	this.price = price;
    }

    public boolean isIsActive() {
	return isActive;
    }

    public void setIsActive(boolean isActive) {
	this.isActive = isActive;
    }

    public String getRemark() {
	return remark;
    }

    public void setRemark(String remark) {
	this.remark = remark;
    }

    

   
    
}
