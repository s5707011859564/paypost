/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import th.ac.kmutnb.it.ase.paypost.dto.Post;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import th.ac.kmutnb.it.ase.paypost.dto.PostProduct;
import th.ac.kmutnb.it.ase.paypost.dto.PostService;

/**
 *
 * @author metanonj
 */
public class PostDaoImpl implements PostDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
	jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void createPost(final Post post) {
	final String sql = "INSERT INTO \n"
		+ "post_tx(\n"
		+ "post_otherprice,post_totalprice\n"
		+ ") VALUES (?,?)";
	KeyHolder keyHolder = new GeneratedKeyHolder();
	//post
	jdbcTemplate.getJdbcOperations().update(new PreparedStatementCreator() {
	    @Override
	    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(sql, new String[]{"post_id"});
		ps.setDouble(1, post.getOtherPrice());
		ps.setDouble(2, post.getTotalPrice());
		return ps;
	    }
	}, keyHolder);
	final int postId = keyHolder.getKey().intValue();
	//sender
	final String sqlSender = "INSERT INTO \n"
		+ "post_tx_contact(\n"
		+ "post_id, post_contact_firstName, post_contact_lastName, post_contact_address, amphure_id, district_id, province_id, post_contact_postcode, post_contact_phone\n"
		+ ") VALUES (?,?,?,?,?,?,?,?,?)";
	jdbcTemplate.getJdbcOperations().update(new PreparedStatementCreator() {
	    @Override
	    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(sqlSender, new String[]{"post_contact_id"});
		ps.setInt(1, postId);
		ps.setString(2, post.getSender().getFirstName());
		ps.setString(3, post.getSender().getLastName());
		ps.setString(4, post.getSender().getAddress());
		ps.setInt(5, post.getSender().getAmphureId());
		ps.setInt(6, post.getSender().getDistrictId());
		ps.setInt(7, post.getSender().getProvinceId());
		ps.setString(8, post.getSender().getPostcode());
		ps.setString(9, post.getSender().getPhone());
		return ps;
	    }
	}, keyHolder);
	//service
	if (!post.getPostServiceList().isEmpty()) {
	    for (final PostService psv : post.getPostServiceList()) {
		final String sqlService = "INSERT INTO \n"
			+ "post_tx_service(\n"
			+ "post_id, service_id, weight\n"
			+ ") VALUES (?,?,?)";
		jdbcTemplate.getJdbcOperations().update(new PreparedStatementCreator() {
		    @Override
		    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			PreparedStatement ps = connection.prepareStatement(sqlService, new String[]{"post_service_id"});
			ps.setInt(1, postId);
			ps.setInt(2, psv.getServiceId());
			ps.setDouble(3, psv.getWeight());
			return ps;
		    }
		}, keyHolder);
		final int postServiceId = keyHolder.getKey().intValue();
		//reciver
		final String sqlReciver = "INSERT INTO \n"
			+ "post_tx_contact(\n"
			+ "post_service_id, post_contact_firstName, post_contact_lastName, post_contact_address, amphure_id, district_id, province_id, post_contact_postcode, post_contact_phone\n"
			+ ") VALUES (?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.getJdbcOperations().update(new PreparedStatementCreator() {
		    @Override
		    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			PreparedStatement ps = connection.prepareStatement(sqlReciver, new String[]{"post_contact_id"});
			ps.setInt(1, postServiceId);
			ps.setString(2, psv.getReciver().getFirstName());
			ps.setString(3, psv.getReciver().getLastName());
			ps.setString(4, psv.getReciver().getAddress());
			ps.setInt(5, psv.getReciver().getAmphureId());
			ps.setInt(6, psv.getReciver().getDistrictId());
			ps.setInt(7, psv.getReciver().getProvinceId());
			ps.setString(8, psv.getReciver().getPostcode());
			ps.setString(9, psv.getReciver().getPhone());
			return ps;
		    }
		}, keyHolder);
		//products
		if (!psv.getPostProducts().isEmpty()) {
		    for (final PostProduct pd : psv.getPostProducts()) {
			final String sqlProduct = "INSERT INTO \n"
				+ "post_tx_product(\n"
				+ "post_service_id, product_id, amount\n"
				+ ") VALUES (?,?,?)";
			jdbcTemplate.getJdbcOperations().update(new PreparedStatementCreator() {
			    @Override
			    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sqlProduct, new String[]{"post_product_id"});
				ps.setInt(1, postServiceId);
				ps.setInt(2, pd.getProductId());
				ps.setInt(3, pd.getAmount());

				return ps;
			    }
			}, keyHolder);
		    }
		}

	    }
	}

    }

    @Override
    public void updatePost(Post post) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletePost(int postId) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Post getPost(int postId) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Post> getPosts() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
