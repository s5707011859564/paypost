/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstAreaType;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstDeliveryType;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstProductService;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstProductType;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 *
 */
public class PostMstProductServiceDaoImpl implements PostMstProductServiceDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //---------------------------------------
    @Override
    public void createPostMstProductService(PostMstProductService obj) {
        /*
         INSERT INTO paypost.post_mst_product_service
         (PRODUCT_TYPE_CODE, DELIVERY_TYPE_CODE, AREA_TYPE_CODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE) 
         VALUES ('PRODUCT_TYPE_CODE', 'DELIVERY_TYPE_CODE', 'AREA_TYPE_CODE', ISACTIVE, 'CREATE_BY', 'CREATE_DATE', 'UPDATE_BY', 'UPDATE_DATE');
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.post_mst_product_service  ")
                .append(" (PRODUCT_TYPE_CODE, DELIVERY_TYPE_CODE, AREA_TYPE_CODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE)     ")
                .append(" VALUES ( :PRODUCT_TYPE_CODE ,  :DELIVERY_TYPE_CODE , :AREA_TYPE_CODE , :ISACTIVE, :CREATE_BY , NOW(), null, null ) ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PRODUCT_TYPE_CODE", obj.getProductTypeCode());
        parameters.put("DELIVERY_TYPE_CODE", obj.getDeliveryTypeCode());
        parameters.put("AREA_TYPE_CODE", obj.getAreaTypeCode());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("CREATE_BY", obj.getCreateBy());

     //   System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert post_mst_product_service OK ");

    }

    @Override
    public void updatePostMstProductService(PostMstProductService obj) {
        /*
         UPDATE paypost.post_mst_product_service 
         SET PRODUCT_TYPE_CODE = 'PRODUCT_TYPE_CODE' , 
         DELIVERY_TYPE_CODE = 'DELIVERY_TYPE_CODE', 
         AREA_TYPE_CODE = 'AREA_TYPE_CODE',
         ISACTIVE = ISACTIVE, CREATE_BY = 'CREATE_BY', CREATE_DATE = 'CREATE_DATE', 
         UPDATE_BY = 'UPDATE_BY', UPDATE_DATE = 'UPDATE_DATE' 
         WHERE -- Please complete        */

        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.post_mst_product_service   ")
                .append(" SET PRODUCT_TYPE_CODE = :PRODUCT_TYPE_CODE , ")
                .append("  DELIVERY_TYPE_CODE = :DELIVERY_TYPE_CODE , ")
                .append("  AREA_TYPE_CODE = :AREA_TYPE_CODE , ")
                .append("  ISACTIVE = :ISACTIVE , ")
                .append("  UPDATE_BY = :UPDATE_BY,  ")
                .append("  UPDATE_DATE = NOW() ")
                .append(" WHERE  PS_ID = :PS_ID  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PRODUCT_TYPE_CODE", obj.getProductTypeCode());
        parameters.put("DELIVERY_TYPE_CODE", obj.getDeliveryTypeCode());
        parameters.put("AREA_TYPE_CODE", obj.getAreaTypeCode());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("UPDATE_BY", obj.getUpdateBy());
        //Where
        parameters.put("PS_ID", obj.getPsId());
       // System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  post_mst_post OK ");
    }

    @Override
    public void deletePostMstProductService(int psId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.post_mst_product_service   ")
                .append(" WHERE  PS_ID = :PS_ID  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PS_ID", psId);

        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("======Delete :" + psId + "  : successfull");
    }

    @Override
    public PostMstProductService getPostPostMstProductService(int psId) {
        /*
         SELECT PS_ID, PRODUCT_TYPE_CODE, DELIVERY_TYPE_CODE, AREA_TYPE_CODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE 
         FROM paypost.post_mst_product_service;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  PS_ID, PRODUCT_TYPE_CODE, DELIVERY_TYPE_CODE, AREA_TYPE_CODE,  ")
                .append(" ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE   ")
                .append(" FROM paypost.post_mst_product_service ")
                .append(" WHERE PS_ID = :PS_ID ");
        
         /* sql.append(" SELECT  ")
                .append("  x.PS_ID, x.PRODUCT_TYPE_CODE,a.PRODUCT_TYPE_DESC_TH, x.DELIVERY_TYPE_CODE, b.DELIVERY_TYPE_DESC_TH,x.AREA_TYPE_CODE,c.AREA_TYPE_DESC_TH,  ")
                .append(" x.ISACTIVE, x.CREATE_BY, x.CREATE_DATE, x.UPDATE_BY, x.UPDATE_DATE   ")
                .append(" FROM paypost.post_mst_product_service x,paypost.post_mst_product_type a, paypost.post_mst_delivery_type b , paypost.post_mst_area_type c ")
                .append(" WHERE  x.PRODUCT_TYPE_CODE = a.PRODUCT_TYPE_CODE ")
                 .append(" AND x.DELIVERY_TYPE_CODE = b.DELIVERY_TYPE_CODE  ")
                 .append(" AND  x.AREA_TYPE_CODE = c.AREA_TYPE_CODE ")
                .append("  WHERE PS_ID = :PS_ID ");
        */
        //System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PS_ID", psId);

        PostMstProductService productObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PostMstProductService>() {
            @Override
            public PostMstProductService mapRow(ResultSet rs, int rowNumber) throws SQLException {

                PostMstProductService obj = new PostMstProductService();
                obj.setPsId(rs.getInt("PS_ID"));
                obj.setProductTypeCode(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")));
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                obj.setAreaTypeCode(Utilizer.replaceNull(rs.getString("AREA_TYPE_CODE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                return obj;
            }
        });
        return productObj;
    }

    @Override
    public List<PostMstProductService> listPostMstProductService() {
        /*
         SELECT PS_ID, PRODUCT_TYPE_CODE, DELIVERY_TYPE_CODE, AREA_TYPE_CODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE 
         FROM paypost.post_mst_product_service;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  x.PS_ID, x.PRODUCT_TYPE_CODE,a.PRODUCT_TYPE_DESC_TH, x.DELIVERY_TYPE_CODE, b.DELIVERY_TYPE_DESC_TH,x.AREA_TYPE_CODE,c.AREA_TYPE_DESC_TH,  ")
                .append(" x.ISACTIVE, x.CREATE_BY, x.CREATE_DATE, x.UPDATE_BY, x.UPDATE_DATE   ")
                .append(" FROM paypost.post_mst_product_service x,paypost.post_mst_product_type a, paypost.post_mst_delivery_type b , paypost.post_mst_area_type c ")
                .append(" WHERE  x.PRODUCT_TYPE_CODE = a.PRODUCT_TYPE_CODE ")
                 .append(" AND x.DELIVERY_TYPE_CODE = b.DELIVERY_TYPE_CODE  ")
                 .append(" AND  x.AREA_TYPE_CODE = c.AREA_TYPE_CODE ")
                .append(" ORDER BY x.PS_ID  ");
       // System.out.println("======Sql :" + sql);
        List<PostMstProductService> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstProductService>() {
            @Override
            public PostMstProductService mapRow(ResultSet rs, int rowNumber) throws SQLException {

                PostMstProductService obj = new PostMstProductService();
                obj.setPsId(rs.getInt("PS_ID"));
                obj.setProductTypeCode(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")));
                obj.setProductTypeName(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_DESC_TH")));
               // obj.setProductTypeName(getProductTypeName(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")))); //CALL SQL
               
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                 obj.setDeliveryTypeName(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_TH")));
               // obj.setDeliveryTypeName(getDeliveryTypeName(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE"))));//CALL SQL
              
                obj.setAreaTypeCode(Utilizer.replaceNull(rs.getString("AREA_TYPE_CODE")));
                obj.setAreaTypeName(Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_TH")));
               //obj.setAreaTypeName(getAreaTypeName(Utilizer.replaceNull(rs.getString("AREA_TYPE_CODE"))));//CALL SQL

                obj.setIsActive(rs.getInt("ISACTIVE"));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                return obj;
            }
        });
        return listResult;
    }
     
   @Override
    public Map<String, String> listMapPostMstProductType() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" PRODUCT_TYPE_ID,PRODUCT_TYPE_CODE,PRODUCT_TYPE_DESC_TH ")
                .append(" FROM PAYPOST.POST_MST_PRODUCT_TYPE ")
                .append(" ORDER BY PRODUCT_TYPE_ID  ");

        //Map<String, Object> parameters = new HashMap<>();
        List<PostMstProductType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstProductType>() {
            @Override
            public PostMstProductType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PostMstProductType obj = new PostMstProductType();
                obj.setProductTypeCode(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")));
                obj.setProductTypeTH(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_DESC_TH")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (PostMstProductType obj : listResult) {
            result.put(obj.getProductTypeTH(), obj.getProductTypeCode());
        }
        return result;
    }

    @Override
    public Map<String, String> listMapPostMstDeliveryType() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" DELIVERY_TYPE_ID,DELIVERY_TYPE_CODE,DELIVERY_TYPE_DESC_TH ")
                .append(" FROM paypost.POST_MST_DELIVERY_TYPE ")
                .append(" ORDER BY DELIVERY_TYPE_ID");
        //Map<String, Object> parameters = new HashMap<>();
        List<PostMstDeliveryType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstDeliveryType>() {
            @Override
            public PostMstDeliveryType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PostMstDeliveryType obj = new PostMstDeliveryType();
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                obj.setDeliveryTypeDescTH(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_TH")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (PostMstDeliveryType obj : listResult) {
            result.put(obj.getDeliveryTypeDescTH(), obj.getDeliveryTypeCode().trim());
        }
        return result;
    }

    @Override
    public Map<String, String> listMapPostMstAreaType() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" AREA_TYPE_ID,AREA_TYPE_CODE,AREA_TYPE_DESC_TH  ")
                .append(" FROM paypost.POST_MST_AREA_TYPE ")
                .append(" ORDER BY AREA_TYPE_ID ");
        //Map<String, Object> parameters = new HashMap<>();
        List<PostMstAreaType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstAreaType>() {
            @Override
            public PostMstAreaType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PostMstAreaType obj = new PostMstAreaType();
                obj.setAreaTypeCode(Utilizer.replaceNull(rs.getString("AREA_TYPE_CODE")));
                obj.setAreaTypeDescTH(Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_TH")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (PostMstAreaType obj : listResult) {
            result.put(obj.getAreaTypeDescTH(), obj.getAreaTypeCode().trim());
        }
        return result;
    }
    
    /*
    public String getAreaTypeName(String areaTypeCode) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append("    SELECT AREA_TYPE_ID, AREA_TYPE_CODE, AREA_TYPE_DESC_TH,AREA_TYPE_DESC_EN ")
                .append(" FROM paypost.post_mst_area_type ")
                .append(" WHERE AREA_TYPE_CODE = :AREA_TYPE_CODE ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("AREA_TYPE_CODE", areaTypeCode);
        
        System.out.println("SQL  : "+sql.toString());
        System.out.println("AREA_TYPE_CODE : "+areaTypeCode);
        Map<String, Object> itemsMap = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> result = new HashMap<>();
                result.put("AREA_TYPE_ID", Utilizer.replaceNull(rs.getString("AREA_TYPE_ID")));
                result.put("AREA_TYPE_CODE", Utilizer.replaceNull(rs.getString("AREA_TYPE_CODE")));
                result.put("AREA_TYPE_DESC_TH", Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_TH")));
                result.put("AREA_TYPE_DESC_EN", Utilizer.replaceNull(rs.getString("AREA_TYPE_DESC_EN")));
                return result;
            }
        });
        return (String) itemsMap.get("AREA_TYPE_DESC_TH");
    }

    public String getDeliveryTypeName(String deliveryTypeCode) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append("   SELECT DELIVERY_TYPE_ID, DELIVERY_TYPE_CODE, DELIVERY_TYPE_DESC_TH,DELIVERY_TYPE_DESC_EN  ")
                .append(" FROM paypost.post_mst_delivery_type ")
                .append(" WHERE DELIVERY_TYPE_CODE = :DELIVERY_TYPE_CODE ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DELIVERY_TYPE_CODE", deliveryTypeCode);

        Map<String, Object> itemsMap = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> result = new HashMap<>();
                result.put("DELIVERY_TYPE_ID", Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_ID")));
                result.put("DELIVERY_TYPE_CODE", Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                result.put("DELIVERY_TYPE_DESC_TH", Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_TH")));
                result.put("DELIVERY_TYPE_DESC_EN", Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_EN")));
                return result;
            }
        });
        return (String) itemsMap.get("DELIVERY_TYPE_DESC_TH");
    }

    public String getProductTypeName(String productTypeCode) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append("   SELECT PRODUCT_TYPE_ID, PRODUCT_TYPE_CODE, PRODUCT_TYPE_DESC_TH,PRODUCT_TYPE_DESC_EN  ")
                .append(" FROM paypost.post_mst_product_type ")
                .append(" WHERE PRODUCT_TYPE_CODE = :PRODUCT_TYPE_CODE ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PRODUCT_TYPE_CODE", productTypeCode);

        Map<String, Object> itemsMap = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> result = new HashMap<>();
                result.put("PRODUCT_TYPE_ID", Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_ID")));
                result.put("PRODUCT_TYPE_CODE", Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")));
                result.put("PRODUCT_TYPE_DESC_TH", Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_DESC_TH")));
                result.put("PRODUCT_TYPE_DESC_EN", Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_DESC_EN")));
                return result;
            }
        });
        return (String) itemsMap.get("PRODUCT_TYPE_DESC_TH");
    }
    */


}
 //String  productTypeName = jdbcTemplate.queryForObject(sql.toString(), parameters, String.class);
 //return productTypeName;
