/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PayMstTypeDao;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstType;

/**
 *
 * @author pradoem
 * ประเภทให้บริการ
 * 
 * PayMstType
 * 
 */

@Component
@ManagedBean
@ViewScoped
public class PayMstTypeView  implements Serializable{
      private static final long serialVersionUID = 5019576627752893180L;
      
    //--------------------Connecton
    @Autowired
    private PayMstTypeDao payMstTypeDao;
    public void setPayMstTypeDao(PayMstTypeDao payMstTypeDao) {
        this.payMstTypeDao = payMstTypeDao;
    }
    //--------------------------------
    private List<PayMstType> listPayMstType;
    private PayMstType payMstTypeObj;
    private boolean editChecker = false;
    private String userLogin;
  
    public List<PayMstType> getListPayMstType() {
        return listPayMstType;
    }

    public void setListPayMstType(List<PayMstType> listPayMstType) {
        this.listPayMstType = listPayMstType;
    }

    public PayMstType getPayMstTypeObj() {
        return payMstTypeObj;
    }

    public void setPayMstTypeObj(PayMstType payMstTypeObj) {
        this.payMstTypeObj = payMstTypeObj;
    }



    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public String getUserLogin() {
        return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }


    //--------------Method Action
    @PostConstruct
    public void init() {
        doReloadData();

    }

    //Reload Data
     public void doReloadData() {
           listPayMstType = payMstTypeDao.listPayMstType();
     }
    /****************
     * Clear
     */
    public void doClear() {
       payMstTypeObj = null;
    }

    /**
     * *************
     * Save/update
     */
    public void doSaveAction() {
        System.out.println("==== doSaveAction ==========");
       // System.out.println("====" +deliveryTypeId);

        payMstTypeObj.setCreateBy(this.getUserLogin());
        payMstTypeObj.setUpdateBy(this.getUserLogin());
         if (!editChecker) {
                payMstTypeDao.createPayMstType(payMstTypeObj);
         } else {
                payMstTypeDao.updatePayMstType(payMstTypeObj);
         }

         editChecker = false;

         doReloadData();

         doClear();
        System.out.println("===== doSaveAction : Complete  =====");
    }

    public void doDeleteAction(int argsId) {
       System.out.println("===== doDeleteAction : Start  =====");
       System.out.println("===== areaTypeId :" + argsId);
       
       payMstTypeDao.deletePayMstType(argsId);
        
        doReloadData();

         editChecker = false;
         addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
         System.out.println("===== Delete : Complete  =====");
    }

    public void doEditAction(int argsId) {
        System.out.println("===== doEditAction : Start  =====");
        System.out.println("===== areaTypeId :" + argsId);
        
        payMstTypeObj = payMstTypeDao.getPayMstType(argsId);

         editChecker = true;
        // System.out.println("===== doEditAction : Complete  =====");
    }

    public void doAddForm() {
        editChecker = false;
        doClear();
        payMstTypeObj = new PayMstType();
        //System.out.println("==== doAddForm ==========");
    }

    public void handleDialogClose() {
      //  System.out.println("========Clase Dialog============");
        // this.mstAreaType = new PostMstAreaType();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    
}
