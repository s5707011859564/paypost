/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstPost;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class PostMstPostDaoImpl implements PostMstPostDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //---------------------------------------
    @Override
    public void createPostMstPost(PostMstPost obj) {
        /*
         INSERT INTO paypost.post_mst_post
         (SUBDISTRICT_ID, SUBDISTRICT_NAME, DISTRICT_ID, DISTRICT_NAME, PROVINCE_ID, PROVINCE_NAME, POSTCODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE) 
         VALUES ('SUBDISTRICT_ID', 'SUBDISTRICT_NAME', 'DISTRICT_ID', 'DISTRICT_NAME', 'PROVINCE_ID', 'PROVINCE_NAME', 'POSTCODE', ISACTIVE, 'CREATE_BY', 'CREATE_DATE', 'UPDATE_BY', 'UPDATE_DATE' );
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.post_mst_post  ")
                .append(" (SUBDISTRICT_ID, SUBDISTRICT_NAME, DISTRICT_ID, DISTRICT_NAME, PROVINCE_ID, PROVINCE_NAME, POSTCODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE)     ")
                .append(" VALUES (:SUBDISTRICT_ID , :SUBDISTRICT_NAME , :DISTRICT_ID , :DISTRICT_NAME , :PROVINCE_ID , :PROVINCE_NAME , :POSTCODE , :ISACTIVE, :CREATE_BY , NOW(), null , null ) ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("SUBDISTRICT_ID", obj.getSubDistrictId());
        parameters.put("SUBDISTRICT_NAME", obj.getSubDistrictName());
        parameters.put("DISTRICT_ID", obj.getDistrictId());
        parameters.put("DISTRICT_NAME", obj.getDistrictName());
        parameters.put("PROVINCE_ID", obj.getProvinceId());
        parameters.put("PROVINCE_NAME", obj.getProvinceName());
        parameters.put("POSTCODE", obj.getPostCode());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("CREATE_BY", obj.getCreateBy());
      //  System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert post_mst_post OK ");

    }

    @Override
    public void updatePostMstPost(PostMstPost obj) {
        /*
         UPDATE paypost.post_mst_post 
         SET SUBDISTRICT_ID = 'SUBDISTRICT_ID' , SUBDISTRICT_NAME = 'SUBDISTRICT_NAME', 
         DISTRICT_ID = 'DISTRICT_ID', DISTRICT_NAME = 'DISTRICT_NAME', PROVINCE_ID = 'PROVINCE_ID', 
         PROVINCE_NAME = 'PROVINCE_NAME', POSTCODE = 'POSTCODE', ISACTIVE = ISACTIVE, 
         CREATE_BY = 'CREATE_BY', CREATE_DATE = 'CREATE_DATE', UPDATE_BY = 'UPDATE_BY', UPDATE_DATE = 'UPDATE_DATE' 
         WHERE -- Please complete
         ;   */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.post_mst_post   ")
                .append(" SET SUBDISTRICT_ID = :SUBDISTRICT_ID , ")
                .append("  SUBDISTRICT_NAME = :SUBDISTRICT_NAME , ")
                .append("  DISTRICT_ID = :PROVINCE_ID , ")
                .append("  DISTRICT_NAME = :DISTRICT_NAME , ")
                .append("  PROVINCE_ID = :PROVINCE_ID,  ")
                .append("  PROVINCE_NAME = :PROVINCE_NAME,  ")
                .append("  POSTCODE = :POSTCODE , ")
                .append("  ISACTIVE = :ISACTIVE , ")
                .append("  UPDATE_BY = :UPDATE_BY,  ")
                .append("  UPDATE_DATE = NOW() ")
                .append(" WHERE  POST_ID = :POST_ID  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("SUBDISTRICT_ID", obj.getSubDistrictId());
        parameters.put("SUBDISTRICT_NAME", obj.getSubDistrictName());
        parameters.put("DISTRICT_ID", obj.getDistrictId());
        parameters.put("DISTRICT_NAME", obj.getDistrictName());
        parameters.put("PROVINCE_ID", obj.getProvinceId());
        parameters.put("PROVINCE_NAME", obj.getProvinceName());
        parameters.put("POSTCODE", obj.getPostCode());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("UPDATE_BY", obj.getUpdateBy());
        //Where
        parameters.put("POST_ID", obj.getPostId());
       // System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  post_mst_post OK ");
    }

    @Override
    public void deletePostMstPost(int postId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.post_mst_post   ")
                .append(" WHERE  POST_ID = :POST_ID  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("POST_ID", postId);

        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("======Delete :" + postId + "  : successfull");
    }

    @Override
    public PostMstPost getPostMstPost(int postId) {
        /*
         SELECT
         POST_ID, SUBDISTRICT_ID, SUBDISTRICT_NAME, DISTRICT_ID, DISTRICT_NAME, PROVINCE_ID, PROVINCE_NAME, POSTCODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE 
         FROM paypost.post_mst_post
         ORDER BY POST_ID
         WHERE POST_ID = 1
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" POST_ID, SUBDISTRICT_ID, SUBDISTRICT_NAME, DISTRICT_ID, DISTRICT_NAME, ")
                .append(" PROVINCE_ID, PROVINCE_NAME, POSTCODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE  ")
                .append(" FROM paypost.post_mst_post ")
                .append(" WHERE POST_ID = :POST_ID ");
       // System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("POST_ID", postId);

        PostMstPost postObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PostMstPost>() {
            @Override
            public PostMstPost mapRow(ResultSet rs, int rowNumber) throws SQLException {
                PostMstPost obj = new PostMstPost();
                obj.setPostId(rs.getInt("POST_ID"));
                obj.setSubDistrictId(Utilizer.replaceNull(rs.getString("SUBDISTRICT_ID")));
                obj.setSubDistrictName(Utilizer.replaceNull(rs.getString("SUBDISTRICT_NAME")));
                obj.setDistrictId(Utilizer.replaceNull(rs.getString("DISTRICT_ID")));
                obj.setDistrictName(Utilizer.replaceNull(rs.getString("DISTRICT_NAME")));
                obj.setProvinceId(Utilizer.replaceNull(rs.getString("PROVINCE_ID")));
                obj.setProvinceName(Utilizer.replaceNull(rs.getString("PROVINCE_NAME")));
                obj.setPostCode(Utilizer.replaceNull(rs.getString("POSTCODE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                return obj;
            }
        });
        return postObj;
    }

    @Override
    public List<PostMstPost> listPostMstPost() {
        /*
         SELECT POST_ID, SUBDISTRICT_ID, SUBDISTRICT_NAME, DISTRICT_ID, DISTRICT_NAME, 
         PROVINCE_ID, PROVINCE_NAME, POSTCODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE 
         FROM paypost.post_mst_post
         ORDER BY POST_ID
         WHERE POST_ID = 1
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" POST_ID, SUBDISTRICT_ID, SUBDISTRICT_NAME, DISTRICT_ID, DISTRICT_NAME, ")
                .append(" PROVINCE_ID, PROVINCE_NAME, POSTCODE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE  ")
                .append(" FROM paypost.post_mst_post ")
                .append("   ORDER BY POST_ID ");
      //  System.out.println("======Sql :" + sql);
        List<PostMstPost> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstPost>() {
            @Override
            public PostMstPost mapRow(ResultSet rs, int rowNum) throws SQLException {
                PostMstPost obj = new PostMstPost();
                obj.setPostId(rs.getInt("POST_ID"));
                obj.setSubDistrictId(Utilizer.replaceNull(rs.getString("SUBDISTRICT_ID")));
                obj.setSubDistrictName(Utilizer.replaceNull(rs.getString("SUBDISTRICT_NAME")));
                obj.setDistrictId(Utilizer.replaceNull(rs.getString("DISTRICT_ID")));
                obj.setDistrictName(Utilizer.replaceNull(rs.getString("DISTRICT_NAME")));
                obj.setProvinceId(Utilizer.replaceNull(rs.getString("PROVINCE_ID")));
                obj.setProvinceName(Utilizer.replaceNull(rs.getString("PROVINCE_NAME")));
                obj.setPostCode(Utilizer.replaceNull(rs.getString("POSTCODE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                return obj;
            }
        });
        return listResult;
    }

}
