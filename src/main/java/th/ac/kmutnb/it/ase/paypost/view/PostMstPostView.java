/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstPostDao;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstPost;

/**
 *
 * @author pradoem สถานที่จัดส่ง
 *
 * PostMstPost 
 * 
 * PostMstPostDao
 */
@Component
@ManagedBean
@ViewScoped
public class PostMstPostView implements Serializable {

    private static final long serialVersionUID = 5582755094563980361L;

    //--------------------Connecton
    @Autowired
    private PostMstPostDao postMstPostDao;

    public void setPostMstPostDao(PostMstPostDao postMstPostDao) {
        this.postMstPostDao = postMstPostDao;
    }
   //-------------------------- 
   private PostMstPost postMstPostObj;
    private List<PostMstPost> listPostMstPost;
    private boolean editChecker = false;
    private String userLogin;

    public PostMstPost getPostMstPostObj() {
        return postMstPostObj;
    }

    public void setPostMstPostObj(PostMstPost postMstPostObj) {
        this.postMstPostObj = postMstPostObj;
    }

    public List<PostMstPost> getListPostMstPost() {
        return listPostMstPost;
    }

    public void setListPostMstPost(List<PostMstPost> listPostMstPost) {
        this.listPostMstPost = listPostMstPost;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public String getUserLogin() {
         return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

       //--------------Method Action
    @PostConstruct
    public void init() {
        doReloadData();

    }

    //Reload Data
     public void doReloadData() {
         listPostMstPost =  postMstPostDao.listPostMstPost();
     }
    /****************
     * Clear
     */
    public void doClear() {
       postMstPostObj= null;
    }

    /**
     * *************
     * Save/update
     */
    public void doSaveAction() {
        //System.out.println("==== doSaveAction ==========");
       // System.out.println("====" +deliveryTypeId);

        postMstPostObj.setCreateBy(this.getUserLogin());
        postMstPostObj.setUpdateBy(this.getUserLogin());
         if (!editChecker) {
                postMstPostDao.createPostMstPost(postMstPostObj);
         } else {
               postMstPostDao.updatePostMstPost(postMstPostObj);
         }
         editChecker = false;
         doReloadData();
         doClear();
        //System.out.println("===== doSaveAction : Complete  =====");
    }

    public void doDeleteAction(int argsId) {
       //System.out.println("===== doDeleteAction : Start  =====");
       //System.out.println("===== areaTypeId :" + argsId);     
       postMstPostDao.deletePostMstPost(argsId);        
        doReloadData();
         editChecker = false;
         addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
        // System.out.println("===== Delete : Complete  =====");
    }

    public void doEditAction(int argsId) {
        //System.out.println("===== doEditAction : Start  =====");
        //System.out.println("===== areaTypeId :" + argsId);
        
         postMstPostObj  = postMstPostDao.getPostMstPost(argsId);

         editChecker = true;
       // System.out.println("===== doEditAction : Complete  =====");
    }

    public void doAddForm() {
        editChecker = false;
        doClear();
        postMstPostObj = new PostMstPost();
        //System.out.println("==== doAddForm ==========");
    }

    public void handleDialogClose() {
      //  System.out.println("========Clase Dialog============");
        //postMstPostObj = new PostMstPost();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
