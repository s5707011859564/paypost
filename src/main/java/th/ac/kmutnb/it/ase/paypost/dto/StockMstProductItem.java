/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author imetanon
 */
public class StockMstProductItem implements Serializable{
    private static final long serialVersionUID = 636579375171106813L;
    
    private int productId;
    private String productCode;
    private String productName;
    private int productCategoryId;
    private String productUnit;
    private double price;
    private double cost;
    private int amount;
    private boolean isActive;
    private String remark;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductCategoryId() {
	return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
	this.productCategoryId = productCategoryId;
    }


    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
	return "StockMstProductItem{" + "productId=" + productId + ", productCode=" + productCode + ", productName=" + productName + ", prodcutCategoryId=" + productCategoryId + ", productUnit=" + productUnit + ", isActive=" + isActive + ", remark=" + remark + '}';
    }

    
    
    
}
