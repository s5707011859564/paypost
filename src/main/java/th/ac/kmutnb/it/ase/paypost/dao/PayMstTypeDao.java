/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstType;

/**
 *
 * @author Administrator
 */
public interface PayMstTypeDao {

    public void createPayMstType(PayMstType obj);

    public void updatePayMstType(PayMstType obj);

    public void deletePayMstType(int argsId);

    public PayMstType getPayMstType(int argsId);

    public List<PayMstType> listPayMstType();

}
