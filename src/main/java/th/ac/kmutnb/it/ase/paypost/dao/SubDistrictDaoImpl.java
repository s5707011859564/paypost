package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.SubDistrict;

public class SubDistrictDaoImpl implements SubDistrictDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<SubDistrict> getSubDistricts(int districtId) {
        String sql =
"SELECT \n" +
"    DISTRICT_ID, DISTRICT_CODE, DISTRICT_NAME\n" +
"FROM\n" +
"    districts\n" +
"WHERE\n" +
"    AMPHUR_ID = :id\n" +
"ORDER BY DISTRICT_CODE ASC";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", districtId);
        List<SubDistrict> result = jdbcTemplate.query(sql, parameters, new RowMapper<SubDistrict>() {
            @Override
            public SubDistrict mapRow(ResultSet rs, int i) throws SQLException {
                SubDistrict o = new SubDistrict();
                o.setId(rs.getInt("DISTRICT_ID"));
                o.setCode(rs.getString("DISTRICT_CODE"));
                o.setName(rs.getString("DISTRICT_NAME"));
                return o;
            }
        });
        return result;
    }

    @Override
    public SubDistrict getSubDistrict(int id) {
        String sql =
"SELECT \n" +
"    DISTRICT_ID, DISTRICT_CODE, DISTRICT_NAME\n" +
"FROM\n" +
"    districts\n" +
"WHERE\n" +
"    DISTRICT_ID = :id\n" +
"ORDER BY DISTRICT_CODE ASC";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        SubDistrict result = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<SubDistrict>() {
            @Override
            public SubDistrict mapRow(ResultSet rs, int i) throws SQLException {
                SubDistrict o = new SubDistrict();
                o.setId(rs.getInt("DISTRICT_ID"));
                o.setCode(rs.getString("DISTRICT_CODE"));
                o.setName(rs.getString("DISTRICT_NAME"));
                return o;
            }
        });
        return result;
    }

    @Override
    public String getZipCode(String districtCode) {
        String sql =
"SELECT \n" +
"    zipcode\n" +
"FROM\n" +
"    zipcodes\n" +
"WHERE\n" +
"    DISTRICT_CODE = :code";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("code", districtCode);
        String result = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString("zipcode");
            }
        });
        return result;
    }

}
