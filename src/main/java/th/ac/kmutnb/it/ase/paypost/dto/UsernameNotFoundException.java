package th.ac.kmutnb.it.ase.paypost.dto;

public class UsernameNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = 8002897495271853604L;
    
}
