/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import java.util.Map;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstServiceFee;

/**
 *
 * @author Administrator
 */
public interface PostMstServiceFeeDao {

    public void createPostMstServiceFee(PostMstServiceFee obj);

    public void updatePostMstServiceFee(PostMstServiceFee obj);

    public void deletePostMstServiceFee(int serviceFeeId);

    public PostMstServiceFee getPostMstServiceFee(int serviceFeeId);

    public List<PostMstServiceFee> listPostMstServiceFee();
    
     public Map<String,String>  listMapPostMstProductType();
     public Map<String,String>  listMapPostMstDeliveryType();

}
