package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import java.util.Map;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstReceiver;


/**
 *
 * @author Administrator
 */
public interface PayMstReceiverDao {

    public void createPayMstReceiver(PayMstReceiver receiverObj);

    public void updatePayMstReceiver(PayMstReceiver receiverObj);

    public void deletePayMstReceiver(int payReceiverId);

    public PayMstReceiver getPayMstReceiver(int payReceiverId);

    public List<PayMstReceiver> listPayMstReceiver();
    
    public Map<String,String>  listMapPayMstType();
    
}
