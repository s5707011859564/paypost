package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.District;

public interface DistrictDao {

    public List<District> getDistricts(int provinceId);

    public District getDistrict(int id);

}
