package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.SubDistrict;

public interface SubDistrictDao {

    public List<SubDistrict> getSubDistricts(int districtId);

    public SubDistrict getSubDistrict(int id);

    public String getZipCode(String districtCode);

}
