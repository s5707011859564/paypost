/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class DocMstTrans  implements  Serializable{
    private int docId;
    private String docAutoId;
    private String docCode;
    private String colorCode;
    private String docDesc;
    private int docItems;
    private double  price;
    private double sumItems;
    private String sizeDocType; 
    private double totalPrice;

    public String getDocAutoId() {
        return docAutoId;
    }

    public void setDocAutoId(String docAutoId) {
        this.docAutoId = docAutoId;
    }

    
    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
    
    public String getSizeDocType() {
        return sizeDocType;
    }

    public void setSizeDocType(String sizeDocType) {
        this.sizeDocType = sizeDocType;
    }

    
    
    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }


    public String getDocCode() {
        return docCode;
    }

    public void setDocCode(String docCode) {
        this.docCode = docCode;
    }

    
    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public String getDocDesc() {
        return docDesc;
    }

    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }

    public int getDocItems() {
        return docItems;
    }

    public void setDocItems(int docItems) {
        this.docItems = docItems;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSumItems() {
        return sumItems;
    }

    public void setSumItems(double sumItems) {
        this.sumItems = sumItems;
    }
  
}
