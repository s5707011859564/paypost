package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.Branch;

public class BranchDaoImpl implements BranchDao {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Branch> getBranches() {
        String sql =
"SELECT \n" +
"    branch_id, branch_name\n" +
"FROM\n" +
"    branch\n" +
"ORDER BY branch_id ASC";
        List<Branch> result = jdbcTemplate.query(sql, new RowMapper<Branch>() {
            @Override
            public Branch mapRow(ResultSet rs, int i) throws SQLException {
                Branch o = new Branch();
                o.setBranchId(rs.getInt("branch_id"));
                o.setBranchName(rs.getString("branch_name"));
                return o;
            }
        });
        return result;
    }

    @Override
    public Branch getBranch(int id) {
        String sql =
"SELECT \n" +
"    branch_id, branch_name\n" +
"FROM\n" +
"    branch where branch_id = :id";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        Branch result = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<Branch>() {
            @Override
            public Branch mapRow(ResultSet rs, int i) throws SQLException {
                Branch o = new Branch();
                o.setBranchId(rs.getInt("branch_id"));
                o.setBranchName(rs.getString("branch_name"));
                return o;
            }
        });
        return result;
    }
    
}
