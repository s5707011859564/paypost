package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.ApplicationUser;
import th.ac.kmutnb.it.ase.paypost.dto.User;

/**
 *
 * @author Panit
 */
public interface UserDao {
    public void createUser(User user);
    public void updateUser(User user);
    public void deleteUser(String username);
    public User getUser(String username);
    public ApplicationUser getApplicationUser(String username);
    public boolean checkPassword(String username, String password);
    public boolean changePassword(String username, String password);
    public boolean enableUser(String username);
    public boolean disableUser(String username);
    public List<User> getUsers();
}
