package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.Province;

public interface ProvinceDao {

    public List<Province> getProvinces();

    public Province getProvince(int id);
}
