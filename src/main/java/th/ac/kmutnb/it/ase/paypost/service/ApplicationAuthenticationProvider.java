package th.ac.kmutnb.it.ase.paypost.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import th.ac.kmutnb.it.ase.paypost.dao.UserDao;

/**
 *
 * @author Panit
 */
public class ApplicationAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider{

    private UserDao userDao;
    
    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        String password = (String) authentication.getCredentials();
        if (userDao.checkPassword(username, password)) {
            return userDao.getApplicationUser(username);
        } else {
            throw new BadCredentialsException("Bad credentials.");
        }
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }
    
}
