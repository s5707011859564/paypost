/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstPost;

/**
 *
 * @author Administrator
 */
public interface PostMstPostDao {

    public void createPostMstPost(PostMstPost obj);

    public void updatePostMstPost(PostMstPost obj);

    public void deletePostMstPost(int postId);

    public PostMstPost getPostMstPost(int postId);

    public List<PostMstPost> listPostMstPost();
}
