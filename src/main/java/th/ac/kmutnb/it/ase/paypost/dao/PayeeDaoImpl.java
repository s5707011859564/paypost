package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.Payee;

public class PayeeDaoImpl implements PayeeDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Payee> getPayees() {
        String sql =
"SELECT \n" +
"    PAY_TYPE_ID, PAY_RECEIVER_NAME\n" +
"FROM\n" +
"    pay_mst_receiver\n" +
"ORDER BY PAY_TYPE_ID ASC";
        List<Payee> result = jdbcTemplate.query(sql, new RowMapper<Payee>() {
            @Override
            public Payee mapRow(ResultSet rs, int i) throws SQLException {
                Payee o = new Payee();
                o.setPayeeId(rs.getInt("PAY_TYPE_ID"));
                o.setPayeeName(rs.getString("PAY_RECEIVER_NAME"));
                return o;
            }
        });
        return result;
    }

}
