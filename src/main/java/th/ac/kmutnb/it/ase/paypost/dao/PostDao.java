/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;
import th.ac.kmutnb.it.ase.paypost.dto.Post;

import java.util.List;

/**
 *
 * @author metanonj
 */
public interface PostDao {
    public void createPost(Post post);
    public void updatePost(Post post);
    public void deletePost(int postId);
    public Post getPost(int postId);
    public List<Post> getPosts();
}
