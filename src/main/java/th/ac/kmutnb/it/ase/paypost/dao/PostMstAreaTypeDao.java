/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstAreaType;

/**
 *
 * @author Administrator
 */
public interface PostMstAreaTypeDao {

    public void createPostMstAreaType(PostMstAreaType obj);

    public void updatePostMstAreaType(PostMstAreaType obj);

    public void deletePostMstAreaType(int areaTypeId);

    public PostMstAreaType getPostMstAreaType(int areaTypeId);

    public List<PostMstAreaType> listPostMstAreaType();
}
