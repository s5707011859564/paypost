/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import th.ac.kmutnb.it.ase.paypost.dao.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstServiceFee;

/**
 *
 * @author pradoem
 * ค่าธรรมเนียม
 * 
 * PostMstServiceFeeDao
 * 
 * PostMstServiceFee
 * 
 */

@Component
@ManagedBean
@ViewScoped
public class PostMstServiceFeeView  implements Serializable{
    private static final long serialVersionUID = -2389786458972938210L;
    
    //===================================
    @Autowired
    private PostMstServiceFeeDao postMstServiceFeeDao;
    public void setPostMstServiceFeeDao(PostMstServiceFeeDao postMstServiceFeeDao) {
        this.postMstServiceFeeDao = postMstServiceFeeDao;
    }
    
    /*@Autowired
    private PostMstProductServiceDao postMstProductServiceDao;
    public void setPostMstProductServiceDao(PostMstProductServiceDao postMstProductServiceDao) {
        this.postMstProductServiceDao = postMstProductServiceDao;
    }*/
    //===================================
    private  List<PostMstServiceFee> listPostMstServiceFee;
    private PostMstServiceFee postMstServiceFeeObj;
    private Map<String,String> productTypeDDL;
    private Map<String,String> deliveryTypeDDL;
    private boolean editChecker = false;
    private String userLogin;

    public List<PostMstServiceFee> getListPostMstServiceFee() {
        return listPostMstServiceFee;
    }

    public void setListPostMstServiceFee(List<PostMstServiceFee> listPostMstServiceFee) {
        this.listPostMstServiceFee = listPostMstServiceFee;
    }

    public PostMstServiceFee getPostMstServiceFeeObj() {
        return postMstServiceFeeObj;
    }

    public void setPostMstServiceFeeObj(PostMstServiceFee postMstServiceFeeObj) {
        this.postMstServiceFeeObj = postMstServiceFeeObj;
    }

    public Map<String, String> getProductTypeDDL() {
        return productTypeDDL;
    }

    public void setProductTypeDDL(Map<String, String> productTypeDDL) {
        this.productTypeDDL = productTypeDDL;
    }

    public Map<String, String> getDeliveryTypeDDL() {
        return deliveryTypeDDL;
    }

    public void setDeliveryTypeDDL(Map<String, String> deliveryTypeDDL) {
        this.deliveryTypeDDL = deliveryTypeDDL;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public String getUserLogin() {
           return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

     //--------------Method Action
    @PostConstruct
    public void init() {
        doReloadData();

    }

    //Reload Data
     public void doReloadData() {
      listPostMstServiceFee = postMstServiceFeeDao.listPostMstServiceFee();
     }
    /****************
     * Clear
     */
    public void doClear() {
      postMstServiceFeeObj = null;
    }

    /** *************
     * Save/update
     */
    public void doSaveAction() {
       // System.out.println("==== doSaveAction ==========");
      //  System.out.println("id ==== " +postMstProductObj.getPsId());

        postMstServiceFeeObj.setCreateBy(this.getUserLogin());
        postMstServiceFeeObj.setUpdateBy(this.getUserLogin());
         if (!editChecker) {
                postMstServiceFeeDao.createPostMstServiceFee(postMstServiceFeeObj);
         } else {
               postMstServiceFeeDao.updatePostMstServiceFee(postMstServiceFeeObj);
         }
         editChecker = false;
         doReloadData();
         doClear();
                
       // System.out.println("===== doSaveAction : Complete  =====");
    }

    public void doDeleteAction(int argsId) {
       //System.out.println("===== doDeleteAction : Start  =====");
       //System.out.println("===== areaTypeId :" + argsId);
       
       postMstServiceFeeDao.deletePostMstServiceFee(argsId);
        
        doReloadData();

         editChecker = false;
         addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
         //System.out.println("===== Delete : Complete  =====");
    }

    public void doEditAction(int argsId) {
     //   System.out.println("===== doEditAction : Start  =====");
      //  System.out.println("===== areaTypeId :" + argsId);
        
       postMstServiceFeeObj = postMstServiceFeeDao.getPostMstServiceFee(argsId);
       productTypeDDL = postMstServiceFeeDao.listMapPostMstProductType();
       deliveryTypeDDL = postMstServiceFeeDao.listMapPostMstDeliveryType();

         editChecker = true;
         //System.out.println("===== doEditAction : Complete  =====");
    }

    public void doAddForm() {
        editChecker = false;
        doClear();
       postMstServiceFeeObj = new PostMstServiceFee();
       productTypeDDL = postMstServiceFeeDao.listMapPostMstProductType();
       deliveryTypeDDL = postMstServiceFeeDao.listMapPostMstDeliveryType();
      // System.out.println("==== doAddForm ==========");
    }

    public void handleDialogClose() {
      //  System.out.println("========Clase Dialog============");
         postMstServiceFeeObj = new PostMstServiceFee();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }   
 
    
}
