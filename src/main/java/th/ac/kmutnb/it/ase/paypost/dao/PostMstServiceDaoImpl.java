/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstService;

/**
 *
 * @author imetanon
 */
public class PostMstServiceDaoImpl implements PostMstServiceDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
	jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void createPostMstService(PostMstService postMstService) {	
	String sql
		= "INSERT INTO post_mst_service (\n"
		+ "    post_sv_name, post_sv_fixcost, post_sv_weightcost\n"
		+ ") VALUE (\n"
		+ ":postServiceName,:postServiceFixCost,:postServiceWeightCost)";

	Map<String, Object> parameters = new HashMap<>();
	parameters.put("postServiceName", postMstService.getServiceName());
	parameters.put("postServiceFixCost", postMstService.getFixCost());
	parameters.put("postServiceWeightCost", postMstService.getWeightCost());
	
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public void updatePostMstService(PostMstService postMstService) {
	String sql
		= "UPDATE post_mst_service SET\n"
		+ "    post_sv_name = :postServiceName,\n"
		+" post_sv_fixcost = :postServiceFixCost,\n"
		+" post_sv_weightcost = :postServiceWeightCost\n"
		+ "WHERE post_sv_id = :postServiceId";

	Map<String, Object> parameters = new HashMap<>();
	parameters.put("postServiceId", postMstService.getServiceId());
	parameters.put("postServiceName", postMstService.getServiceName());
	parameters.put("postServiceFixCost", postMstService.getFixCost());
	parameters.put("postServiceWeightCost", postMstService.getWeightCost());
	
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public void deletePostMstService(int postMstServiceId) {
	String sql
		= "DELETE FROM post_mst_service \n"
		+ "WHERE\n"
		+ "    post_sv_id = :postServiceId";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("postServiceId", postMstServiceId);
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public PostMstService getPostMstService(int postMstServiceId) {
	String sql
		= "SELECT \n"
		+ "    post_sv_id, post_sv_name, post_sv_fixcost, post_sv_weightcost\n"
		+ "FROM\n"
		+ "    post_mst_service\n"
		+ "WHERE\n"
		+ "    post_sv_id = :id\n"
		+ "ORDER BY post_sv_id ASC";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("id", postMstServiceId);
	PostMstService result = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<PostMstService>() {
	    @Override
	    public PostMstService mapRow(ResultSet rs, int i) throws SQLException {
		PostMstService o = new PostMstService();
		o.setServiceId(rs.getInt("post_sv_id"));
		o.setServiceName(rs.getString("post_sv_name"));
		o.setFixCost(rs.getDouble("post_sv_fixcost"));
		o.setWeightCost(rs.getDouble("post_sv_weightcost"));
		return o;
	    }
	});
	return result;
    }

    @Override
    public List<PostMstService> getPostMstServices() {
	String sql
		= "SELECT \n"
		+ "    post_sv_id, post_sv_name, post_sv_fixcost, post_sv_weightcost\n"
		+ "FROM\n"
		+ "    post_mst_service\n"
		+ "ORDER BY post_sv_id";
	List<PostMstService> result = jdbcTemplate.query(sql, new RowMapper<PostMstService>() {

	    @Override
	    public PostMstService mapRow(ResultSet rs, int i) throws SQLException {
		PostMstService o = new PostMstService();
		o.setServiceId(rs.getInt("post_sv_id"));
		o.setServiceName(rs.getString("post_sv_name"));
		o.setFixCost(rs.getDouble("post_sv_fixcost"));
		o.setWeightCost(rs.getDouble("post_sv_weightcost"));
		return o;
	    }

	});
	return result;
    }

}
