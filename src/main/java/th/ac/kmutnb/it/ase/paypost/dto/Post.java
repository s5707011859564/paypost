/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author imetanon
 */
public class Post implements Serializable{
    private static final long serialVersionUID = 3285611735631671931L;
    
    private int postId;
    private PostContact sender;
    private List<PostService> postServiceList;
    private double otherPrice;
    private double totalPrice;

    public PostContact getSender() {
	return sender;
    }

    public void setSender(PostContact sender) {
	this.sender = sender;
    }

    public List<PostService> getPostServiceList() {
	return postServiceList;
    }

    public void setPostServiceList(List<PostService> postServiceList) {
	this.postServiceList = postServiceList;
    }

    public double getTotalPrice() {
	return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
	this.totalPrice = totalPrice;
    }

    public double getOtherPrice() {
        return otherPrice;
    }

    public void setOtherPrice(double otherPrice) {
        this.otherPrice = otherPrice;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

  
    
    
}
