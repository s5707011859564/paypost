/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author Administrator
 */
public class Utilizer {

    public static String NowByCalendar(String dateFormat) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());
    }

    public static String GenNextId(int b) {
        String temp = "" + b;
        String newSp_id;
        switch (temp.length()) {
            // case 1: newSp_id="00000"+temp; break; // case 2: newSp_id="0000"+temp; break; //case 1: newSp_id="000"+temp; break;
            case 1:
                newSp_id = "0" + temp;
                break;
            default:
                newSp_id = temp;
        }
        return newSp_id;
    }

    public static String replaceNull(String s) {
        if (s == null) {
            s = "";
        }
        return s;
    }

    public static String replaceNull(String s, String tarGet) {
        if (s == null) {
            s = tarGet;
        }
        return s;
    }

    public static boolean isValueStrAndObj(String str) throws Exception {
        if ((str == null) || str.equals("")) {
            return false;
        } else {
            return true;
        }
    }

}
