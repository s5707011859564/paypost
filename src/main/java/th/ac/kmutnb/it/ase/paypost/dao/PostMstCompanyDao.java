/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstCompany;

/**
 *
 * @author Administrator
 */
public interface PostMstCompanyDao {
     public void createPostMstCompany(PostMstCompany obj);

    public void updatePostMstCompany(PostMstCompany obj);

    public void deletePostMstCompany(String companyCode);

    public PostMstCompany getPostMstCompany(String companyCode);

    public List<PostMstCompany> listPostMstCompany();
}
