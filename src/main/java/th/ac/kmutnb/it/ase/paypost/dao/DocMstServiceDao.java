/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import java.util.Map;
import th.ac.kmutnb.it.ase.paypost.dto.DocMstService;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstDeliveryType;

/**
 *
 * @author Administrator
 */
public interface DocMstServiceDao {
    
    public void createDocMstService(PostMstDeliveryType obj);
    public void updateDocMstService(PostMstDeliveryType obj);
    public void deleteDocMstService(int docId);
    public DocMstService  getDocMstService(String docCode);
    public List<DocMstService> listDocMstService(); 
    public Map<String,String>  listMapDocTypeService(String docType);
    public Map<String,String>  listMapDocTypeServiceForPaper(String docType);
    
}
