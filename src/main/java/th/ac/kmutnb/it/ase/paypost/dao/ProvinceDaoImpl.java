package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.Province;

public class ProvinceDaoImpl implements ProvinceDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Province> getProvinces() {
        String sql = 
"SELECT \n" +
"    PROVINCE_ID, PROVINCE_CODE, PROVINCE_NAME\n" +
"FROM\n" +
"    provinces\n" +
"ORDER BY PROVINCE_CODE ASC";
        List<Province> result = jdbcTemplate.query(sql, new RowMapper<Province>() {
            @Override
            public Province mapRow(ResultSet rs, int i) throws SQLException {
                Province o = new Province();
                o.setId(rs.getInt("PROVINCE_ID"));
                o.setCode(rs.getString("PROVINCE_CODE"));
                o.setName(rs.getString("PROVINCE_NAME"));
                return o;
            }
        });
        return result;
    }

    @Override
    public Province getProvince(int id) {
        String sql =
"SELECT \n" +
"    PROVINCE_ID, PROVINCE_CODE, PROVINCE_NAME\n" +
"FROM\n" +
"    provinces\n" +
"WHERE\n" +
"    PROVINCE_ID = :id\n" +
"ORDER BY PROVINCE_CODE ASC";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        Province result = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<Province>() {
            @Override
            public Province mapRow(ResultSet rs, int i) throws SQLException {
                Province o = new Province();
                o.setId(rs.getInt("PROVINCE_ID"));
                o.setCode(rs.getString("PROVINCE_CODE"));
                o.setName(rs.getString("PROVINCE_NAME"));
                return o;
            }
        });
        return result;
    }

}
