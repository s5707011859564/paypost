/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author imetanon
 */
public class PostProduct implements Serializable{
    private static final long serialVersionUID = -6978003902708054340L;
    
    private int postProductId;
    private int productId;
    private int amount;
    private int serviceId;

    public int getProductId() {
	return productId;
    }

    public void setProductId(int productId) {
	this.productId = productId;
    }


    public int getAmount() {
	return amount;
    }

    public void setAmount(int amount) {
	this.amount = amount;
    }

    public int getServiceId() {
	return serviceId;
    }

    public void setServiceId(int serviceId) {
	this.serviceId = serviceId;
    }

    public int getPostProductId() {
	return postProductId;
    }

    public void setPostProductId(int postProductId) {
	this.postProductId = postProductId;
    }

    @Override
    public String toString() {
	return "PostProduct{" + "postProductId=" + postProductId + ", productId=" + productId + ", amount=" + amount + ", serviceId=" + serviceId + '}';
    }
    
    
    
    
}
