package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.Payee;

public interface PayeeDao {

    public List<Payee> getPayees();
}
