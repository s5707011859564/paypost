package th.ac.kmutnb.it.ase.paypost.view;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import th.ac.kmutnb.it.ase.paypost.dto.PostInputItem;

@ManagedBean
@ViewScoped
public class PostInputView {
    private static final long serialVersionUID = -5973845939229056729L;
    private List<PostInputItem> items;

    public List<PostInputItem> getItems() {
        return items;
    }
    
    @PostConstruct
    public void init() {
        items = new ArrayList<>();
    }
}
