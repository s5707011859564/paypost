/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class DocTransDetail implements  Serializable{
    private static final long serialVersionUID = 233327026078684021L;
    private int transDetId;
    private String docTransId;
    private String docTransDesc;
    private int unit;
    private String UOM;
    private double price;
    private double total;
    private double disCount;
    private double moneyInCome;
    private double moneyReturn;
    private String remark;
    private String createDate;
    private String createBy;
    private String updateDate;
    private String updateBy;

    public int getTransDetId() {
        return transDetId;
    }

    public void setTransDetId(int transDetId) {
        this.transDetId = transDetId;
    }

    public String getDocTransId() {
        return docTransId;
    }

    public void setDocTransId(String docTransId) {
        this.docTransId = docTransId;
    }

    public String getDocTransDesc() {
        return docTransDesc;
    }

    public void setDocTransDesc(String docTransDesc) {
        this.docTransDesc = docTransDesc;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public String getUOM() {
        return UOM;
    }

    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDisCount() {
        return disCount;
    }

    public void setDisCount(double disCount) {
        this.disCount = disCount;
    }

    public double getMoneyInCome() {
        return moneyInCome;
    }

    public void setMoneyInCome(double moneyInCome) {
        this.moneyInCome = moneyInCome;
    }

    public double getMoneyReturn() {
        return moneyReturn;
    }

    public void setMoneyReturn(double moneyReturn) {
        this.moneyReturn = moneyReturn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public String toString() {
        return "DocTransDetail{" + "transDetId=" + transDetId + ", docTransId=" + docTransId + ", docTransDesc=" + docTransDesc + ", unit=" + unit + ", UOM=" + UOM + ", price=" + price + ", total=" + total + ", disCount=" + disCount + ", moneyInCome=" + moneyInCome + ", moneyReturn=" + moneyReturn + ", remark=" + remark + ", createDate=" + createDate + ", createBy=" + createBy + ", updateDate=" + updateDate + ", updateBy=" + updateBy + '}';
    }
    
}
