/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.DocMstServiceDao;
import th.ac.kmutnb.it.ase.paypost.dao.DocTransDetailDao;
import th.ac.kmutnb.it.ase.paypost.dto.DocMstService;
import th.ac.kmutnb.it.ase.paypost.dto.DocMstTrans;
import th.ac.kmutnb.it.ase.paypost.dto.DocTransDetail;

/**
 *
 * @author Administrator
 */

@Component
@ManagedBean
@ViewScoped
public class TransMstDocView implements  Serializable{
    private static final long serialVersionUID = 1979107752585292618L;
     //------------Connection
     @Autowired
    private DocMstServiceDao docMstServiceDao;
    public void setPayMstReceiverDao(DocMstServiceDao docMstServiceDao) {
        this.docMstServiceDao = docMstServiceDao;
    }
    @Autowired
    private DocTransDetailDao docTransDetailDao;
    public void setDocTransDetailDao(DocTransDetailDao docTransDetailDao) {
        this.docTransDetailDao = docTransDetailDao;
    }
    //====================
    
    private List<DocMstTrans> listDocMstTrans;
    private  Map<String,String>  listMapItemsDDL;
    //private  Map<String,String>  listMapColorDDL; 
    private Map<String,String> listMapDetialDDL;
     private Map<String,String> listMapCopyDDL; //C=Coppy
     private Map<String,String> listMapScanDDL; //S=Scan
     private Map<String,String> listMapFaxDDL; //F=Fax
     private Map<String,String> listMapBookDDL; //B=Book
     private Map<String,String> listMapLamDDL; //L=lam

    private DocMstTrans docMstTransObj;
    private DocMstService docMstServiceObj;
    private DocTransDetail  docTransDetailObj;
    
    private String docType;
    private int isColor;
    private int isActive;
    private boolean editChecker = false;
    private String userName;
    
    //-----------------------
    private String docType1 = ""; 
    private String docTypeC = "C";//ถ่ายเอกสาร
    private String docTypeS = "S";
    private String docTypeF = "F";
    private String docTypeB = "B";
    private String docTypeL = "L";

    public DocTransDetail getDocTransDetailObj() {
        return docTransDetailObj;
    }

    public void setDocTransDetailObj(DocTransDetail docTransDetailObj) {
        this.docTransDetailObj = docTransDetailObj;
    }

    public DocMstService getDocMstServiceObj() {
        return docMstServiceObj;
    }

    public void setDocMstServiceObj(DocMstService docMstServiceObj) {
        this.docMstServiceObj = docMstServiceObj;
    }

    
    public Map<String, String> getListMapCopyDDL() {
        return listMapCopyDDL;
    }

    public void setListMapCopyDDL(Map<String, String> listMapCopyDDL) {
        this.listMapCopyDDL = listMapCopyDDL;
    }

    public Map<String, String> getListMapScanDDL() {
        return listMapScanDDL;
    }

    public void setListMapScanDDL(Map<String, String> listMapScanDDL) {
        this.listMapScanDDL = listMapScanDDL;
    }

    public Map<String, String> getListMapFaxDDL() {
        return listMapFaxDDL;
    }

    public void setListMapFaxDDL(Map<String, String> listMapFaxDDL) {
        this.listMapFaxDDL = listMapFaxDDL;
    }

    public Map<String, String> getListMapBookDDL() {
        return listMapBookDDL;
    }

    public void setListMapBookDDL(Map<String, String> listMapBookDDL) {
        this.listMapBookDDL = listMapBookDDL;
    }

    public Map<String, String> getListMapLamDDL() {
        return listMapLamDDL;
    }

    public void setListMapLamDDL(Map<String, String> listMapLamDDL) {
        this.listMapLamDDL = listMapLamDDL;
    }
    public Map<String, String> getListMapDetialDDL() {
        return listMapDetialDDL;
    }

    public void setListMapDetialDDL(Map<String, String> listMapDetialDDL) {
        this.listMapDetialDDL = listMapDetialDDL;
    }

    public int getIsColor() {
        return isColor;
    }

    public void setIsColor(int isColor) {
        this.isColor = isColor;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public Map<String, String> getListMapItemsDDL() {
        return listMapItemsDDL;
    }

    public void setListMapItemsDDL(Map<String, String> listMapItemsDDL) {
        this.listMapItemsDDL = listMapItemsDDL;
    }

    public List<DocMstTrans> getListDocMstTrans() {
        return listDocMstTrans;
    }

    public void setListDocMstTrans(List<DocMstTrans> listDocMstTrans) {
        this.listDocMstTrans = listDocMstTrans;
    }

    public DocMstTrans getDocMstTransObj() {
        return docMstTransObj;
    }

    public void setDocMstTransObj(DocMstTrans docMstTransObj) {
        this.docMstTransObj = docMstTransObj;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LinkedHashMap getListMapDetail(){
        listMapDetialDDL = new LinkedHashMap<String, String>();
        listMapDetialDDL.put("1/4", "1"); //label, value
        listMapDetialDDL.put("2/4", "2");
        listMapDetialDDL.put("3/4", "3");
        listMapDetialDDL.put("เต็มหน้า", "4");
        
        return (LinkedHashMap) listMapDetialDDL;
   }
    //--------------Method Action
    @PostConstruct
    public void init() {
        doReloadData();
    }

    //Reload Data
     public void doReloadData() {
         listDocMstTrans = new ArrayList();
     }
    /****************
     * Clear
     */
    public void doClear() {
       docMstTransObj = null;
       docMstServiceObj = null;
    }

     public void doSummarySaveAction() {
          System.out.println("==== doSummarySaveAction ==========");
          if(listDocMstTrans!=null && listDocMstTrans.size()>0){
            String AutoId = docTransDetailDao.GenAutoId();
            System.out.println("==== AutoId : "+AutoId);
            //1. Create 1 record in to table detail
            docMstTransObj.setDocAutoId(AutoId);
            docTransDetailDao.insertDocTransDetail(docMstTransObj);

            //2. Create  mutiple in to table docMstTrans
            docTransDetailDao.insertDocMstTrans(listDocMstTrans,AutoId);

            addMessage("จัดเก็บข้อมูลเรียบร้อยแล้ว  ");
            RequestContext context = RequestContext.getCurrentInstance();
            context.reset("docForm");
            doClear() ;
            listDocMstTrans = null;
          }else{
               addMessage("!!ยังไม่มีการเพิ่มรายการงานเอกสาร  ");
          }
     }
     
      public void doCancelAction() {
         System.out.println("==== doCancelAction ==========");
         doClear() ;
         listDocMstTrans = null;
          RequestContext context = RequestContext.getCurrentInstance();
          context.reset("docForm");
     }
    
    /***************
     * Save/update
     */
    public void doSaveAction() {
         int items = docMstTransObj.getDocItems();
         double priceO = docMstTransObj.getPrice();
        docMstServiceObj = docMstServiceDao.getDocMstService(docMstTransObj.getDocCode());
        docMstTransObj  = new DocMstTrans();
        docMstTransObj.setDocId(docMstServiceObj.getDocId());
        docMstTransObj.setDocCode(docMstServiceObj.getDocCode());
        //docMstTransObj.setColorCode("1");
        docMstTransObj.setPrice(docMstServiceObj.getDocPrice());
        //C,S
        if("C".equals(docType) || "S".equals(docType)){
            docMstTransObj.setDocDesc(docMstServiceObj.getDocPaper());
        }else if("O".equals(docType)){
            docMstTransObj.setDocDesc("ค่าบริการ");
            docMstTransObj.setPrice(priceO);
        }else{
             docMstTransObj.setDocDesc(docMstServiceObj.getDocCodeName());
        }
       docMstTransObj.setDocItems(items);
       
       listDocMstTrans.add(docMstTransObj);
       
       //Calulate Payment parameter
        double sumTotal = 0d;
        int cnt = 0;
        if(listDocMstTrans!=null && listDocMstTrans.size()>0){
                 for(DocMstTrans obj :listDocMstTrans){
                     cnt +=docMstTransObj.getDocItems();
                     
                     if(obj.getDocItems()>0){
                         sumTotal += obj.getDocItems()*obj.getPrice();
                     }else{
                         sumTotal +=obj.getPrice();
                     }
                 }
                docMstTransObj.setTotalPrice(sumTotal);
                docMstTransObj.setSumItems(cnt);
         } 
    }
    
    public void doDeleteAction(int argsId) {
        System.out.println("===== doDeleteAction : Start  ====="+argsId);
        
        if(listDocMstTrans!=null && listDocMstTrans.size()>0){
            int x = 0;
            for(DocMstTrans obj :listDocMstTrans){
                //docId
                if(obj.getDocId() == argsId){
                     listDocMstTrans.remove(x);
                      addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
                     System.out.println("===== Delete : Complete  =====");
	   break;
                }
                x++;
            }
              //Calulate Payment parameter
             double sumTotal = 0d;
              int cnt = 0;
             for(DocMstTrans obj :listDocMstTrans){
                     cnt +=docMstTransObj.getDocItems();                  
                     if(obj.getDocItems()>0){
                         sumTotal += obj.getDocItems()*obj.getPrice();
                     }else{
                         sumTotal +=obj.getPrice();
                     }
                 }
                docMstTransObj.setTotalPrice(sumTotal);
                docMstTransObj.setSumItems(cnt);
        }
    }
      
    public void doAddForm1() { //Copy form
        editChecker = false;
        doClear();
        docMstTransObj = new DocMstTrans();
        listMapItemsDDL =  docMstServiceDao.listMapDocTypeService("PG");
       listMapDetialDDL =  getListMapDetail();
       this.docType = "PG";
    }
    
    public void doAddForm2() { //Copy form
        editChecker = false;
        doClear();
        docMstTransObj = new DocMstTrans();
        listMapCopyDDL= docMstServiceDao.listMapDocTypeServiceForPaper(docTypeC);
       this.docType = "C";
    }
    public void doAddForm3() { //Scan form
        editChecker = false;
        doClear();
        docMstTransObj = new DocMstTrans();
        listMapScanDDL = docMstServiceDao.listMapDocTypeServiceForPaper(docTypeS);
       this.docType = "S";
    }
    
      public void doAddForm4() { //Fax form
        editChecker = false;
        doClear();
        docMstTransObj = new DocMstTrans();
        listMapFaxDDL = docMstServiceDao.listMapDocTypeService(docTypeF);
       this.docType = "F";
    }
      
    public void doAddForm5() { //BOOK form
        editChecker = false;
        doClear();
        docMstTransObj = new DocMstTrans();
        listMapBookDDL= docMstServiceDao.listMapDocTypeService(docTypeB);
       this.docType = "B";
    }
    public void doAddForm6() { //LAM form
        editChecker = false;
        doClear();
        docMstTransObj = new DocMstTrans();
       listMapLamDDL= docMstServiceDao.listMapDocTypeService(docTypeL);
       this.docType = "B";
    }  
    
     public void doAddForm7() { //Other
        editChecker = false;
        doClear();
        docMstTransObj = new DocMstTrans();
        docMstTransObj.setDocCode("0");
        docMstTransObj.setDocItems(1);
        this.docType = "O";
    } 
    

    public void handleDialogClose() {
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }   
    
}
