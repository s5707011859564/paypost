/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstDeliveryTypeDao;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstDeliveryType;

/**
 *
 * @author pradoem ประเภทการส่ง
 */
@Component
@ManagedBean
@ViewScoped
public class PostMstDeliveryTypeView implements Serializable {

    private static final long serialVersionUID = -4344332201358174506L;

    //--------------------Connecton
    @Autowired
    private PostMstDeliveryTypeDao postMstDeliveryTypeDao;

    public void setPostMstDeliveryTypeDao(PostMstDeliveryTypeDao postMstDeliveryTypeDao) {
        this.postMstDeliveryTypeDao = postMstDeliveryTypeDao;
    }
    //---------------------------

    private List<PostMstDeliveryType> listMstDeliveryType;
    private PostMstDeliveryType postMstDeliveryType;
    private int deliveryTypeId;
    private String deliveryTypeCode;
    private String deliveryTypeDescTH;
    private String deliveryTypeDescEN;
    private int isActive;
    private boolean editChecker = false;
    private String userName;

    public List<PostMstDeliveryType> getListMstDeliveryType() {
        return listMstDeliveryType;
    }

    public void setListMstDeliveryType(List<PostMstDeliveryType> listMstDeliveryType) {
        this.listMstDeliveryType = listMstDeliveryType;
    }

    public PostMstDeliveryType getPostMstDeliveryType() {
        return postMstDeliveryType;
    }

    public void setPostMstDeliveryType(PostMstDeliveryType postMstDeliveryType) {
        this.postMstDeliveryType = postMstDeliveryType;
    }

    public int getDeliveryTypeId() {
        return deliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }

    public String getDeliveryTypeCode() {
        return deliveryTypeCode;
    }

    public void setDeliveryTypeCode(String deliveryTypeCode) {
        this.deliveryTypeCode = deliveryTypeCode;
    }

    public String getDeliveryTypeDescTH() {
        return deliveryTypeDescTH;
    }

    public void setDeliveryTypeDescTH(String deliveryTypeDescTH) {
        this.deliveryTypeDescTH = deliveryTypeDescTH;
    }

    public String getDeliveryTypeDescEN() {
        return deliveryTypeDescEN;
    }

    public void setDeliveryTypeDescEN(String deliveryTypeDescEN) {
        this.deliveryTypeDescEN = deliveryTypeDescEN;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public String getUserName() {
        return userName = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    //--------------Method Action
    @PostConstruct
    public void init() {
        doReloadData();

    }

    //Reload Data
     public void doReloadData() {
           listMstDeliveryType = postMstDeliveryTypeDao.listPostMstDeliveryType();
     }
    /****************
     * Clear
     */
    public void doClear() {
        postMstDeliveryType = null;
        deliveryTypeId = 0;
        deliveryTypeCode = null;
        deliveryTypeDescTH = null;
        deliveryTypeDescEN = null;
        isActive = -1;
    }

    /**
     * *************
     * Save/update
     */
    public void doSaveAction() {
        //System.out.println("==== doSaveAction ==========");
       // System.out.println("====" +deliveryTypeId);
        
        PostMstDeliveryType obj = new PostMstDeliveryType();
        obj.setDeliveryTypeId(deliveryTypeId);
        obj.setDeliveryTypeCode(deliveryTypeCode);
        obj.setDeliveryTypeDescTH(deliveryTypeDescTH);
        obj.setDeliveryTypeDescEN(deliveryTypeDescEN);
        obj.setIsActive(isActive);
        obj.setCreateBy(this.getUserName());
        obj.setUpdateBy(this.getUserName());

         if (!editChecker) {
                postMstDeliveryTypeDao.createPostMstDeliveryType(obj);
         } else {
               postMstDeliveryTypeDao.updatePostMstDeliveryType(obj);
         }

         editChecker = false;

         doReloadData();

         doClear();
        // System.out.println("===== doSaveAction : Complete  =====");
    }

    public void doDeleteAction(int argsId) {
      //  System.out.println("===== doDeleteAction : Start  =====");
      //  System.out.println("===== areaTypeId :" + argsId);

        postMstDeliveryTypeDao.deletePostMstDeliveryType(argsId);
        
        doReloadData();

         editChecker = false;
         addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
       //  System.out.println("===== Delete : Complete  =====");
    }

    public void doEditAction(int argsId) {
        //System.out.println("===== doEditAction : Start  =====");
       // System.out.println("===== areaTypeId :" + argsId);
        
         PostMstDeliveryType obj = postMstDeliveryTypeDao.getPostMstDeliveryType(argsId);
         this.deliveryTypeId = obj.getDeliveryTypeId();
         this.deliveryTypeCode = obj.getDeliveryTypeCode();
         this.deliveryTypeDescTH = obj.getDeliveryTypeDescTH();
         this.deliveryTypeDescEN = obj.getDeliveryTypeDescEN();
         this.isActive = obj.getIsActive();

         editChecker = true;
        // System.out.println("===== doEditAction : Complete  =====");
    }

    public void doAddForm() {
        editChecker = false;
        doClear();
        //System.out.println("==== doAddForm ==========");
    }

    public void handleDialogClose() {
      //  System.out.println("========Clase Dialog============");
        // this.mstAreaType = new PostMstAreaType();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
