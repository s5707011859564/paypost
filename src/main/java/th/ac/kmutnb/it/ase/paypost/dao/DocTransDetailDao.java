/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.DocMstTrans;
import th.ac.kmutnb.it.ase.paypost.dto.DocTransDetail;

/**
 *
 * @author Administrator
 */
public interface DocTransDetailDao {
     public void createDocTransDetail(DocTransDetail obj);
     public void insertDocMstTrans(List<DocMstTrans> objList,String autoId);
     public void insertDocTransDetail(DocMstTrans obj);
     public String GenAutoId();
     
    public void updateDocTransDetail(DocTransDetail obj);

    public void deleteDocTransDetail(int docId);

    public DocTransDetail  getDocTransDetail(String docCode);

    public List<DocTransDetail> listDocTransDetail();
}
