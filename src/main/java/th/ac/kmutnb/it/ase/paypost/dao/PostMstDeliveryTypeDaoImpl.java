/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstDeliveryType;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class PostMstDeliveryTypeDaoImpl implements PostMstDeliveryTypeDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    //---------------------------------------

    @Override
    public void createPostMstDeliveryType(PostMstDeliveryType obj) {
        /*
         INSERT INTO paypost.post_mst_delivery_type
         (DELIVERY_TYPE_CODE, DELIVERY_TYPE_DESC_TH, DELIVERY_TYPE_DESC_EN, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE) 
         VALUES ('DELIVERY_TYPE_CODE', 'DELIVERY_TYPE_DESC_TH', 'DELIVERY_TYPE_DESC_EN', ISACTIVE, 'CREATE_BY', 'CREATE_DATE', 'UPDATE_BY', 'UPDATE_DATE');
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.post_mst_delivery_type  ")
                .append(" (DELIVERY_TYPE_CODE, DELIVERY_TYPE_DESC_TH, DELIVERY_TYPE_DESC_EN, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE)    ")
                .append(" VALUES ( :DELIVERY_TYPE_CODE , :DELIVERY_TYPE_DESC_TH  , :DELIVERY_TYPE_DESC_EN , :ISACTIVE, :CREATE_BY , NOW(), null, null ) ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DELIVERY_TYPE_CODE", obj.getDeliveryTypeCode());
        parameters.put("DELIVERY_TYPE_DESC_TH", obj.getDeliveryTypeDescTH());
        parameters.put("DELIVERY_TYPE_DESC_EN", obj.getDeliveryTypeDescEN());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("CREATE_BY", obj.getCreateBy());

        //System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert post_mst_delivery_type OK ");
    }

    @Override
    public void updatePostMstDeliveryType(PostMstDeliveryType obj) {
        /*
         UPDATE paypost.post_mst_delivery_type 
         SET DELIVERY_TYPE_CODE = 'DELIVERY_TYPE_CODE' ,
         DELIVERY_TYPE_DESC_TH = 'DELIVERY_TYPE_DESC_TH',
         DELIVERY_TYPE_DESC_EN = 'DELIVERY_TYPE_DESC_EN', 
         ISACTIVE = ISACTIVE, 
         CREATE_BY = 'CREATE_BY', 
         CREATE_DATE = 'CREATE_DATE', 
         UPDATE_BY = 'UPDATE_BY', 
         UPDATE_DATE = 'UPDATE_DATE' 
         WHERE -- Please complet;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.post_mst_delivery_type   ")
                .append(" SET DELIVERY_TYPE_CODE = :DELIVERY_TYPE_CODE , ")
                .append("  DELIVERY_TYPE_DESC_TH = :DELIVERY_TYPE_DESC_TH , ")
                .append("  DELIVERY_TYPE_DESC_EN = :DELIVERY_TYPE_DESC_EN , ")
                .append("  ISACTIVE = :ISACTIVE,  ")
                .append("  UPDATE_BY = :UPDATE_BY , ")
                .append("  UPDATE_DATE = NOW()  ")
                .append(" WHERE  DELIVERY_TYPE_ID = :DELIVERY_TYPE_ID  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DELIVERY_TYPE_CODE", obj.getDeliveryTypeCode());
        parameters.put("DELIVERY_TYPE_DESC_TH", obj.getDeliveryTypeDescTH());
        parameters.put("DELIVERY_TYPE_DESC_EN", obj.getDeliveryTypeDescEN());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("UPDATE_BY", obj.getUpdateBy());
        //Where
        parameters.put("DELIVERY_TYPE_ID", obj.getDeliveryTypeId());
       // System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  post_mst_delivery_type OK ");
    }

    @Override
    public void deletePostMstDeliveryType(int deliveryTypeId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.post_mst_delivery_type   ")
                .append(" WHERE  DELIVERY_TYPE_ID = :DELIVERY_TYPE_ID  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DELIVERY_TYPE_ID", deliveryTypeId);

        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("======Delete :" + deliveryTypeId + "  : successfull");
    }

    @Override
    public PostMstDeliveryType getPostMstDeliveryType(int deliveryTypeId) {
        /*
         SELECT DELIVERY_TYPE_ID, DELIVERY_TYPE_CODE, DELIVERY_TYPE_DESC_TH, DELIVERY_TYPE_DESC_EN, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE 
         FROM paypost.post_mst_delivery_type;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  DELIVERY_TYPE_ID, DELIVERY_TYPE_CODE, DELIVERY_TYPE_DESC_TH, DELIVERY_TYPE_DESC_EN, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE  ")
                .append(" FROM paypost.post_mst_delivery_type ")
                .append(" WHERE DELIVERY_TYPE_ID = :DELIVERY_TYPE_ID ");

       // System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DELIVERY_TYPE_ID", deliveryTypeId);

        PostMstDeliveryType deliveryTypeObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PostMstDeliveryType>() {
            @Override
            public PostMstDeliveryType mapRow(ResultSet rs, int rowNumber) throws SQLException {
                PostMstDeliveryType obj = new PostMstDeliveryType();
                obj.setDeliveryTypeId(rs.getInt("DELIVERY_TYPE_ID"));
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                obj.setDeliveryTypeDescTH(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_TH")));
                obj.setDeliveryTypeDescEN(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_EN")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                return obj;
            }
        });
        return deliveryTypeObj;
    }

    @Override
    public List<PostMstDeliveryType> listPostMstDeliveryType() {
        /*
         SELECT DELIVERY_TYPE_ID, DELIVERY_TYPE_CODE, DELIVERY_TYPE_DESC_TH, DELIVERY_TYPE_DESC_EN, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE 
         FROM paypost.post_mst_delivery_type;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  DELIVERY_TYPE_ID, DELIVERY_TYPE_CODE, DELIVERY_TYPE_DESC_TH, DELIVERY_TYPE_DESC_EN, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE  ")
                .append(" FROM paypost.post_mst_delivery_type ")
                .append(" Order by  DELIVERY_TYPE_ID ");

       // System.out.println("======Sql :" + sql);
        List<PostMstDeliveryType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstDeliveryType>() {
            @Override
            public PostMstDeliveryType mapRow(ResultSet rs, int rowNumber) throws SQLException {
                PostMstDeliveryType obj = new PostMstDeliveryType();
                obj.setDeliveryTypeId(rs.getInt("DELIVERY_TYPE_ID"));
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                obj.setDeliveryTypeDescTH(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_TH")));
                obj.setDeliveryTypeDescEN(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_EN")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                return obj;
            }
        });
        return listResult;
    }

}
