/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 * ข้อมูลบริษัท
 */
public class PostMstCompany implements Serializable {
    private static final long serialVersionUID = -285777249960924681L;

    private String companyCode;
    private String companyName;
    private String companyAddress;
    private String companyOpen1;
    private String companyOpen2;
    private String companyContact;
    private String companyPhone;
    private String companyFax;
    private String remark;
    private int isActive; 

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyOpen1() {
        return companyOpen1;
    }

    public void setCompanyOpen1(String companyOpen1) {
        this.companyOpen1 = companyOpen1;
    }

    public String getCompanyOpen2() {
        return companyOpen2;
    }

    public void setCompanyOpen2(String companyOpen2) {
        this.companyOpen2 = companyOpen2;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyFax() {
        return companyFax;
    }

    public void setCompanyFax(String companyFax) {
        this.companyFax = companyFax;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "PostMstCompany{" + "companyCode=" + companyCode + ", companyName=" + companyName + ", companyAddress=" + companyAddress + ", companyOpen1=" + companyOpen1 + ", companyOpen2=" + companyOpen2 + ", companyContact=" + companyContact + ", companyPhone=" + companyPhone + ", companyFax=" + companyFax + ", remark=" + remark + ", isActive=" + isActive + '}';
    }

    
    
}
