/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstCompany;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class PostMstCompanyDaoImpl implements PostMstCompanyDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //---------------------------------------

    @Override
    public void createPostMstCompany(PostMstCompany obj) {
        /*
         INSERT INTO paypost.post_company
         (COMPANY_CODE, COMPANY_NAME, COMPANY_ADDRESS, COMPANY_OPEN1, COMPANY_OPEN2, COMPANY_CONTACT, COMPANY_PHONE, COMPANY_FAX, REMARK, ISACTIVE) 
         VALUES ('COMPANY_CODE', 'COMPANY_NAME', 'COMPANY_ADDRESS', 'COMPANY_OPEN1', 'COMPANY_OPEN2', 'COMPANY_CONTACT', 'COMPANY_PHONE', 'COMPANY_FAX', 'REMARK', ISACTIVE);
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.post_company  ")
                .append(" (COMPANY_CODE, COMPANY_NAME, COMPANY_ADDRESS, COMPANY_OPEN1, COMPANY_OPEN2, COMPANY_CONTACT, COMPANY_PHONE, COMPANY_FAX, REMARK, ISACTIVE)   ")
                .append(" VALUES (:COMPANY_CODE  , :COMPANY_NAME ,  :COMPANY_ADDRESS   , :COMPANY_OPEN1 , :COMPANY_OPEN2 , :COMPANY_CONTACT , :COMPANY_PHONE , :COMPANY_FAX , :REMARK , :ISACTIVE ) ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("COMPANY_CODE", obj.getCompanyCode());
        parameters.put("COMPANY_NAME", obj.getCompanyName());
        parameters.put("COMPANY_ADDRESS", obj.getCompanyAddress());
        parameters.put("COMPANY_OPEN1", obj.getCompanyOpen1());
        parameters.put("COMPANY_OPEN2", obj.getCompanyOpen2());
        parameters.put("COMPANY_CONTACT", obj.getCompanyContact());
        parameters.put("COMPANY_PHONE", obj.getCompanyPhone());
        parameters.put("COMPANY_FAX", obj.getCompanyFax());
        parameters.put("REMARK", obj.getRemark());
        parameters.put("ISACTIVE", obj.getRemark());

       // System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert post_company OK ");
    }

    @Override
    public void updatePostMstCompany(PostMstCompany obj) {
        /*
         UPDATE paypost.post_company 
         SET COMPANY_CODE = 'COMPANY_CODE' , COMPANY_NAME = 'COMPANY_NAME', COMPANY_ADDRESS = 'COMPANY_ADDRESS', COMPANY_OPEN1 = 'COMPANY_OPEN1',
         COMPANY_OPEN2 = 'COMPANY_OPEN2', COMPANY_CONTACT = 'COMPANY_CONTACT', COMPANY_PHONE = 'COMPANY_PHONE', COMPANY_FAX = 'COMPANY_FAX', 
         REMARK = 'REMARK', ISACTIVE = ISACTIVE 
         WHERE -- Please complete
         ;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.post_company   ")
                .append(" SET COMPANY_NAME = :COMPANY_NAME , ")
                .append("  COMPANY_ADDRESS = :COMPANY_ADDRESS , ")
                .append("  COMPANY_OPEN1 = :COMPANY_OPEN1 , ")
                .append("  COMPANY_OPEN2 = :COMPANY_OPEN2,  ")
                .append("  COMPANY_CONTACT = :COMPANY_CONTACT , ")
                .append("  COMPANY_PHONE = :COMPANY_PHONE , ")
                .append("  COMPANY_FAX = :COMPANY_FAX,  ")
                .append("  REMARK = :REMARK , ")
                .append("  ISACTIVE = :ISACTIVE  ")
                .append(" WHERE  COMPANY_CODE = :COMPANY_CODE  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("COMPANY_NAME", obj.getCompanyName());
        parameters.put("COMPANY_ADDRESS", obj.getCompanyAddress());
        parameters.put("COMPANY_OPEN1", obj.getCompanyOpen1());
        parameters.put("COMPANY_OPEN2", obj.getCompanyOpen2());
        parameters.put("COMPANY_CONTACT", obj.getCompanyContact());
        parameters.put("COMPANY_PHONE", obj.getCompanyPhone());
        parameters.put("COMPANY_FAX", obj.getCompanyFax());
        parameters.put("REMARK", obj.getRemark());
        parameters.put("ISACTIVE", obj.getIsActive());
        //Where
        parameters.put("COMPANY_CODE", obj.getCompanyCode());
      //  System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  post_company OK ");
    }

    @Override
    public void deletePostMstCompany(String companyCode) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.post_company   ")
                .append(" WHERE  COMPANY_CODE = :COMPANY_CODE  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("COMPANY_CODE", companyCode);

        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("======Delete :" + companyCode + "  : successfull");

    }

    @Override
    public PostMstCompany getPostMstCompany(String companyCode) {
        /*
         SELECT COMPANY_CODE, COMPANY_NAME, COMPANY_ADDRESS, COMPANY_OPEN1, COMPANY_OPEN2, COMPANY_CONTACT, COMPANY_PHONE, COMPANY_FAX, REMARK, ISACTIVE 
         FROM paypost.post_company;
         */
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  COMPANY_CODE, COMPANY_NAME, COMPANY_ADDRESS, COMPANY_OPEN1, COMPANY_OPEN2, ")
                .append(" COMPANY_CONTACT, COMPANY_PHONE, COMPANY_FAX, REMARK, ISACTIVE   ")
                .append(" FROM paypost.post_company ")
                .append(" WHERE COMPANY_CODE = :COMPANY_CODE ");

       // System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("COMPANY_CODE", companyCode);

        PostMstCompany companyObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PostMstCompany>() {
            @Override
            public PostMstCompany mapRow(ResultSet rs, int rowNumber) throws SQLException {
                PostMstCompany obj = new PostMstCompany();
                obj.setCompanyCode(Utilizer.replaceNull(rs.getString("COMPANY_CODE")));
                obj.setCompanyName(Utilizer.replaceNull(rs.getString("COMPANY_NAME")));
                obj.setCompanyAddress(Utilizer.replaceNull(rs.getString("COMPANY_ADDRESS")));
                obj.setCompanyOpen1(Utilizer.replaceNull(rs.getString("COMPANY_OPEN1")));
                obj.setCompanyOpen2(Utilizer.replaceNull(rs.getString("COMPANY_OPEN2")));
                obj.setCompanyContact(Utilizer.replaceNull(rs.getString("COMPANY_CONTACT")));
                obj.setCompanyPhone(Utilizer.replaceNull(rs.getString("COMPANY_PHONE")));
                obj.setCompanyFax(Utilizer.replaceNull(rs.getString("COMPANY_FAX")));
                obj.setRemark(Utilizer.replaceNull(rs.getString("REMARK")));
                obj.setIsActive(rs.getInt("ISACTIVE"));

                return obj;
            }
        });
        return companyObj;
    }

    @Override
    public List<PostMstCompany> listPostMstCompany() {

        /* SELECT COMPANY_CODE, COMPANY_NAME, COMPANY_ADDRESS, COMPANY_OPEN1, COMPANY_OPEN2, COMPANY_CONTACT, COMPANY_PHONE, COMPANY_FAX, REMARK, ISACTIVE 
         FROM paypost.post_company;*/
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
