package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 * รายการผู้รับ
 */
public class PayMstReceiver implements Serializable {

    private static final long serialVersionUID = 6640619799040465018L;

    private int payReceiverId;
    private int payTypeId;
     private String payTypeName;
    private String payReceiverName;
    private String remark;
    private String createDate;
    private String createBy;
    private String updateDate;
    private String updateBy;

    public String getPayTypeName() {
        return payTypeName;
    }

    public void setPayTypeName(String payTypeName) {
        this.payTypeName = payTypeName;
    }

    
    public int getPayReceiverId() {
        return payReceiverId;
    }

    public void setPayReceiverId(int payReceiverId) {
        this.payReceiverId = payReceiverId;
    }

    public int getPayTypeId() {
        return payTypeId;
    }

    public void setPayTypeId(int payTypeId) {
        this.payTypeId = payTypeId;
    }

    public String getPayReceiverName() {
        return payReceiverName;
    }

    public void setPayReceiverName(String payReceiverName) {
        this.payReceiverName = payReceiverName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Override
    public String toString() {
        return "PayMstReceiver{" + "payReceiverId=" + payReceiverId + ", payTypeId=" + payTypeId + ", payReceiverName=" + payReceiverName + ", remark=" + remark + ", createDate=" + createDate + ", createBy=" + createBy + ", updateDate=" + updateDate + ", updateBy=" + updateBy + '}';
    }

}
