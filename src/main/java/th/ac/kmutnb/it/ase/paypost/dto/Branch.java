package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Panit
 */
public class Branch implements Serializable {

    private static final long serialVersionUID = -1705080402516611783L;
    private int branchId;
    private String branchName;

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.branchId;
        hash = 97 * hash + Objects.hashCode(this.branchName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Branch other = (Branch) obj;
        if (this.branchId != other.branchId) {
            return false;
        }
        if (!Objects.equals(this.branchName, other.branchName)) {
            return false;
        }
        return true;
    }
    
}
