/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstType;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class PayMstTypeDaoImpl implements PayMstTypeDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //---------------------------------------
    @Override
    public void createPayMstType(PayMstType obj) {

        /*  INSERT INTO paypost.pay_mst_type
         (PAY_TYPE_NAME, PAY_REMARK, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY) 
         VALUES ('PAY_TYPE_NAME', 'PAY_REMARK', 'CREATE_DATE', 'CREATE_BY', 'UPDATE_DATE', 'UPDATE_BY');*/
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.pay_mst_type  ")
                .append("    (PAY_TYPE_NAME, PAY_REMARK, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY)    ")
                .append("   VALUES ( :PAY_TYPE_NAME  , :PAY_REMARK  , NOW(), :CREATE_BY, null, null)  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PAY_TYPE_NAME", obj.getPayTypeName());
        parameters.put("PAY_REMARK", obj.getPayRemark());
        parameters.put("CREATE_BY", obj.getCreateBy());

       // System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert pay_mst_type OK ");
    }

    @Override
    public void updatePayMstType(PayMstType obj) {
        /*UPDATE paypost.pay_mst_type 
         SET PAY_TYPE_NAME = 'PAY_TYPE_NAME' , PAY_REMARK = 'PAY_REMARK', CREATE_DATE = 'CREATE_DATE', CREATE_BY = 'CREATE_BY', UPDATE_DATE = 'UPDATE_DATE', UPDATE_BY = 'UPDATE_BY' 
         WHERE -- Please complete
         ;*/
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.pay_mst_type   ")
                .append(" SET PAY_TYPE_NAME = :PAY_TYPE_NAME , ")
                .append("  PAY_REMARK = :PAY_REMARK , ")
                .append("  UPDATE_DATE = NOW() , ")
                .append("  UPDATE_BY = :UPDATE_BY  ")
                .append(" WHERE  PAY_TYPE_ID = :PAY_TYPE_ID  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PAY_TYPE_NAME", obj.getPayTypeName());
        parameters.put("PAY_REMARK", obj.getPayRemark());
        parameters.put("UPDATE_BY", obj.getUpdateBy());
        //Where
        parameters.put("PAY_TYPE_ID", obj.getPayTypeId());
       // System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  pay_mst_type OK ");
    }

    @Override
    public void deletePayMstType(int argsId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.pay_mst_type   ")
                .append(" WHERE  PAY_TYPE_ID = :PAY_TYPE_ID  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PAY_TYPE_ID", argsId);

        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("======Delete :" +argsId + "  : successfull");
    }

    @Override
    public PayMstType getPayMstType(int argsId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  PAY_TYPE_ID, PAY_TYPE_NAME, PAY_REMARK, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY  ")
                .append(" FROM paypost.pay_mst_type ")
                .append(" WHERE PAY_TYPE_ID = :PAY_TYPE_ID ");

       // System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PAY_TYPE_ID", argsId);

        PayMstType payTypeObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PayMstType>() {
            @Override
            public PayMstType mapRow(ResultSet rs, int rowNumber) throws SQLException {
                PayMstType obj = new PayMstType();
                obj.setPayTypeId(rs.getInt("PAY_TYPE_ID"));
                obj.setPayTypeName(Utilizer.replaceNull(rs.getString("PAY_TYPE_NAME")));
                obj.setPayRemark(Utilizer.replaceNull(rs.getString("PAY_REMARK")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                return obj;
            }
        });
        return payTypeObj;
    }

    @Override
    public List<PayMstType> listPayMstType() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("   PAY_TYPE_ID, PAY_TYPE_NAME, PAY_REMARK, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY  ")
                .append(" FROM paypost.pay_mst_type ")
                .append(" Order by  PAY_TYPE_ID ");
      //  System.out.println("======Sql :" + sql);
        List<PayMstType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PayMstType>() {
            @Override
            public PayMstType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PayMstType obj = new PayMstType();
                obj.setPayTypeId(rs.getInt("PAY_TYPE_ID"));
                obj.setPayTypeName(Utilizer.replaceNull(rs.getString("PAY_TYPE_NAME")));
                obj.setPayRemark(Utilizer.replaceNull(rs.getString("PAY_REMARK")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                return obj;
            }
        });

        return listResult;
    }

}
