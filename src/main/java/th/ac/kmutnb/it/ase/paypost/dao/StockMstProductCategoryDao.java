/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductCategory;
/**
 *
 * @author metanonj
 */
public interface StockMstProductCategoryDao {
    public void createProductCategory(StockMstProductCategory productCategory);
    public void updateProductCategory(StockMstProductCategory productCategory);
    public void deleteProductCategory(int productCategoryId);
    public StockMstProductCategory getProductCategory(int productCategoryId);
    public List<StockMstProductCategory> getProductCategorys();
    
}
