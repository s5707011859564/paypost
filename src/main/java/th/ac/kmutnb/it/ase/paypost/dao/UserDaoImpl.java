package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.faces.context.FacesContext;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import th.ac.kmutnb.it.ase.paypost.dto.ApplicationUser;
import th.ac.kmutnb.it.ase.paypost.dto.Branch;
import th.ac.kmutnb.it.ase.paypost.dto.DuplicatedUsernameException;
import th.ac.kmutnb.it.ase.paypost.dto.User;

/**
 *
 * @author Panit
 */
public class UserDaoImpl implements UserDao {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void createUser(User user) throws DuplicatedUsernameException {
        String sql =
"SELECT \n" +
"    COUNT(username)\n" +
"FROM\n" +
"    users\n" +
"WHERE\n" +
"    username = :username";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", user.getUsername());
        Integer count = jdbcTemplate.queryForObject(sql, parameters, Integer.class);
        if (count > 0) throw new DuplicatedUsernameException();
        sql =
"insert into user_details (username, branch_id, first_name, last_name, created_datetime, created_user) values (:username, :branch_id, :first_name, :last_name, now(), :created_user)";
        String createdUser = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        parameters.put("branch_id", user.getBranch());
        parameters.put("first_name", user.getFirstName());
        parameters.put("last_name", user.getLastName());
        parameters.put("created_user", createdUser);
        jdbcTemplate.update(sql, parameters);
        sql =
"insert into users (username, passphrase, enabled) values (:username, :passphrase, 1)";
        String passphrase = passwordEncoder.encode(user.getPassword());
        parameters.put("passphrase", passphrase);
        jdbcTemplate.update(sql, parameters);
        sql =
"insert into authorities (username, authority) values (:username, :authority)";
        parameters.put("authority", user.getRole());
        jdbcTemplate.update(sql, parameters);
    }

    @Override
    public void updateUser(User user) {
        String sql =
"UPDATE user_details \n" +
"SET \n" +
"    branch_id = :branch_id,\n" +
"    first_name = :first_name,\n" +
"    last_name = last_name,\n" +
"    modified_datetime = NOW(),\n" +
"    modified_user = :modified_user\n" +
"WHERE\n" +
"    username = :username";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", user.getUsername());
        String modifiedUser = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        parameters.put("branch_id", user.getBranch());
        parameters.put("first_name", user.getFirstName());
        parameters.put("last_name", user.getLastName());
        parameters.put("modified_user", modifiedUser);
        jdbcTemplate.update(sql, parameters);
        String password = user.getPassword();
        if ((password != null) && (!"".equals(password.trim()))) {
            sql =
"UPDATE users \n" +
"SET \n" +
"    passphrase = :passphrase\n" +
"WHERE\n" +
"    username = :username";
            String passphrase = passwordEncoder.encode(password.trim());
            parameters.put("passphrase", passphrase);
            jdbcTemplate.update(sql, parameters);
        }
        sql =
"UPDATE authorities \n" +
"SET \n" +
"    authority = :authority\n" +
"WHERE\n" +
"    username = :username";
        parameters.put("authority", user.getRole());
        jdbcTemplate.update(sql, parameters);
    }

    @Override
    public void deleteUser(String username) {
        String sql =
"DELETE FROM users \n" +
"WHERE\n" +
"    username = :username";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", username);
        jdbcTemplate.update(sql, parameters);
        sql =
"DELETE FROM user_details \n" +
"WHERE\n" +
"    username = :username";
        jdbcTemplate.update(sql, parameters);
        sql =
"DELETE FROM authorities \n" +
"WHERE\n" +
"    username = :username";
        jdbcTemplate.update(sql, parameters);
    }

    @Override
    public User getUser(String username) {
        return null;
    }

    @Override
    public ApplicationUser getApplicationUser(final String username) {
        String sql =
"SELECT \n" +
"    a.username,\n" +
"    a.passphrase,\n" +
"    a.enabled,\n" +
"    b.first_name,\n" +
"    b.last_name,\n" +
"    c.branch_id,\n" +
"    c.branch_name\n" +
"FROM\n" +
"    users a\n" +
"        INNER JOIN\n" +
"    user_details b ON a.username = b.username\n" +
"        INNER JOIN\n" +
"    branch c ON b.branch_id = c.branch_id\n" +
"WHERE\n" +
"    a.username = :username";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", username);
        Map<String, Object> details = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<Map<String, Object>>() {

            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> result = new HashMap<>();
                result.put("password", rs.getString("passphrase"));
                result.put("enabled", rs.getBoolean("enabled"));
                result.put("firstName", rs.getString("first_name"));
                result.put("lastName", rs.getString("last_name"));
                Branch branch = new Branch();
                branch.setBranchId(rs.getInt("branch_id"));
                branch.setBranchName(rs.getString("branch_name"));
                result.put("branch", branch);
                return result;
            }
        });

        sql =
"SELECT \n" +
"    authority\n" +
"FROM\n" +
"    authorities\n" +
"WHERE\n" +
"    username = :username";
        List<String> roles = jdbcTemplate.query(sql, parameters, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString("authority");
            }
        });

        Set<GrantedAuthority> authorities = new HashSet<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }

        ApplicationUser user = new ApplicationUser(username, (String) details.get("password"), authorities);
        user.setBranch((Branch) details.get("branch"));
        user.setEnabled((Boolean) details.get("enabled"));
        user.setFirstName((String) details.get("firstName"));
        user.setLastName((String) details.get("lastName"));
        
        return user;
    }

    @Override
    public boolean checkPassword(String username, String password) {
        String sql =
"SELECT \n" +
"    passphrase\n" +
"FROM\n" +
"    users\n" +
"WHERE\n" +
"    username = :username";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", username);
        String cipher = jdbcTemplate.queryForObject(sql, parameters, String.class);
        return passwordEncoder.matches(password, cipher);
    }

    @Override
    public boolean changePassword(String username, String password) {
        return false;
    }

    @Override
    public boolean enableUser(String username) {
        return false;
    }

    @Override
    public boolean disableUser(String username) {
        return false;
    }

    @Override
    public List<User> getUsers() {
        String sql =
"SELECT \n" +
"    username, branch_id, first_name, last_name\n" +
"FROM\n" +
"    user_details\n" +
"ORDER BY username , first_name , last_name";
        List<User> result = jdbcTemplate.query(sql, new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet rs, int i) throws SQLException {
                User o = new User();
                o.setUsername(rs.getString("username"));
                o.setBranch(rs.getInt("branch_id"));
                o.setFirstName(rs.getString("first_name"));
                o.setLastName(rs.getString("last_name"));
                return o;
            }
        });
        return result;
    }

}
