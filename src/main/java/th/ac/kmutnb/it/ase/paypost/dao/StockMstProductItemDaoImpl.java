/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductItem;

/**
 *
 * @author imetanon
 */
public class StockMstProductItemDaoImpl implements StockMstProductItemDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
	jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void createProductItem(StockMstProductItem productItem) {
	String sql
		= "INSERT INTO post_stock_mst_product_item(PRODUCT_ITEM_CODE, PRODUCT_ITEM_NAME, PRODUCT_ITEM_UNIT, PRODUCT_ITEM_PRICE, PRODUCT_ITEM_COST, PRODUCT_ITEM_AMOUNT, LAST_TX_DATE, PRODUCT_CATEGORY_ID, PRODUCT_ITEM_IS_ACTIVE, PRODUCT_ITEM_REMARK, CREATED_DATETIME, CREATE_BY) VALUES (:productItemCode, :productItemName, :productItemUnit, :productItemPrice, :productItemCost, :productItemAmount, now(), :productCategoryId, :isActive, :remark, now(), :created_user)";

	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productItemCode", productItem.getProductCode());
	parameters.put("productItemName", productItem.getProductName());
	parameters.put("productItemUnit", productItem.getProductUnit());
	parameters.put("productItemPrice", productItem.getPrice());
	parameters.put("productItemCost", productItem.getCost());
	parameters.put("productItemAmount", productItem.getAmount());
	parameters.put("productCategoryId", productItem.getProductCategoryId());
	parameters.put("isActive", productItem.isIsActive());
	parameters.put("remark", productItem.getRemark());
	parameters.put("created_user", "SYSTEM");
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public void updateProductItem(StockMstProductItem productItem) {
	String sql
		= "UPDATE post_stock_mst_product_item \n"
		+ "SET \n"
		+ "    PRODUCT_ITEM_CODE = :productCode,\n"
		+ "    PRODUCT_ITEM_NAME = :productName,\n"
		+ "    PRODUCT_ITEM_UNIT = :productUnit,\n"
		+ "    PRODUCT_ITEM_COST = :productCost,\n"
		+ "    PRODUCT_ITEM_PRICE = :productPrice,\n"
		+ "    PRODUCT_ITEM_AMOUNT = :productAmount,\n"
		+ "    LAST_TX_DATE = now(),\n"
		+ "    PRODUCT_CATEGORY_ID = :productCategoryId,\n"
		+ "    PRODUCT_ITEM_IS_ACTIVE = :isActive,\n"
		+ "    PRODUCT_ITEM_REMARK = :remark,\n"
		+ "    UPDATE_DATETIME = now(),\n"
		+ "    UPDATE_BY = :updated_user\n"
		+ "WHERE\n"
		+ "    PRODUCT_ITEM_ID = :productId";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productId", productItem.getProductId());
	parameters.put("productCode", productItem.getProductCode());
	parameters.put("productName", productItem.getProductName());
	parameters.put("productUnit", productItem.getProductUnit());
	parameters.put("productPrice", productItem.getPrice());
	parameters.put("productCost", productItem.getCost());
	parameters.put("productAmount", productItem.getAmount());
	parameters.put("productCategoryId", productItem.getProductCategoryId());
	parameters.put("isActive", productItem.isIsActive());
	parameters.put("remark", productItem.getRemark());
	parameters.put("updated_user", "SYSTEM");
	jdbcTemplate.update(sql, parameters);

    }

    @Override
    public void deleteProductItem(int productItemId) {
	String sql
		= "DELETE FROM post_stock_mst_product_item \n"
		+ "WHERE\n"
		+ "    product_item_id = :productItemId";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productItemId", productItemId);
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public StockMstProductItem getProductItem(int productItemId) {
	String sql
		= "SELECT \n"
		+ "    product_item_id, product_item_code, product_item_name, product_item_unit, product_item_price, product_item_cost, product_item_amount, last_tx_date, product_category_id, product_item_is_active, product_item_remark\n"
		+ "FROM\n"
		+ "    post_stock_mst_product_item\n"
		+ "WHERE\n"
		+ "    product_item_id = :id\n"
		+ "ORDER BY product_item_id ASC";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("id", productItemId);
	StockMstProductItem result = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<StockMstProductItem>() {
	    @Override
	    public StockMstProductItem mapRow(ResultSet rs, int i) throws SQLException {
		StockMstProductItem o = new StockMstProductItem();
		o.setProductId(rs.getInt("product_item_id"));
		o.setProductCode(rs.getString("product_item_code"));
		o.setProductName(rs.getString("product_item_name"));
		o.setProductUnit(rs.getString("product_item_unit"));
		o.setCost(rs.getDouble("product_item_price"));
		o.setPrice(rs.getDouble("product_item_cost"));
		o.setAmount(rs.getInt("product_item_amount"));
		o.setProductCategoryId(rs.getInt("product_category_id"));
		o.setIsActive(rs.getBoolean("product_item_is_active"));
		o.setRemark(rs.getString("product_item_remark"));
		return o;
	    }
	});
	return result;
    }

    @Override
    public List<StockMstProductItem> getProductItems() {
	String sql
		= "SELECT \n"
		+ "    product_item_id, product_item_code, product_item_name, product_item_unit, product_item_price, product_item_cost, product_item_amount, last_tx_date, product_category_id, product_item_is_active, product_item_remark\n"
		+ "FROM\n"
		+ "    post_stock_mst_product_item\n"
		+ "ORDER BY product_item_id";
	List<StockMstProductItem> result = jdbcTemplate.query(sql, new RowMapper<StockMstProductItem>() {

	    @Override
	    public StockMstProductItem mapRow(ResultSet rs, int i) throws SQLException {
		StockMstProductItem o = new StockMstProductItem();
		o.setProductId(rs.getInt("product_item_id"));
		o.setProductCode(rs.getString("product_item_code"));
		o.setProductName(rs.getString("product_item_name"));
		o.setProductUnit(rs.getString("product_item_unit"));
		o.setCost(rs.getDouble("product_item_price"));
		o.setPrice(rs.getDouble("product_item_cost"));
		o.setAmount(rs.getInt("product_item_amount"));
		o.setProductCategoryId(rs.getInt("product_category_id"));
		o.setIsActive(rs.getBoolean("product_item_is_active"));
		o.setRemark(rs.getString("product_item_remark"));
		return o;
	    }

	});
	return result;
    }

    @Override
    public List<StockMstProductItem> getProductItemsByCategory(int categoryId) {
	System.out.println(categoryId);
	String sql
		= "SELECT \n"
		+ "    product_item_id, product_item_code, product_item_name, product_item_unit, product_item_price, product_item_cost, product_item_amount, last_tx_date, product_category_id, product_item_is_active, product_item_remark\n"
		+ "FROM\n"
		+ "    post_stock_mst_product_item\n"
		+ " WHERE\n"
		+ " product_category_id = :id\n"
		+ " ORDER BY product_item_id";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("id", categoryId);
	List<StockMstProductItem> result = jdbcTemplate.query(sql, parameters, new RowMapper<StockMstProductItem>() {

	    @Override
	    public StockMstProductItem mapRow(ResultSet rs, int i) throws SQLException {
		StockMstProductItem o = new StockMstProductItem();
		o.setProductId(rs.getInt("product_item_id"));
		o.setProductCode(rs.getString("product_item_code"));
		o.setProductName(rs.getString("product_item_name"));
		o.setProductUnit(rs.getString("product_item_unit"));
		o.setCost(rs.getDouble("product_item_price"));
		o.setPrice(rs.getDouble("product_item_cost"));
		o.setAmount(rs.getInt("product_item_amount"));
		o.setProductCategoryId(rs.getInt("product_category_id"));
		o.setIsActive(rs.getBoolean("product_item_is_active"));
		o.setRemark(rs.getString("product_item_remark"));
		return o;
	    }

	});
	return result;
    }

}
