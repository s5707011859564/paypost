package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

public class Payee implements Serializable {
    
    private static final long serialVersionUID = 8714165424602872685L;
    private int payeeId;
    private String payeeName;

    public int getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(int payeeId) {
        this.payeeId = payeeId;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }
}
