/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author imetanon
 */
public class PostService implements Serializable{
    private static final long serialVersionUID = -6587301925228503724L;
    
    private int postServiceId;
    private PostContact reciver;
    private Integer serviceId;
    private double weight;
    private List<PostProduct> postProducts;

    public PostContact getReciver() {
	return reciver;
    }

    public void setReciver(PostContact reciver) {
	this.reciver = reciver;
    }

    public Integer getServiceId() {
	return serviceId;
    }

    public void setServiceId(Integer serviceId) {
	this.serviceId = serviceId;
    }

    public double getWeight() {
	return weight;
    }

    public void setWeight(double weight) {
	this.weight = weight;
    }

    public List<PostProduct> getPostProducts() {
	return postProducts;
    }

    public void setPostProducts(List<PostProduct> postProducts) {
	this.postProducts = postProducts;
    }

    @Override
    public String toString() {
        return "PostService{" + "reciver=" + reciver + ", serviceId=" + serviceId + ", weight=" + weight + ", postProducts=" + postProducts + '}';
    }

    public int getPostServiceId() {
        return postServiceId;
    }

    public void setPostServiceId(int postServiceId) {
        this.postServiceId = postServiceId;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
