/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProduct;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductItem;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductCategory;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.StockMstProductCategoryDao;
import th.ac.kmutnb.it.ase.paypost.dao.StockMstProductDao;
import th.ac.kmutnb.it.ase.paypost.dao.StockMstProductItemDao;

@Component
@ManagedBean
/**
 *
 * @author imetanon
 */
public class StockMstProductView implements Serializable {

    private static final long serialVersionUID = 6641060428434642492L;
    
    private List<StockMstProduct> productStockList;
    private List<StockMstProduct> filterStocks;
    private List<StockMstProductItem> productItemList;
    private List<StockMstProductCategory> productCategoryList;
    private StockMstProduct productStock;
    private StockMstProductItem productItem;
    private StockMstProductCategory productCategory;
    private Object selectedObj;
    private int amountAdjust = 0;

    @Autowired
    private StockMstProductCategoryDao productCategoryDao;

    public void setProductCategoryDao(StockMstProductCategoryDao productCategoryDao) {
	this.productCategoryDao = productCategoryDao;
    }
    
    @Autowired
    private StockMstProductItemDao productItemDao;

    public void setProductItemDao(StockMstProductItemDao productItemDao) {
	this.productItemDao = productItemDao;
    }

    @Autowired
    private StockMstProductDao productStockDao;

    public void setProductStockDao(StockMstProductDao productStockDao) {
	this.productStockDao = productStockDao;
    }
    

    @PostConstruct
    public void init() {
	productStock = new StockMstProduct();
	productStock.setIsActive(true);
	productItem = new StockMstProductItem();
	productItem.setIsActive(true);
	productCategory = new StockMstProductCategory();
	productCategory.setIsActive(true);
	productItemList = productItemDao.getProductItems();
	productCategoryList = productCategoryDao.getProductCategorys();
	productStockList = productStockDao.getProductStocks();
    }

    public void createProductCategory() {
	productCategoryDao.createProductCategory(productCategory);
	productCategoryList = productCategoryDao.getProductCategorys();
	productCategory = new StockMstProductCategory();
	productCategory.setIsActive(true);
	RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void createProductItem() {
	productItemDao.createProductItem(productItem);
	productItemList = productItemDao.getProductItems();
	productItem = new StockMstProductItem();
	productItem.setIsActive(true);
	RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }
    
    public void createProductStock() {
	productStockDao.createProductStock(productStock);
	productStockList = productStockDao.getProductStocks();
	productStock = new StockMstProduct();
	productStock.setIsActive(true);
	RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void updateObj() {
	if (selectedObj instanceof StockMstProductCategory) {
	    StockMstProductCategory pc = (StockMstProductCategory) selectedObj;
	    productCategoryDao.updateProductCategory(pc);
	    productCategoryList = productCategoryDao.getProductCategorys();
	    productCategory = new StockMstProductCategory();
	    productCategory.setIsActive(true);
	} else if (selectedObj instanceof StockMstProductItem) {
	    StockMstProductItem pi = (StockMstProductItem) selectedObj;
            pi.setAmount(pi.getAmount()+amountAdjust);
	    productItemDao.updateProductItem(pi);
	    productItemList = productItemDao.getProductItems();
	    productItem = new StockMstProductItem();
	    productItem.setIsActive(true);
            amountAdjust = 0;
	} else if (selectedObj instanceof StockMstProduct) {
	    StockMstProduct ps = (StockMstProduct) selectedObj;
	    productStockDao.updateProductStock(ps);
	    productStockList = productStockDao.getProductStocks();
	    productStock = new StockMstProduct();
	    productStock.setIsActive(true);
	}
	selectedObj = null;
	RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void editObj(Object obj) {
	selectedObj = obj;
	if (obj instanceof StockMstProductCategory) {
	    StockMstProductCategory pc = (StockMstProductCategory) obj;
	    productCategory = pc;
	}else if (obj instanceof StockMstProductItem) {
	    StockMstProductItem pi = (StockMstProductItem) obj;
	    productItem = pi;
	}else if (obj instanceof StockMstProduct) {
	    StockMstProduct ps = (StockMstProduct) obj;
	    productStock = ps;
	}
	
    }

    public void deleteObj(Object obj) {
	if (obj instanceof StockMstProductCategory) {
	    StockMstProductCategory pc = (StockMstProductCategory) obj;
	    productCategoryDao.deleteProductCategory(pc.getProductCategoryId());
	    productCategoryList = productCategoryDao.getProductCategorys();
	}else if (obj instanceof StockMstProductItem) {
	    StockMstProductItem pi = (StockMstProductItem) obj;
	    productItemDao.deleteProductItem(pi.getProductId());
	    productItemList = productItemDao.getProductItems();
	}else if (obj instanceof StockMstProduct) {
	    StockMstProduct ps = (StockMstProduct) obj;
	    productStockDao.deleteProductStock(ps.getProductStockId());
	    productStockList = productStockDao.getProductStocks();
	}

    }

    public void handleDialogClose() {
	productStock = new StockMstProduct();
	productStock.setIsActive(true);
	productItem = new StockMstProductItem();
	productItem.setIsActive(true);
	productCategory = new StockMstProductCategory();
	productCategory.setIsActive(true);
	selectedObj = null;
    }

    public List<StockMstProduct> getProductStockList() {
	return productStockList;
    }

    public void setProductStockList(List<StockMstProduct> productStockList) {
	this.productStockList = productStockList;
    }

    public List<StockMstProductItem> getProductItemList() {
	return productItemList;
    }

    public void setProductItemList(List<StockMstProductItem> productItemList) {
	this.productItemList = productItemList;
    }

    public List<StockMstProductCategory> getProductCategoryList() {
	return productCategoryList;
    }

    public void setProductCategoryList(List<StockMstProductCategory> productCategoryList) {
	this.productCategoryList = productCategoryList;
    }

    public StockMstProduct getProductStock() {
	return productStock;
    }

    public void setProductStock(StockMstProduct productStock) {
	this.productStock = productStock;
    }

    public StockMstProductItem getProductItem() {
	return productItem;
    }

    public void setProductItem(StockMstProductItem productItem) {
	this.productItem = productItem;
    }

    public StockMstProductCategory getProductCategory() {
	return productCategory;
    }

    public void setProductCategory(StockMstProductCategory productCategory) {
	this.productCategory = productCategory;
    }

    public Object getSelectedObj() {
	return selectedObj;
    }

    public void setSelectedObj(Object selectedObj) {
	this.selectedObj = selectedObj;
    }
    
    public String findProductCategory(int id){
	for(StockMstProductCategory pc : productCategoryList){
	    if(pc.getProductCategoryId() == id){
		return pc.getProductCategoryName();
	    }
	}
	return null;
    }

    public int getAmountAdjust() {
        return amountAdjust;
    }

    public void setAmountAdjust(int amountAdjust) {
        this.amountAdjust = amountAdjust;
    }
    
    
    
    
    public String findProductItem(int id){
	for(StockMstProductItem pi : productItemList){
	    if(pi.getProductId() == id){
		return pi.getProductName();
	    }
	}
	return null;
    }

    public List<StockMstProduct> getFilterStocks() {
	return filterStocks;
    }

    public void setFilterStocks(List<StockMstProduct> filterStocks) {
	this.filterStocks = filterStocks;
    }
    
    
    
    

}
