package th.ac.kmutnb.it.ase.paypost.view;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.BranchDao;
import th.ac.kmutnb.it.ase.paypost.dao.UserDao;
import th.ac.kmutnb.it.ase.paypost.dto.Branch;
import th.ac.kmutnb.it.ase.paypost.dto.User;

@Component
@ManagedBean
public class UserView {

    private List<User> users;
    private List<Branch> branches;
    private User user;
    private User selectedUser;
    
    @Autowired
    private BranchDao branchDao;
    public void setBranchDao(BranchDao branchDao) {
        this.branchDao = branchDao;
    }
    
    @Autowired
    private UserDao userDao;
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @PostConstruct
    public void init() {
        user = new User();
        selectedUser = new User();
        branches = branchDao.getBranches();
        users = userDao.getUsers();
    }

    public void createUser(ActionEvent event) {
        userDao.createUser(user);
        users = userDao.getUsers();
        user.setUsername(null);
        user.setFirstName(null);
        user.setLastName(null);
        user.setPassword(null);
        RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void updateUser() {
        userDao.updateUser(selectedUser);
        users = userDao.getUsers();
        selectedUser.setUsername(null);
        selectedUser.setFirstName(null);
        selectedUser.setLastName(null);
        selectedUser.setPassword(null);
        RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void deleteUser() {
        userDao.deleteUser(selectedUser.getUsername());
        users = userDao.getUsers();
        selectedUser.setUsername(null);
        selectedUser.setFirstName(null);
        selectedUser.setLastName(null);
        selectedUser.setPassword(null);
        RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void changePassword() {

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }
}
