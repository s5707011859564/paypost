package th.ac.kmutnb.it.ase.paypost.dto;

public class DuplicatedUsernameException extends RuntimeException{
    
    private static final long serialVersionUID = -2832490661154524976L;
    
}
