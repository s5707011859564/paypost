/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author imetanon
 */
public class PostContact implements Serializable{
    private static final long serialVersionUID = 6732937698940764447L;
    
    private int postContactId;
    private int postId;
    private int postServiceId;
    private String firstName;
    private String lastName;
    private String address;
    private Integer districtId;
    private Integer amphureId;
    private Integer provinceId;
    private String postcode;
    private String phone;    

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    public Integer getDistrictId() {
	return districtId;
    }

    public void setDistrictId(Integer districtId) {
	this.districtId = districtId;
    }

    public Integer getAmphureId() {
	return amphureId;
    }

    public void setAmphureId(Integer amphureId) {
	this.amphureId = amphureId;
    }

    public Integer getProvinceId() {
	return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
	this.provinceId = provinceId;
    }

    public String getPostcode() {
	return postcode;
    }

    public void setPostcode(String postcode) {
	this.postcode = postcode;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public int getPostContactId() {
        return postContactId;
    }

    public void setPostContactId(int postContactId) {
        this.postContactId = postContactId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getPostServiceId() {
        return postServiceId;
    }

    public void setPostServiceId(int postServiceId) {
        this.postServiceId = postServiceId;
    }
    
    

    
    
}
