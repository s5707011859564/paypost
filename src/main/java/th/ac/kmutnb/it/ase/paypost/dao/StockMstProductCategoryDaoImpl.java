/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.ApplicationUser;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductCategory;
import th.ac.kmutnb.it.ase.paypost.dto.User;

/**
 *
 * @author metanonj
 */
public class StockMstProductCategoryDaoImpl implements StockMstProductCategoryDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
	jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void createProductCategory(StockMstProductCategory productCategory) {
	String sql
		= "INSERT INTO post_stock_mst_product_category(PRODUCT_CATEGORY_CODE, PRODUCT_CATEGORY_NAME, PRODUCT_CATEGORY_IS_ACTIVE, PRODUCT_CATEGORY_REMARK, CREATED_DATETIME, CREATE_BY) VALUES (:productCategoryCode, :productCategoryName, :isActive, :remark, now(), :created_user)";

	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productCategoryCode", productCategory.getProductCategoryCode());
	parameters.put("productCategoryName", productCategory.getProductCategoryName());
	parameters.put("isActive", productCategory.isIsActive());
	parameters.put("remark", productCategory.getRemark());
	parameters.put("created_user", "SYSTEM");
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public void updateProductCategory(StockMstProductCategory productCategory) {
	String sql
		= "UPDATE post_stock_mst_product_category \n"
		+ "SET \n"
		+ "    PRODUCT_CATEGORY_CODE = :productCode,\n"
		+ "    PRODUCT_CATEGORY_NAME = :productName,\n"
		+ "    PRODUCT_CATEGORY_IS_ACTIVE = :isActive,\n"
		+ "    PRODUCT_CATEGORY_REMARK = :remark,\n"
		+ "    UPDATE_DATETIME = now(),\n"
		+ "    UPDATE_BY = :updated_user\n"
		+ "WHERE\n"
		+ "    PRODUCT_CATEGORY_ID = :productId";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productId", productCategory.getProductCategoryId());
	parameters.put("productCode", productCategory.getProductCategoryCode());
	parameters.put("productName", productCategory.getProductCategoryName());
	parameters.put("isActive", productCategory.isIsActive());
	parameters.put("remark", productCategory.getRemark());
	parameters.put("updated_user", "SYSTEM");
	jdbcTemplate.update(sql, parameters);

    }

    @Override
    public void deleteProductCategory(int productCategoryId) {
	String sql
		= "DELETE FROM post_stock_mst_product_category \n"
		+ "WHERE\n"
		+ "    product_category_id = :productCategoryId";
	Map<String, Object> parameters = new HashMap<>();
	parameters.put("productCategoryId", productCategoryId);
	jdbcTemplate.update(sql, parameters);
    }

    @Override
    public StockMstProductCategory getProductCategory(int productCategoryId) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<StockMstProductCategory> getProductCategorys() {
	String sql
		= "SELECT \n"
		+ "    product_category_id, product_category_code, product_category_name, product_category_is_active, product_category_remark\n"
		+ "FROM\n"
		+ "    post_stock_mst_product_category\n"
		+ "ORDER BY product_category_id";
	List<StockMstProductCategory> result = jdbcTemplate.query(sql, new RowMapper<StockMstProductCategory>() {
	    @Override
	    public StockMstProductCategory mapRow(ResultSet rs, int i) throws SQLException {
		StockMstProductCategory o = new StockMstProductCategory();
		o.setProductCategoryId(rs.getInt("product_category_id"));
		o.setProductCategoryCode(rs.getString("product_category_code"));
		o.setProductCategoryName(rs.getString("product_category_name"));
		o.setIsActive(rs.getBoolean("product_category_is_active"));
		o.setRemark(rs.getString("product_category_remark"));
		return o;
	    }
	});
	return result;
    }

}
