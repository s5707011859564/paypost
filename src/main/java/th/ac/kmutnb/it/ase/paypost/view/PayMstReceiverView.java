/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PayMstReceiverDao;
import th.ac.kmutnb.it.ase.paypost.dto.PayMstReceiver;


/**
 *
 * @author pradoem
 * รายการผู้รับ
 * PayMstReceiver
 */

@Component
@ManagedBean
@ViewScoped
public class PayMstReceiverView  implements  Serializable{
    private static final long serialVersionUID = -6439394570292467660L;
    
    //------------Connection
     @Autowired
    private PayMstReceiverDao payMstReceiverDao;
    public void setPayMstReceiverDao(PayMstReceiverDao payMstReceiverDao) {
        this.payMstReceiverDao = payMstReceiverDao;
    }
    
    private List<PayMstReceiver> listPayMstReceiver;
    private PayMstReceiver payMstReceiverObj;
    private Map<String,String> payMstTypeDDL;
    private boolean editChecker = false;
    private String userLogin;

    
    public Map<String, String> getPayMstTypeDDL() {
        return payMstTypeDDL;
    }

    public void setPayMstTypeDDL(Map<String, String> payMstTypeDDL) {
        this.payMstTypeDDL = payMstTypeDDL;
    }
    
    public PayMstReceiver getPayMstReceiverObj() {
        return payMstReceiverObj;
    }

    public void setPayMstReceiverObj(PayMstReceiver payMstReceiverObj) {
        this.payMstReceiverObj = payMstReceiverObj;
    }

    
    public List<PayMstReceiver> getListPayMstReceiver() {
        return listPayMstReceiver;
    }

    public void setListPayMstReceiver(List<PayMstReceiver> listPayMstReceiver) {
        this.listPayMstReceiver = listPayMstReceiver;
    }


    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public String getUserLogin() {
        return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
    
    //--------------Method Action
    @PostConstruct
    public void init() {
        doReloadData();

    }

    //Reload Data
     public void doReloadData() {
         listPayMstReceiver  = payMstReceiverDao.listPayMstReceiver();
     }
    /****************
     * Clear
     */
    public void doClear() {
      payMstReceiverObj = null;
    }

    /**
     * *************
     * Save/update
     */
    public void doSaveAction() {
        System.out.println("==== doSaveAction ==========");
        System.out.println("getPayTypeId ==== " +payMstReceiverObj.getPayTypeId());

        payMstReceiverObj.setCreateBy(this.getUserLogin());
        payMstReceiverObj.setUpdateBy(this.getUserLogin());
         if (!editChecker) {
                payMstReceiverDao.createPayMstReceiver(payMstReceiverObj);
         } else {
                payMstReceiverDao.updatePayMstReceiver(payMstReceiverObj);
         }

         editChecker = false;

         doReloadData();

         doClear();
                
        System.out.println("===== doSaveAction : Complete  =====");
    }

    public void doDeleteAction(int argsId) {
       System.out.println("===== doDeleteAction : Start  =====");
       System.out.println("===== areaTypeId :" + argsId);
       
        payMstReceiverDao.deletePayMstReceiver(argsId);
        
        doReloadData();

         editChecker = false;
         addMessage(" ลบข้อมูลเรียบร้อยแล้ว  ");
         System.out.println("===== Delete : Complete  =====");
    }

    public void doEditAction(int argsId) {
        System.out.println("===== doEditAction : Start  =====");
        System.out.println("===== areaTypeId :" + argsId);
        
         payMstReceiverObj = payMstReceiverDao.getPayMstReceiver(argsId);
        // payMstReceiverObj.
         payMstTypeDDL =   payMstReceiverDao.listMapPayMstType();
         
         editChecker = true;
        // System.out.println("===== doEditAction : Complete  =====");
    }

    public void doAddForm() {
        editChecker = false;
        doClear();
        payMstReceiverObj = new PayMstReceiver();
        payMstTypeDDL =   payMstReceiverDao.listMapPayMstType();
       System.out.println("==== doAddForm ==========");
    }

    public void handleDialogClose() {
      //  System.out.println("========Clase Dialog============");
        // this.mstAreaType = new PostMstAreaType();
        this.editChecker = false;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }   
    
    
    
}
