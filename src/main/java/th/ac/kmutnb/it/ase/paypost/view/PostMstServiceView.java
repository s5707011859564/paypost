/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstServiceDao;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstService;

/**
 *
 * @author imetanon
 */
@Component
@ManagedBean
public class PostMstServiceView implements Serializable{
    private static final long serialVersionUID = 2481820311926570165L;
    
    private PostMstService postMstService;
    private List<PostMstService> postMstServiceList;
    
    @Autowired
    private PostMstServiceDao postMstServiceDao;

    public void setPostMstServiceDao(PostMstServiceDao postMstServiceDao) {
        this.postMstServiceDao = postMstServiceDao;
    }
    
    @PostConstruct
    public void init() {
	postMstService = new PostMstService();
	postMstServiceList = postMstServiceDao.getPostMstServices();
    }
    
    public void createProductStock() {
	postMstServiceDao.createPostMstService(postMstService);
	postMstServiceList = postMstServiceDao.getPostMstServices();
	postMstService = new PostMstService();
	RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }
    
    public void editObj(PostMstService pms) {
	System.out.println(pms);
	this.postMstService = pms;
    }
    
    public void updateObj() {
	postMstServiceDao.updatePostMstService(postMstService);
	postMstService = new PostMstService();
	RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }
    
    public void deleteObj(PostMstService pms) {
	postMstServiceDao.deletePostMstService(pms.getServiceId());
	postMstServiceList = postMstServiceDao.getPostMstServices();
    }
    
    public void handleDialogClose() {
	postMstService = new PostMstService();
	RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public PostMstService getPostMstService() {
	return postMstService;
    }

    public void setPostMstService(PostMstService postMstService) {
	this.postMstService = postMstService;
    }

    public List<PostMstService> getPostMstServiceList() {
	return postMstServiceList;
    }

    public void setPostMstServiceList(List<PostMstService> postMstServiceList) {
	this.postMstServiceList = postMstServiceList;
    }
    
    
}
