/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author imetanon
 */
public class ProductTx implements Serializable{
    private static final long serialVersionUID = 8084739818238459926L;
    
    private int productTxId;
    private String firstName;
    private String lastName;
    private String phone;
    private List<ProductTxItem> itemList;
    private double totalPrice;

    public int getProductTxId() {
	return productTxId;
    }

    public void setProductTxId(int productTxId) {
	this.productTxId = productTxId;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    

    public List<ProductTxItem> getItemList() {
	return itemList;
    }

    public void setItemList(List<ProductTxItem> itemList) {
	this.itemList = itemList;
    }

    public double getTotalPrice() {
	return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
	this.totalPrice = totalPrice;
    }
    
    
    
}
