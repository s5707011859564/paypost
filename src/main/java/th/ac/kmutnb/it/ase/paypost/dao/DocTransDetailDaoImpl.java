/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.DocMstTrans;
import th.ac.kmutnb.it.ase.paypost.dto.DocTransDetail;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class DocTransDetailDaoImpl implements DocTransDetailDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    //---------------------------------------
    @Override
    public void createDocTransDetail(DocTransDetail obj) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.doc_trans_detail  ")
                .append("  (DOC_TRANS_ID, DOC_TRANS_DETAIL_ID, UNIT, UOM, PRICE, TOTAL, DISCOUNT, MONEY_INCOME, MONEY_RETURN, REMARK, CREATE_DATE, CREATE_USER, MODIFY_DATE, MODIFY_USER)   ")
                .append("   VALUES ( :DOC_TRANS_ID , :DOC_TRANS_DETAIL_ID  , :UNIT, null  , :PRICE , :TOTAL, :DISCOUNT, :MONEY_INCOME, :MONEY_RETURN, :REMARK , NOW(), :CREATE_USER , null , null  ) ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DOC_TRANS_ID", obj.getDocTransId());
        parameters.put("DOC_TRANS_DETAIL_ID", obj.getDocTransDesc());
        parameters.put("UNIT", obj.getUnit());
        //parameters.put("UOM", obj);
        parameters.put("PRICE", obj.getPrice());
        parameters.put("TOTAL", obj.getTotal());
        parameters.put("DISCOUNT", obj.getDisCount());
        parameters.put("MONEY_INCOME", obj.getMoneyInCome());
        parameters.put("MONEY_RETURN", obj.getMoneyReturn());
        parameters.put("REMARK", obj.getRemark());
        parameters.put("CREATE_USER", obj.getCreateBy());

       // System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert doc_trans_detail OK ");
    }

    @Override
    public void insertDocMstTrans(List<DocMstTrans> objList,String autoId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.doc_mst_trans  ")
                .append(" (DOCS_TRANSID, DOC_TRANS_DETAIL_ID, DOC_QTY, DOC_PRICE, DOC_TOTAL, REMARK, STATUS, CREATE_DATE, CREATE_USER, MODIFY_DATE, MODIFY_USER)   ")
                .append(" VALUES ( :DOCS_TRANSID , :DOC_TRANS_DETAIL_ID , :DOC_QTY, :DOC_PRICE, :DOC_TOTAL, null , 1,  NOW() , :CREATE_USER , null ,  null ) ");

        for (DocMstTrans obj : objList) {
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("DOCS_TRANSID",autoId);
            parameters.put("DOC_TRANS_DETAIL_ID", obj.getDocCode());
            parameters.put("DOC_QTY", obj.getDocItems());
            parameters.put("DOC_PRICE", obj.getPrice());
            parameters.put("DOC_TOTAL", obj.getTotalPrice());
            // parameters.put("REMARK", "");
            // parameters.put("STATUS", obj.getMoneyInCome());
            parameters.put("CREATE_USER", "System");

            //System.out.println("SQL :" + sql.toString());
            jdbcTemplate.update(sql.toString(), parameters);
            System.out.println("==Insert insertDocMstTrans OK ");
        }
    }

    @Override
    public void insertDocTransDetail(DocMstTrans obj) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.doc_trans_detail  ")
                .append("  (DOC_TRANS_ID, DOC_TRANS_DETAIL_ID, UNIT, UOM, PRICE, TOTAL, DISCOUNT, MONEY_INCOME, MONEY_RETURN, REMARK, CREATE_DATE, CREATE_USER, MODIFY_DATE, MODIFY_USER)   ")
                .append("   VALUES ( :DOC_TRANS_ID , :DOC_TRANS_DETAIL_ID  , :UNIT, null  , :PRICE , :TOTAL, :DISCOUNT, :MONEY_INCOME, :MONEY_RETURN, :REMARK , NOW(), :CREATE_USER , null , null  ) ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DOC_TRANS_ID", obj.getDocAutoId());
         parameters.put("DOC_TRANS_DETAIL_ID", "99999");
         parameters.put("UNIT", obj.getSumItems());
         parameters.put("PRICE", obj.getPrice());
         parameters.put("TOTAL", obj.getTotalPrice());
         parameters.put("DISCOUNT", 0);
         parameters.put("MONEY_INCOME", 0);
         parameters.put("MONEY_RETURN", 0);                         
         parameters.put("REMARK", "Payment");
         parameters.put("CREATE_USER", "System");

        //System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert doc_trans_detail OK ");
    }

    @Override
    public String GenAutoId() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append("  select max(DOC_TRANS_ID) as maxid from  doc_trans_detail ");
        Map<String, Object> parameters = new HashMap<>();
        //parameters.put("xxx", xxxx);
        Map<String, Object> itemsMap = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> result = new HashMap<>();
                result.put("maxid", Utilizer.replaceNull(rs.getString("maxid")));
                return result;
            }
        });
        String maxId = (String) itemsMap.get("maxid");
        String tempId = "";

        String currentID = Utilizer.NowByCalendar("yyyy-MM-dd").replaceAll("-", "");
        if (maxId == null || maxId.equals("")) {
            return currentID + "01";
        }

        String dateID = maxId;
        if (maxId.length() > 7) {
            dateID = maxId.substring(0, 8);
        }
        if (dateID.equals(currentID)) {
            int x = Integer.parseInt(maxId.substring(8, maxId.length()));
            x++;
            tempId = currentID + Utilizer.GenNextId(x);
        } else {
            tempId = currentID + "01";
        }
        return tempId;
    }

    @Override
    public void updateDocTransDetail(DocTransDetail obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteDocTransDetail(int docId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DocTransDetail getDocTransDetail(String docCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DocTransDetail> listDocTransDetail() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
