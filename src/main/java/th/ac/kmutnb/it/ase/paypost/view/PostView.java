package th.ac.kmutnb.it.ase.paypost.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.ac.kmutnb.it.ase.paypost.dao.DistrictDao;
import th.ac.kmutnb.it.ase.paypost.dao.PostDao;
import th.ac.kmutnb.it.ase.paypost.dao.PostMstServiceDao;
import th.ac.kmutnb.it.ase.paypost.dao.ProvinceDao;
import th.ac.kmutnb.it.ase.paypost.dao.StockMstProductCategoryDao;
import th.ac.kmutnb.it.ase.paypost.dao.StockMstProductItemDao;
import th.ac.kmutnb.it.ase.paypost.dao.SubDistrictDao;
import th.ac.kmutnb.it.ase.paypost.dto.District;
import th.ac.kmutnb.it.ase.paypost.dto.Post;
import th.ac.kmutnb.it.ase.paypost.dto.PostContact;
import th.ac.kmutnb.it.ase.paypost.dto.PostService;
import th.ac.kmutnb.it.ase.paypost.dto.Province;
import th.ac.kmutnb.it.ase.paypost.dto.SubDistrict;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstService;
import th.ac.kmutnb.it.ase.paypost.dto.PostProduct;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductItem;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductCategory;
/**
 *
 * @author Panit
 */
@Component
@ManagedBean
public class PostView implements Serializable {

    private static final long serialVersionUID = -6918368566881527064L;

    private Post post;
    private PostService postService;
    private List<Province> provinceList;
    private List<District> amphureList;
    private List<SubDistrict> districtList;
    private List<PostMstService> serviceList;
    private List<StockMstProductItem> productList;
    private List<StockMstProductCategory> productCategoryList;
    private PostProduct postProduct;
    private double servicePrice = 0;
    private double productPrice = 0;
    private double otherPrice = 0;
    private double totalPrice = 0;
    private boolean editChecker = false;
    private int categoryId;

    @Autowired
    private ProvinceDao provinceDao;

    public void setProvinceDao(ProvinceDao provinceDao) {
        this.provinceDao = provinceDao;
    }

    @Autowired
    private DistrictDao districtDao;

    public void setDistrictDao(DistrictDao districtDao) {
        this.districtDao = districtDao;
    }

    @Autowired
    private SubDistrictDao subDistrictDao;

    public void setSubDistrictDao(SubDistrictDao subDistrictDao) {
        this.subDistrictDao = subDistrictDao;
    }

    @Autowired
    private PostMstServiceDao postMstServiceDao;

    public void setPostMstServiceDao(PostMstServiceDao postMstServiceDao) {
        this.postMstServiceDao = postMstServiceDao;
    }

    @Autowired
    private StockMstProductItemDao stockMstProductItemDao;

    public StockMstProductItemDao getStockMstProductItemDao() {
        return stockMstProductItemDao;
    }

    public void setStockMstProductItemDao(StockMstProductItemDao stockMstProductItemDao) {
        this.stockMstProductItemDao = stockMstProductItemDao;
    }

    @Autowired
    private StockMstProductItemDao productItemDao;

    public void setProductItemDao(StockMstProductItemDao productItemDao) {
        this.productItemDao = productItemDao;
    }
    
    @Autowired
    private StockMstProductCategoryDao stockMstProductCategoryDao;

    public void setStockMstProductCategoryDao(StockMstProductCategoryDao stockMstProductCategoryDao) {
	this.stockMstProductCategoryDao = stockMstProductCategoryDao;
    }
    
    
    @Autowired
    private PostDao postDao;

    public void setPostDao(PostDao postDao) {
	this.postDao = postDao;
    }
    

    @PostConstruct
    public void init() {
        post = new Post();
        post.setSender(new PostContact());
        post.setPostServiceList(new ArrayList<PostService>());
        postService = new PostService();
        postService.setReciver(new PostContact());
        postService.setPostProducts(new ArrayList<PostProduct>());
        postProduct = new PostProduct();
        provinceList = provinceDao.getProvinces();
        serviceList = postMstServiceDao.getPostMstServices();
        productList = stockMstProductItemDao.getProductItems();
	productCategoryList = stockMstProductCategoryDao.getProductCategorys();

    }

    public void handleDialogClose() {
        provinceList = provinceDao.getProvinces();
        onProvinceChange(post.getSender());
        onAmphureChange(post.getSender());
        onDistrictChange(post.getSender());
        postService = new PostService();
        postService.setReciver(new PostContact());
        postService.setWeight(0);
        postService.setPostProducts(new ArrayList<PostProduct>());
        servicePrice = 0;
        serviceList = postMstServiceDao.getPostMstServices();
        productList = stockMstProductItemDao.getProductItems();
	productCategoryList = stockMstProductCategoryDao.getProductCategorys();
        postProduct = new PostProduct();
        editChecker = false;
    }
    
    public void createPost() {
	postDao.createPost(post);
	post = new Post();
        post.setSender(new PostContact());
        post.setPostServiceList(new ArrayList<PostService>());
        postService = new PostService();
        postService.setReciver(new PostContact());
        postService.setPostProducts(new ArrayList<PostProduct>());
        postProduct = new PostProduct();
        provinceList = provinceDao.getProvinces();
        serviceList = postMstServiceDao.getPostMstServices();
        productList = stockMstProductItemDao.getProductItems();
	productCategoryList = stockMstProductCategoryDao.getProductCategorys();
	servicePrice = 0;
	otherPrice = 0;
	totalPrice = 0;
	//RequestContext.getCurrentInstance().addCallbackParam("saved", true);
    }

    public void addService() {
        if (!editChecker) {
            post.getPostServiceList().add(postService);
        }
        calculateServicePrice();
        calculateProductPrice();
        calculateTotalPrice();
        provinceList = provinceDao.getProvinces();
        onProvinceChange(post.getSender());
        onAmphureChange(post.getSender());
        onDistrictChange(post.getSender());
        postService = new PostService();
        postService.setReciver(new PostContact());
        postService.setWeight(0);
        postService.setPostProducts(new ArrayList<PostProduct>());
        serviceList = postMstServiceDao.getPostMstServices();
        productList = stockMstProductItemDao.getProductItems();
	productCategoryList = stockMstProductCategoryDao.getProductCategorys();
        postProduct = new PostProduct();
        editChecker = false;

    }

    public void deleteService(PostService ps) {
        post.getPostServiceList().remove(ps);
        calculateTotalPrice();
    }

    public void editService(PostService ps) {
        int postServiceIndex = post.getPostServiceList().indexOf(ps);
        this.postService = post.getPostServiceList().get(postServiceIndex);
        this.editChecker = true;
    }

    public void addProduct() {

        postService.getPostProducts().add(postProduct);
        postProduct = new PostProduct();
    }

    public void deleteProduct(PostProduct pp) {
        postService.getPostProducts().remove(pp);
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public PostService getPostService() {
        return postService;
    }

    public void setPostService(PostService postService) {
        this.postService = postService;
    }

    public List<Province> getProvinceList() {
        return provinceList;
    }

    public void setProvinceList(List<Province> provinceList) {
        this.provinceList = provinceList;
    }

    public List<District> getAmphureList() {
        return amphureList;
    }

    public void setAmphureList(List<District> amphureList) {
        this.amphureList = amphureList;
    }

    public List<SubDistrict> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<SubDistrict> districtList) {
        this.districtList = districtList;
    }

    public void onProvinceChange(Object obj) {
        PostContact pc = (PostContact) obj;
        if (pc.getProvinceId() != null && !pc.getProvinceId().equals(0)) {
            amphureList = districtDao.getDistricts(pc.getProvinceId());
        } else {
            amphureList.clear();
            pc.setAmphureId(null);
            districtList.clear();
            pc.setDistrictId(null);
            pc.setPostcode(null);
        }

    }

    public void onAmphureChange(Object obj) {
        PostContact pc = (PostContact) obj;
        if (pc.getAmphureId() != null && !pc.getAmphureId().equals(0)) {
            districtList = subDistrictDao.getSubDistricts(pc.getAmphureId());
        } else {
            districtList.clear();
            pc.setDistrictId(null);
            pc.setPostcode(null);
        }

    }

    public void onDistrictChange(Object obj) {
        PostContact pc = (PostContact) obj;
        if (pc.getDistrictId() != null && !pc.getDistrictId().equals(0)) {
            Integer districtId = pc.getDistrictId();
            SubDistrict subDistrict = subDistrictDao.getSubDistrict(districtId);
            pc.setPostcode(subDistrictDao.getZipCode(subDistrict.getCode()));
        } else {
            pc.setDistrictId(null);
            pc.setPostcode(null);
        }

    }

    public void weightChange() {
//        if (postService.getServiceId() != null && !postService.getServiceId().equals(0)) {
//            if (postService.getWeight() > 0.0) {
//                double weightCost = postMstServiceDao.getPostMstService(postService.getServiceId()).getWeightCost();
//                servicePrice = weightCost * postService.getWeight();
//            } else {
//                servicePrice = postMstServiceDao.getPostMstService(postService.getServiceId()).getFixCost();
//            }
//        }
    }

    public void onServiceChange() {
//        if (postService.getServiceId() != null && !postService.getServiceId().equals(0)) {
//            if (postService.getWeight() > 0.0) {
//                double weightCost = postMstServiceDao.getPostMstService(postService.getServiceId()).getWeightCost();
//                servicePrice = weightCost * postService.getWeight();
//            } else {
//                servicePrice = postMstServiceDao.getPostMstService(postService.getServiceId()).getFixCost();
//            }
//        }
    }
    
    public void onProductCategoryChange() {
	productList = stockMstProductItemDao.getProductItemsByCategory(categoryId);
    }

    public List<PostMstService> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<PostMstService> serviceList) {
        this.serviceList = serviceList;
    }

    public double getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(double servicePrice) {
        this.servicePrice = servicePrice;
    }

    public List<StockMstProductItem> getProductList() {
        return productList;
    }

    public void setProductList(List<StockMstProductItem> productList) {
        this.productList = productList;
    }

    public PostProduct getPostProduct() {
        return postProduct;
    }

    public void setPostProduct(PostProduct postProduct) {
        this.postProduct = postProduct;
    }

    public boolean isEditChecker() {
        return editChecker;
    }

    public void setEditChecker(boolean editChecker) {
        this.editChecker = editChecker;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public double getOtherPrice() {
        return otherPrice;
    }

    public void setOtherPrice(double otherPrice) {
        this.otherPrice = otherPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
    
    public void calculateTotalPrice() {
        this.totalPrice = 0;
        calculateProductPrice();
        calculateServicePrice();
        System.out.println(this.productPrice);
        System.out.println(this.servicePrice);
        System.out.println(this.otherPrice);
        this.totalPrice += this.productPrice;
        this.totalPrice += this.servicePrice;
        this.totalPrice += this.otherPrice;
        post.setOtherPrice(otherPrice);
        post.setTotalPrice(totalPrice);
    }

    public void calculateProductPrice() {
        this.productPrice = 0;
        for (PostService ps : post.getPostServiceList()) {
            if (!ps.getPostProducts().isEmpty()) {
                for (PostProduct pd : ps.getPostProducts()) {
                    double price = stockMstProductItemDao.getProductItem(pd.getProductId()).getPrice();
                    this.productPrice += price * pd.getAmount();
                }
            }

        }
    }
    
    public void calculateServicePrice() {
        this.servicePrice = 0;
        for (PostService ps : post.getPostServiceList()) {
            if (ps.getWeight() > 0.0) {
                //System.out.println(postMstServiceDao.getPostMstService(ps.getServiceId()));
                double weightCost = postMstServiceDao.getPostMstService(ps.getServiceId()).getWeightCost();
                System.out.println(weightCost);
                this.servicePrice += weightCost * ps.getWeight();
            } else {
                this.servicePrice += postMstServiceDao.getPostMstService(ps.getServiceId()).getFixCost();
            }
            
        }
    }
    
    public String getProductName(int id){
	return productItemDao.getProductItem(id).getProductName();
    }

    public List<StockMstProductCategory> getProductCategoryList() {
	return productCategoryList;
    }

    public void setProductCategoryList(List<StockMstProductCategory> productCategoryList) {
	this.productCategoryList = productCategoryList;
    }

    public int getCategoryId() {
	return categoryId;
    }

    public void setCategoryId(int categoryId) {
	this.categoryId = categoryId;
    }
    
    

}
