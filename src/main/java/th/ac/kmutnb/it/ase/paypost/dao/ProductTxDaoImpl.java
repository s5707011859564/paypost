/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import th.ac.kmutnb.it.ase.paypost.dto.ProductTx;
import th.ac.kmutnb.it.ase.paypost.dto.ProductTxItem;

/**
 *
 * @author imetanon
 */
public class ProductTxDaoImpl implements ProductTxDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
	jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void createProductTx(final ProductTx ptx) {
	final String sql = "INSERT INTO \n"
		+ "product_tx(\n"
		+ "firstName,lastName,phone,totalPrice\n"
		+ ") VALUES (?,?,?,?)";
	KeyHolder keyHolder = new GeneratedKeyHolder();
	//post
	jdbcTemplate.getJdbcOperations().update(new PreparedStatementCreator() {
	    @Override
	    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(sql, new String[]{"proudct_tx_id"});
		ps.setString(1, ptx.getFirstName());
		ps.setString(2, ptx.getLastName());
		ps.setString(3, ptx.getPhone());
		ps.setDouble(4, ptx.getTotalPrice());
		return ps;
	    }
	}, keyHolder);
	final int productTxId = keyHolder.getKey().intValue();
	//products
	if (!ptx.getItemList().isEmpty()) {
	    for (final ProductTxItem pi : ptx.getItemList()) {
		final String sqlProduct = "INSERT INTO \n"
			+ "product_tx_item(\n"
			+ "product_tx_id, product_id, amount\n"
			+ ") VALUES (?,?,?)";
		jdbcTemplate.getJdbcOperations().update(new PreparedStatementCreator() {
		    @Override
		    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			PreparedStatement ps = connection.prepareStatement(sqlProduct, new String[]{"itemId"});
			ps.setInt(1, productTxId);
			ps.setInt(2, pi.getProductId());
			ps.setInt(3, pi.getAmount());

			return ps;
		    }
		}, keyHolder);
	    }
	}
    }

    @Override
    public void updateProductTx(ProductTx post) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteProductTx(int postId) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProductTx getProductTx(int postId) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ProductTx> getProductTxs() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
