/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.ProductTx;

/**
 *
 * @author imetanon
 */
public interface ProductTxDao {
    public void createProductTx(ProductTx post);
    public void updateProductTx(ProductTx post);
    public void deleteProductTx(int postId);
    public ProductTx getProductTx(int postId);
    public List<ProductTx> getProductTxs();
}
