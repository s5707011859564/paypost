/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.List;
import java.util.Map;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstProductService;

/**
 *
 * @author Administrator
 */
public interface PostMstProductServiceDao {

    public void createPostMstProductService(PostMstProductService obj);

    public void updatePostMstProductService(PostMstProductService obj);

    public void deletePostMstProductService(int psId);

    public PostMstProductService getPostPostMstProductService(int psId);

    public List<PostMstProductService> listPostMstProductService();
    
     public Map<String,String>  listMapPostMstProductType();
     public Map<String,String>  listMapPostMstDeliveryType();
     public Map<String,String>  listMapPostMstAreaType();
}
