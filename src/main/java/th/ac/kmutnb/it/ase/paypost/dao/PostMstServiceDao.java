/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;
import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstService;

/**
 *
 * @author imetanon
 */
public interface PostMstServiceDao {

    public void createPostMstService(PostMstService postMstService);
    public void updatePostMstService(PostMstService postMstService);
    public void deletePostMstService(int postMstServiceId);
    public PostMstService getPostMstService(int postMstServiceId);
    public List<PostMstService> getPostMstServices();
    
}
