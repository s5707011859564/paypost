package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.District;

public class DistrictDaoImpl implements DistrictDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<District> getDistricts(int provinceId) {
        String sql = 
"SELECT \n" +
"    AMPHUR_ID, AMPHUR_CODE, AMPHUR_NAME\n" +
"FROM\n" +
"    amphures\n" +
"WHERE\n" +
"    PROVINCE_ID = :id\n" +
"ORDER BY AMPHUR_CODE ASC";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", provinceId);
        List<District> result = jdbcTemplate.query(sql, parameters, new RowMapper<District>() {
            @Override
            public District mapRow(ResultSet rs, int i) throws SQLException {
                District o = new District();
                o.setId(rs.getInt("AMPHUR_ID"));
                o.setCode(rs.getString("AMPHUR_CODE"));
                o.setName(rs.getString("AMPHUR_NAME"));
                return o;
            }
        });
        return result;
    }

    @Override
    public District getDistrict(int id) {
        String sql = 
"SELECT \n" +
"    AMPHUR_ID, AMPHUR_CODE, AMPHUR_NAME\n" +
"FROM\n" +
"    amphures\n" +
"WHERE\n" +
"    AMPHUR_ID = :id\n" +
"ORDER BY AMPHUR_CODE ASC";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("id", id);
        District result = jdbcTemplate.queryForObject(sql, parameters, new RowMapper<District>() {
            @Override
            public District mapRow(ResultSet rs, int i) throws SQLException {
                District o = new District();
                o.setId(rs.getInt("AMPHUR_ID"));
                o.setCode(rs.getString("AMPHUR_CODE"));
                o.setName(rs.getString("AMPHUR_NAME"));
                return o;
            }
        });
        return result;
    }

}
