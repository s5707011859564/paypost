package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

public class Province implements Serializable {

    private static final long serialVersionUID = -8125533460425368955L;
    private int id;
    private String code;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
