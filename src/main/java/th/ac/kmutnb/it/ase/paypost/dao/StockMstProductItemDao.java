/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;
import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProductItem;
/**
 *
 * @author imetanon
 */
public interface StockMstProductItemDao {
    public void createProductItem(StockMstProductItem productItem);
    public void updateProductItem(StockMstProductItem productItem);
    public void deleteProductItem(int productItemId);
    public StockMstProductItem getProductItem(int productItemId);
    public List<StockMstProductItem> getProductItems();
    public List<StockMstProductItem> getProductItemsByCategory(int categoryId);
}
