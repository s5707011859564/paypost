/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 *
 * ประเภทพื้นที่การส่ง
 */
public class PostMstAreaType implements Serializable {

    private static final long serialVersionUID = -7299648880554441352L;

    private int areaTypeId;
    private String areaTypeCode;
    private String areaTypeDescTH;
    private String areaTypeDescEN;
    private String createBy;
    private String createDate;
    private String updateBy;
    private String updateDate;
    private int isActive;

    public int getAreaTypeId() {
        return areaTypeId;
    }

    public void setAreaTypeId(int areaTypeId) {
        this.areaTypeId = areaTypeId;
    }

    public String getAreaTypeCode() {
        return areaTypeCode;
    }

    public void setAreaTypeCode(String areaTypeCode) {
        this.areaTypeCode = areaTypeCode;
    }

    public String getAreaTypeDescTH() {
        return areaTypeDescTH;
    }

    public void setAreaTypeDescTH(String areaTypeDescTH) {
        this.areaTypeDescTH = areaTypeDescTH;
    }

    public String getAreaTypeDescEN() {
        return areaTypeDescEN;
    }

    public void setAreaTypeDescEN(String areaTypeDescEN) {
        this.areaTypeDescEN = areaTypeDescEN;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "PostMstAreaType{" + "areaTypeId=" + areaTypeId + ", areaTypeCode=" + areaTypeCode + ", areaTypeDescTH=" + areaTypeDescTH + ", areaTypeDescEN=" + areaTypeDescEN + ", createBy=" + createBy + ", createDate=" + createDate + ", updateBy=" + updateBy + ", updateDate=" + updateDate + ", isActive=" + isActive + '}';
    }

}
