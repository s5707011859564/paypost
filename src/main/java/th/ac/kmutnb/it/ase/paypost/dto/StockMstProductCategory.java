/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author imetanon
 */
public class StockMstProductCategory implements Serializable{
    private static final long serialVersionUID = 2384452997960370717L;
    
    private int productCategoryId;
    private String productCategoryCode;
    private String productCategoryName;
    private boolean isActive;
    private String remark;

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductCategoryCode() {
        return productCategoryCode;
    }

    public void setProductCategoryCode(String productCategoryCode) {
        this.productCategoryCode = productCategoryCode;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
	return "StockMstProductCategory{" + "productCategoryId=" + productCategoryId + ", productCategoryCode=" + productCategoryCode + ", productCategoryName=" + productCategoryName + ", isActive=" + isActive + ", remark=" + remark + '}';
    }

    
    
    
}
