package th.ac.kmutnb.it.ase.paypost.dao;

import java.util.AbstractList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import th.ac.kmutnb.it.ase.paypost.dto.Payment;

public class PaymentDaoImpl implements PaymentDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void createPayments(final List<Payment> payments) {
        String sql =
"insert into payments (payee, referenceNumber, amount, dueDate, created_datetime, created_user) values (:payee, :referenceNumber, :amount, :dueDate, now(), :created_user)";
        final String createdUser = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        List<SqlParameterSource> parameters = new AbstractList<SqlParameterSource>() {
            @Override
            public SqlParameterSource get(int i) {
                Map<String, Object> values = new HashMap();
                values.put("payee", payments.get(i).getPayee());
                values.put("referenceNumber", payments.get(i).getReferenceNumber());
                values.put("amount", payments.get(i).getAmount());
                values.put("dueDate", payments.get(i).getDueDate());
                values.put("created_user", createdUser);
                return new MapSqlParameterSource().addValues(values);
            }

            @Override
            public int size() {
                return payments.size();
            }
        };
        jdbcTemplate.batchUpdate(sql, parameters.toArray(new SqlParameterSource[0]));
    }

}
