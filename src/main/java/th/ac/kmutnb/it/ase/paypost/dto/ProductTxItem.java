/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author imetanon
 */
public class ProductTxItem implements Serializable{
    private static final long serialVersionUID = -1271233867932008694L;
    
    private int itemId;
    private int productTxId;
    private int productId;
    private int amount;

    public int getItemId() {
	return itemId;
    }

    public void setItemId(int itemId) {
	this.itemId = itemId;
    }

    public int getProductTxId() {
	return productTxId;
    }

    public void setProductTxId(int productTxId) {
	this.productTxId = productTxId;
    }

    

    public int getProductId() {
	return productId;
    }

    public void setProductId(int productId) {
	this.productId = productId;
    }

    public int getAmount() {
	return amount;
    }

    public void setAmount(int amount) {
	this.amount = amount;
    }

    @Override
    public String toString() {
	return "ProductTxItem{" + "itemId=" + itemId + ", productTxTd=" + productTxId + ", productId=" + productId + ", amount=" + amount + '}';
    }
    
    
    
    
    
    
}
