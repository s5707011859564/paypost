/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author imetanon
 */
public class PostMstService implements Serializable{
    private static final long serialVersionUID = 3466538067071364650L;
    
    private int serviceId;
    private String serviceName;
    private double fixCost;
    private double weightCost;

    public int getServiceId() {
	return serviceId;
    }

    public void setServiceId(int serviceId) {
	this.serviceId = serviceId;
    }

    public String getServiceName() {
	return serviceName;
    }

    public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
    }

    public double getFixCost() {
	return fixCost;
    }

    public void setFixCost(double fixCost) {
	this.fixCost = fixCost;
    }

    public double getWeightCost() {
	return weightCost;
    }

    public void setWeightCost(double weightCost) {
	this.weightCost = weightCost;
    }
    
    
}
