/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 * ค่าธรรมเนียมการบริการ
 */
public class PostMstServiceFee  implements  Serializable{
    private static final long serialVersionUID = -7443462170768768289L;
    private int postServiceFeeId;
    private String productTypeCode;
    private String productTypeName;
    private String deliveryTypeCode;
    private String deliveryTypeName;
    private int weightFrom;
    private int weightTo;
    private double postFee;
    private double serviceFee;
    private int isActive;
    private String createBy;
    private String createDate;
    private String updateBy;
    private String updateDate;

    public int getPostServiceFeeId() {
        return postServiceFeeId;
    }

    public void setPostServiceFeeId(int postServiceFeeId) {
        this.postServiceFeeId = postServiceFeeId;
    }

    

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getDeliveryTypeName() {
        return deliveryTypeName;
    }

    /*
    PRIMARY KEY (`PRODUCT_TYPE_ID`,`PRODUCT_TYPE_CODE`,`WEIGHT_FROM`,`WEIGHT_TO`)
     */
    public void setDeliveryTypeName(String deliveryTypeName) {
        this.deliveryTypeName = deliveryTypeName;
    }

    public String getProductTypeCode() {
        return productTypeCode;
    }

    public void setProductTypeCode(String productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    public String getDeliveryTypeCode() {
        return deliveryTypeCode;
    }

    public void setDeliveryTypeCode(String deliveryTypeCode) {
        this.deliveryTypeCode = deliveryTypeCode;
    }

    public int getWeightFrom() {
        return weightFrom;
    }

    public void setWeightFrom(int weightFrom) {
        this.weightFrom = weightFrom;
    }

    public int getWeightTo() {
        return weightTo;
    }

    public void setWeightTo(int weightTo) {
        this.weightTo = weightTo;
    }

    public double getPostFee() {
        return postFee;
    }

    public void setPostFee(double postFee) {
        this.postFee = postFee;
    }

    public double getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(double serviceFee) {
        this.serviceFee = serviceFee;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "PostMstServiceFee{" + "postServiceFeeId=" + postServiceFeeId + ", productTypeCode=" + productTypeCode + ", productTypeName=" + productTypeName + ", deliveryTypeCode=" + deliveryTypeCode + ", deliveryTypeName=" + deliveryTypeName + ", weightFrom=" + weightFrom + ", weightTo=" + weightTo + ", postFee=" + postFee + ", serviceFee=" + serviceFee + ", isActive=" + isActive + ", createBy=" + createBy + ", createDate=" + createDate + ", updateBy=" + updateBy + ", updateDate=" + updateDate + '}';
    }


    
}
