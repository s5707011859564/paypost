/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.DocMstService;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstDeliveryType;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class DocMstServiceDaoImpl  implements DocMstServiceDao{
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    //---------------------------------------

    @Override
    public void createDocMstService(PostMstDeliveryType obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateDocMstService(PostMstDeliveryType obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteDocMstService(int docId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override 
    public DocMstService getDocMstService(String docCode) { 
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  *  ")
                .append(" FROM paypost.doc_mst_service ")
                .append(" WHERE DOC_CODE = :DOC_CODE ");
       // System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DOC_CODE", docCode);
        DocMstService  docObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<DocMstService>() {
            @Override
            public DocMstService mapRow(ResultSet rs, int rowNumber) throws SQLException {

                DocMstService obj = new DocMstService();
                obj.setDocId(rs.getInt("DOC_ID"));
                obj.setDocCode(Utilizer.replaceNull(rs.getString("DOC_CODE")));
                obj.setDocCodeName(Utilizer.replaceNull(rs.getString("DOC_CODENAME")));
                obj.setDocPaper(Utilizer.replaceNull(rs.getString("DOC_PAPER")));
                obj.setDocRemark(Utilizer.replaceNull(rs.getString("DOC_REMARK")));
                obj.setDocPrice(rs.getDouble("DOC_PRICE"));
                obj.setDocUnit(rs.getInt("DOC_UNIT"));
                obj.setDocType(Utilizer.replaceNull(rs.getString("DOC_TYPE")));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_USER")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("MODIFY_USER")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("MODIFY_DATE")));
                obj.setIsActive(rs.getInt("ISACTIVE"));

                return obj;
            }
        });
        return docObj;
    }

    @Override
    public List<DocMstService> listDocMstService() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public Map<String, String> listMapDocTypeService(String docType) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  doc_code,doc_codename   ")
                .append(" FROM paypost.doc_mst_service ")
                .append("  WHERE  ");
               if("PG".equals(docType)){
                   sql.append("  DOC_TYPE  in ('PC','PG')  ")
                     .append(" Order by  doc_code asc ");
               }else{
                 sql.append("   DOC_TYPE =  '").append(docType).append("'  ")
                      .append(" Order by  doc_code  asc");
               }

        
        System.out.println("===>SQL  :"+sql.toString());
        //Map<String, Object> parameters = new HashMap<>();
        List<DocMstService> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<DocMstService>() {
            @Override
            public DocMstService mapRow(ResultSet rs, int rowNum) throws SQLException {
                DocMstService obj = new DocMstService();
                obj.setDocCode(Utilizer.replaceNull(rs.getString("doc_code")));
                obj.setDocCodeName(Utilizer.replaceNull(rs.getString("doc_codename")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (DocMstService obj : listResult) {
            result.put(obj.getDocCodeName(), obj.getDocCode());
        }
        return result;
    }

    @Override
    public Map<String, String> listMapDocTypeServiceForPaper(String docType) {
      
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  DOC_CODE,DOC_PAPER   ")
                .append(" FROM paypost.doc_mst_service ")
                .append(" WHERE    DOC_TYPE =  '").append(docType).append("'  ")
                .append(" Order by  DOC_CODE ");
        
        System.out.println("===>SQL  :"+sql.toString());
        //Map<String, Object> parameters = new HashMap<>();
        List<DocMstService> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<DocMstService>() {
            @Override
            public DocMstService mapRow(ResultSet rs, int rowNum) throws SQLException {
                DocMstService obj = new DocMstService();
                obj.setDocCode(Utilizer.replaceNull(rs.getString("DOC_CODE")));
                obj.setDocCodeName(Utilizer.replaceNull(rs.getString("DOC_PAPER")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (DocMstService obj : listResult) {
            result.put(obj.getDocCodeName(), obj.getDocCode());
        }
        return result;
    }
    
}
