/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstDeliveryType;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstProductType;
import th.ac.kmutnb.it.ase.paypost.dto.PostMstServiceFee;
import th.ac.kmutnb.it.ase.util.Utilizer;

/**
 *
 * @author Administrator
 */
public class PostMstServiceFeeDaoImpl implements PostMstServiceFeeDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    //---------------------------------------

    @Override
    public void createPostMstServiceFee(PostMstServiceFee obj) {

        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" INSERT INTO paypost.post_mst_service_fee  ")
                .append(" (PRODUCT_TYPE_CODE, DELIVERY_TYPE_CODE, WEIGHT_FROM, WEIGHT_TO, POST_FEE, SERVICE_FEE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE)   ")
                .append(" VALUES ( :PRODUCT_TYPE_CODE , :DELIVERY_TYPE_CODE , :WEIGHT_FROM, :WEIGHT_TO, :POST_FEE, :SERVICE_FEE, :ISACTIVE, :CREATE_BY , NOW(), null, null )  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PRODUCT_TYPE_CODE", obj.getProductTypeCode());
        parameters.put("DELIVERY_TYPE_CODE", obj.getDeliveryTypeCode());
        parameters.put("WEIGHT_FROM", obj.getWeightFrom());
        parameters.put("WEIGHT_TO", obj.getWeightTo());
        parameters.put("POST_FEE", obj.getPostFee());
        parameters.put("SERVICE_FEE", obj.getServiceFee());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("CREATE_BY", obj.getCreateBy());

        //System.out.println("SQL :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Insert post_mst_service_fee OK ");
    }

    @Override
    public void updatePostMstServiceFee(PostMstServiceFee obj) {

        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" UPDATE paypost.post_mst_service_fee   ")
                .append(" SET PRODUCT_TYPE_CODE = :PRODUCT_TYPE_CODE , ")
                .append("  DELIVERY_TYPE_CODE = :DELIVERY_TYPE_CODE , ")
                .append("  WEIGHT_FROM = :WEIGHT_FROM , ")
                .append("  WEIGHT_TO = :WEIGHT_TO , ")
                .append("  POST_FEE = :POST_FEE , ")
                .append("  SERVICE_FEE = :SERVICE_FEE , ")
                .append("  ISACTIVE = :ISACTIVE , ")
                .append("  UPDATE_BY = :UPDATE_BY,  ")
                .append("  UPDATE_DATE = NOW() ")
                .append(" WHERE  PRODUCT_TYPE_ID = :PRODUCT_TYPE_ID  ");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PRODUCT_TYPE_CODE", obj.getProductTypeCode());
        parameters.put("DELIVERY_TYPE_CODE", obj.getDeliveryTypeCode());
        parameters.put("WEIGHT_FROM", obj.getWeightFrom());
        parameters.put("WEIGHT_TO", obj.getWeightTo());
        parameters.put("POST_FEE", obj.getPostFee());
        parameters.put("SERVICE_FEE", obj.getServiceFee());
        parameters.put("ISACTIVE", obj.getIsActive());
        parameters.put("UPDATE_BY", obj.getUpdateBy());
        //Where
        parameters.put("PRODUCT_TYPE_ID", obj.getPostServiceFeeId());
        //System.out.println("SQL  :" + sql.toString());
        jdbcTemplate.update(sql.toString(), parameters);
        System.out.println("==Update  post_mst_service_fee  OK ");
    }

    @Override
    public void deletePostMstServiceFee(int serviceFeeId) {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" DELETE FROM paypost.post_mst_service_fee   ")
                .append(" WHERE  PRODUCT_TYPE_ID = :PRODUCT_TYPE_ID  ");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PRODUCT_TYPE_ID", serviceFeeId);

        jdbcTemplate.update(sql.toString(), parameters);
        //System.out.println("======Delete :" + serviceFeeId + "  : successfull");
    }

    @Override
    public PostMstServiceFee getPostMstServiceFee(int serviceFeeId) {

        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("  PRODUCT_TYPE_ID, PRODUCT_TYPE_CODE, DELIVERY_TYPE_CODE, WEIGHT_FROM, WEIGHT_TO,  ")
                .append(" POST_FEE, SERVICE_FEE, ISACTIVE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE   ")
                .append(" FROM paypost.post_mst_service_fee ")
                .append(" WHERE PRODUCT_TYPE_ID = :PRODUCT_TYPE_ID ");
       // System.out.println("======Sql :" + sql);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("PRODUCT_TYPE_ID", serviceFeeId);

        PostMstServiceFee serviceFeeObj = jdbcTemplate.queryForObject(sql.toString(), parameters, new RowMapper<PostMstServiceFee>() {
            @Override
            public PostMstServiceFee mapRow(ResultSet rs, int rowNumber) throws SQLException {

                PostMstServiceFee obj = new PostMstServiceFee();
                obj.setPostServiceFeeId(rs.getInt("PRODUCT_TYPE_ID"));
                obj.setProductTypeCode(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")));
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                obj.setWeightFrom(rs.getInt("WEIGHT_FROM"));
                obj.setWeightTo(rs.getInt("WEIGHT_TO"));
                obj.setPostFee(rs.getDouble("POST_FEE"));
                obj.setServiceFee(rs.getDouble("SERVICE_FEE"));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));

                return obj;
            }
        });
        return serviceFeeObj;
    }

    @Override
    public List<PostMstServiceFee> listPostMstServiceFee() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append("   x.PRODUCT_TYPE_ID, ")
                .append("  x.PRODUCT_TYPE_CODE,a.PRODUCT_TYPE_DESC_TH ,  ")
                .append("  x.DELIVERY_TYPE_CODE,b.DELIVERY_TYPE_DESC_TH ,")
                .append("  x.WEIGHT_FROM,  x.WEIGHT_TO,  ")
                .append("  x.POST_FEE,  x.SERVICE_FEE,x.ISACTIVE, ")
                .append("  x.CREATE_BY,  x.CREATE_DATE,  x.UPDATE_BY,  x.UPDATE_DATE   ")
                .append(" FROM paypost.post_mst_service_fee x,paypost.post_mst_product_type a,paypost.post_mst_delivery_type b ")
                .append(" WHERE  x.PRODUCT_TYPE_CODE = a.PRODUCT_TYPE_CODE ")
                .append(" AND x.DELIVERY_TYPE_CODE = b.DELIVERY_TYPE_CODE ")
                .append("  ORDER BY  x.PRODUCT_TYPE_ID  ");
        
       // System.out.println("======Sql :" + sql);
        List<PostMstServiceFee> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstServiceFee>() {
            @Override
            public PostMstServiceFee mapRow(ResultSet rs, int rowNumber) throws SQLException {

                PostMstServiceFee obj = new PostMstServiceFee();
                obj.setPostServiceFeeId(rs.getInt("PRODUCT_TYPE_ID"));
                
                obj.setProductTypeCode(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")));
                obj.setProductTypeName(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_DESC_TH")));
                //obj.setProductTypeName(getProductTypeName(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE"))));
               
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                obj.setDeliveryTypeName(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_TH")));
                //obj.setDeliveryTypeName(getDeliveryTypeName(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE"))));
                obj.setWeightFrom(rs.getInt("WEIGHT_FROM"));
                obj.setWeightTo(rs.getInt("WEIGHT_TO"));
                obj.setPostFee(rs.getDouble("POST_FEE"));
                obj.setServiceFee(rs.getDouble("SERVICE_FEE"));
                obj.setIsActive(rs.getInt("ISACTIVE"));
                obj.setCreateBy(Utilizer.replaceNull(rs.getString("CREATE_BY")));
                obj.setCreateDate(Utilizer.replaceNull(rs.getString("CREATE_DATE")));
                obj.setUpdateBy(Utilizer.replaceNull(rs.getString("UPDATE_BY")));
                obj.setUpdateDate(Utilizer.replaceNull(rs.getString("UPDATE_DATE")));
                return obj;
            }
        });
        return listResult;
    }
    
    
    @Override
    public Map<String, String> listMapPostMstDeliveryType() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" DELIVERY_TYPE_ID,DELIVERY_TYPE_CODE,DELIVERY_TYPE_DESC_TH ")
                .append(" FROM paypost.POST_MST_DELIVERY_TYPE ")
                .append(" ORDER BY DELIVERY_TYPE_ID");
        //Map<String, Object> parameters = new HashMap<>();
        List<PostMstDeliveryType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstDeliveryType>() {
            @Override
            public PostMstDeliveryType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PostMstDeliveryType obj = new PostMstDeliveryType();
                obj.setDeliveryTypeCode(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_CODE")));
                obj.setDeliveryTypeDescTH(Utilizer.replaceNull(rs.getString("DELIVERY_TYPE_DESC_TH")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (PostMstDeliveryType obj : listResult) {
            result.put(obj.getDeliveryTypeDescTH(), obj.getDeliveryTypeCode().trim());
        }
        return result;
    }


   @Override
    public Map<String, String> listMapPostMstProductType() {
        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" SELECT  ")
                .append(" PRODUCT_TYPE_ID,PRODUCT_TYPE_CODE,PRODUCT_TYPE_DESC_TH ")
                .append(" FROM PAYPOST.POST_MST_PRODUCT_TYPE ")
                .append(" ORDER BY PRODUCT_TYPE_ID  ");

        //Map<String, Object> parameters = new HashMap<>();
        List<PostMstProductType> listResult = jdbcTemplate.query(sql.toString(), new RowMapper<PostMstProductType>() {
            @Override
            public PostMstProductType mapRow(ResultSet rs, int rowNum) throws SQLException {
                PostMstProductType obj = new PostMstProductType();
                obj.setProductTypeCode(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_CODE")));
                obj.setProductTypeTH(Utilizer.replaceNull(rs.getString("PRODUCT_TYPE_DESC_TH")));
                return obj;
            }
        });

        Map<String, String> result = new HashMap<>();
        for (PostMstProductType obj : listResult) {
            result.put(obj.getProductTypeTH(), obj.getProductTypeCode());
        }
        return result;
    }

}
