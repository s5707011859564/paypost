/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kmutnb.it.ase.paypost.dao;
import java.util.List;
import th.ac.kmutnb.it.ase.paypost.dto.StockMstProduct;
/**
 *
 * @author imetanon
 */
public interface StockMstProductDao {
    public void createProductStock(StockMstProduct productStock);
    public void updateProductStock(StockMstProduct productStock);
    public void deleteProductStock(int productStockId);
    public StockMstProduct getProductStock(int productStockId);
    public List<StockMstProduct> getProductStocks();
}
